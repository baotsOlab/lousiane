import { AppRegistry } from 'react-native';
import App from './App';

/* eslint-disable-next-line no-console */
console.disableYellowBox = true;

AppRegistry.registerComponent('OliveryCustomer', () => App, false);
