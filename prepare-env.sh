#!/usr/bin/env bash

sourceConfigFile=$1
targetConfigFile=$2

if [ ! -f $1 ];
then
    echo "WARNING: Source configuration file cannot be found!"
    exit 1
fi

echo "// Auto generated file by 'prepare-env.sh', don't modify. Please refer to dev.s, uat.js, or prod.js" > $targetConfigFile
echo "// $1" >> $targetConfigFile
echo "" >> $targetConfigFile

cat $sourceConfigFile >> $targetConfigFile
