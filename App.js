import React, { Component } from 'react';
import { Alert } from 'react-native';
import { Provider } from 'react-redux';
import _ from 'lodash';
import SplashScreen from 'react-native-splash-screen';
import { PersistGate } from 'redux-persist/integration/react';
import errorManager from './src/services/errorManager';
import TabbarController from './src/navigation/TabbarController';
import dataManager from './src/assets/constants/dataManager';
import { store, persistor } from './src/store';
import api from './src/services/api';

export default class App extends Component {
  state = { done: false };
  componentDidMount() {
    this._launchConfig();
  }

  _launchConfig = async () => {
    this._getPhoneNumber();
    await dataManager.setDeviceId();
    this.setState({ done: true });
    errorManager.subscribe(this._onError);
    SplashScreen.hide();
  };

  _getPhoneNumber = async () => {
    dataManager.phoneNumber = await api.getPhoneNumber();
  };

  componentWillUnmount() {
    errorManager.unsubscribe(this._onError);
  }

  _onError = (title, error) => {
    const message = _.get(error, 'message', 'Oops, it\'s an error from us. Please make another attempt or contact our support line.');
    Alert.alert(title, message);
  };

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          { this.state.done && <TabbarController /> }
        </PersistGate>
      </Provider>
    );
  }
}
