import { StyleSheet } from 'react-native';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  loadingView: {
    flex: 1,
    position: 'absolute',
    backgroundColor: Colors.BLACK_OPACITY40,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: Colors.WHITE
  }
});
