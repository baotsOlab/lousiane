import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import _ from 'lodash';
import styles from './styles';

export default (WrappedComponent) =>
  class WithLoadingHOC extends Component {
    state = { loading: false, title: '', showIndicator: true };

    _showLoading = (loading, options) => {
      const title = _.get(options, 'title', '');
      const showIndicator = _.get(options, 'showIndicator', true);
      this.setState({ loading, title, showIndicator });
    };

    render() {
      const { loading, title, showIndicator } = this.state;
      return (
        <View style={styles.container}>
          <WrappedComponent
            {...this.props}
            showLoading={this._showLoading}
            loading={loading}
            loadingText={title}
          />
          {
            loading && showIndicator && (
              <View style={styles.loadingView}>
                <ActivityIndicator
                  size='large'
                  color='white'
                  animating={loading}
                />
                { !_.isEmpty(title) && <Text style={styles.title}>{ title }</Text> }
              </View>
            )
          }
        </View>
      );
    }
  };
