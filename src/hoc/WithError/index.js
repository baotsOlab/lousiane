import React, { Component } from 'react';
import { View } from 'react-native';
import _ from 'lodash';
import styles from './styles';
import GeneralError from '../../components/GeneralError';

export default (WrappedComponent) =>
  class WithLoadingHOC extends Component {
    state = { error: null, title: '' };
    _onRetry = () => {};

    _showError = (title, error, onRetry = () => {}) => {
      this._onRetry = onRetry;
      this.setState({ title, error });
    };

    render() {
      const { title, error } = this.state;
      return (
        <View style={styles.container}>
          {
            _.isNil(error) && (
              <WrappedComponent
                {...this.props}
                showError={this._showError}
              />
            )
          }
          {
            !_.isNil(error) && (
              <GeneralError title={title} error={error} onReload={this._onRetry} />
            )
          }
        </View>
      );
    }
  };
