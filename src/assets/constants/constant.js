import {
  Dimensions
} from 'react-native';

const { width, height } = Dimensions.get('window');
export const deviceWidth = width;
export const deviceHeight = height;
export const disPlayScale = width / 375.0;
export const ADDRESS_TYPES = {
  COUNTRY: 'country',
  COLLOQUIAL_AREA: 'colloquial_area'
};

export const RESTAURANT_STATUS = {
  OPEN: 'OPEN',
  CLOSE: 'CLOSE',
  DELAY: 'DELAY'
};

export const HARDWARE_BACK_PRESS = 'hardwareBackPress';

// TODO replace to customer service phone number
export const PHONE_NUMBER = '+84908873436';

export const ORDER_PHASES = {
  SUBMITTED: 'SUBMITTED',
  CONFIRMATION: 'CONFIRMATION',
  ASSIGNED: 'ASSIGNED',
  PICKUP: 'PICKUP',
  DELIVERED: 'DELIVERED'
};

export const ORDER_STATUS = {
  TODO: 'TODO',
  IN_PROGRESS: 'IN_PROGRESS',
  DONE: 'DONE'
};

export const OPEN_PICKER_CONFIG = {
  hideBottomControls: true,
  showCropGuidelines: false,
  cropperToolbarTitle: 'Move and Scale',
  width: 250,
  height: 250,
  cropping: true,
  cropperCircleOverlay: true,
  compressImageMaxWidth: 250,
  compressImageMaxHeight: 250,
  compressImageQuality: 1,
  includeExif: true,
  mediaType: 'photo'
};

export const OPEN_CAMERA_CONFIG = {
  hideBottomControls: true,
  showCropGuidelines: false,
  cropperToolbarTitle: 'Move and Scale',
  cropping: true,
  cropperCircleOverlay: true,
  compressImageMaxWidth: 250,
  compressImageMaxHeight: 250,
  compressImageQuality: 1,
  width: 250,
  height: 250,
  includeBase64: true,
  includeExif: true,
  mediaType: 'photo'
};

export const NOTIFICATION_TYPE = {
  ORDER_STATUS: 'order_status'
};

export const GOOGLE_STATUS = {
  OK: 'OK',
  ZERO_RESULTS: 'ZERO_RESULTS',
};

export const STATUS = {
  NONE: 1,
  SUCCESS: 2,
  FAILED: 3
};

export const ERROR_CODE = {
  PRICE_CHANGE: 'E_001'
};
