import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import utils from '../../utils';
import { PHONE_NUMBER } from './constant';
import { store } from '../../store';
import api from '../../services/api';
import { actions as authActions } from '../../store/reducers/auth';

const DEVICE_ID_KEY = 'DEVICE_ID_KEY';

class DataManager {
  selectedLocation = {};
  constructor() {
    this.deviceId = '';
    this.phoneNumber = PHONE_NUMBER;
  }

  async fetchProfile() {
    const result = await api.getProfile();
    store.dispatch(authActions.setProfile(result));
  }

  async setDeviceId() {
    const deviceId = await AsyncStorage.getItem(DEVICE_ID_KEY);
    if (_.isNil(deviceId)) {
      this.deviceId = utils.uuidv4();
      AsyncStorage.setItem(DEVICE_ID_KEY, this.deviceId);
    } else {
      this.deviceId = deviceId;
    }
  }
}

export default new DataManager();
