const baseUrl = 'http://52.187.232.212/api';

export default {
  hostHeader: 'olivery.develop',
  baseUrl,
  stripe: {
    publishableKey: 'pk_test_VMnG7UobmfgrkViLRdnWHelP'
  },
  googleAPI: {
    maps: {
      GET_LOCATION_GOOGLE_MAP_URL: `${baseUrl}/utility/google-proxy?url=https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lng}`,
      GET_LOCATIONS_GOOGLE_MAP_URL: `${baseUrl}/utility/google-proxy?url=https://maps.googleapis.com/maps/api/geocode/json?address={address}`,
      GET_DISTANCE_MATRIX_GOOGLE_MAP_URL: `${baseUrl}/utility/google-proxy?url=https://maps.googleapis.com/maps/api/distancematrix/json?origins={from}&destinations={to}`
    }
  }
};

