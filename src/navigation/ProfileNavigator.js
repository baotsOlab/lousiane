import { createStackNavigator } from 'react-navigation';

import SignUp from '../containers/SignUp';
import EnterCode from '../containers/EnterCode';
import Congratulations from '../containers/Congratulations';
import EnterEmail from '../containers/EnterEmail';
import SubmitPhone from '../containers/SubmitPhone';
import ProfileOption from '../containers/ProfileOption';
import LoginRequired from '../containers/LoginRequired';
import Help from '../containers/Help';
import Profile from '../containers/Profile';
import EditProfile from '../containers/EditProfile';
import SearchLocation from '../containers/SearchLocation';
import OrderHistory from '../containers/OrderHistory';
import OrderDetail from '../containers/OrderDetail';
import OrderStatus from '../containers/OrderStatus';
import Routes from './routes';

const ProfileNavigator = createStackNavigator(
  {
    [Routes.SignUp]: { screen: SignUp },
    [Routes.EnterCode]: { screen: EnterCode },
    [Routes.Congratulations]: { screen: Congratulations },
    [Routes.EnterEmail]: { screen: EnterEmail },
    [Routes.SubmitPhone]: { screen: SubmitPhone },
    [Routes.ProfileOption]: { screen: ProfileOption },
    [Routes.LoginRequired]: { screen: LoginRequired },
    [Routes.Help]: { screen: Help },
    [Routes.Profile]: { screen: Profile },
    [Routes.EditProfile]: { screen: EditProfile },
    [Routes.SearchLocation]: { screen: SearchLocation },
    [Routes.OrderHistory]: { screen: OrderHistory },
    [Routes.OrderDetail]: { screen: OrderDetail },
    [Routes.OrderStatus]: { screen: OrderStatus },
  },
  {
    initialRouteName: Routes.LoginRequired,
    headerMode: 'none',
  },
);

export default ProfileNavigator;
