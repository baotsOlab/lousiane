import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import PropTypes from 'prop-types';
import HomeNavigator from './HomeNavigator';
import ProfileNavigator from './ProfileNavigator';
import CartNavigator from './CartNavigator';
// import SearchNavigator from './SearchNavigator';
import Images from '../assets/Images';
import styles from './TabbarController.styles';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../services/subscriptionManager';
import { NOTIFICATION_TYPE } from '../assets/constants/constant';
import navigationUtils from '../utils/navigation';
import Routes from './routes';
import dataManager from '../assets/constants/dataManager';
import fcm from '../utils/fcm';

const tabType = {
  home: 0,
  search: 1,
  cart: 2,
  profile: 3,
};

const tabIcon = {
  home: {
    active: Images.tabHomeActive,
    inactive: Images.tabHomeInactive
  },
  search: {
    active: Images.tabSearchActive,
    inactive: Images.tabSearchInactive
  },
  cart: {
    active: Images.tabCartActive,
    inactive: Images.tabCartInactive
  },
  profile: {
    active: Images.tabProfileActive,
    inactive: Images.tabProfileInactive
  },
};

class TabbarController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: tabType.home,
      cartNumber: 0,
      showTabBar: true
    };
  }

  componentDidMount() {
    dataManager.fetchProfile();
    fcm.updateToken();
    subscriptionManager.subscribe(SUBSCRIPTION_TYPE.NOTIFICATION_OPENED, this._onNotificationOpen);

    this._checkInitialNotifications();
  }

  _checkInitialNotifications = async () => {
    const openResult = await firebase.notifications().getInitialNotification();
    this._onNotificationOpen(openResult.notification.data);
  };


  componentWillUnmount() {
    subscriptionManager
      .unsubscribe(SUBSCRIPTION_TYPE.NOTIFICATION_OPENED, this._onNotificationOpen);
  }

  _onNotificationOpen = (notificationData) => {
    const payload = JSON.parse(notificationData.data);
    const { cart_id: cartId, type } = payload;
    if (type !== NOTIFICATION_TYPE.ORDER_STATUS) return;
    if (!this.props.loggedIn) return;
    this.setState({ selectedTab: tabType.profile }, () => {
      if (this.profileNavigator) {
        navigationUtils.reset({
          dispatch: this.profileNavigator.dispatch,
          index: 2,
          routes: [
            { name: Routes.ProfileOption },
            { name: Routes.OrderHistory },
            { name: Routes.OrderDetail, params: { id: cartId } }
          ]
        });
      }
    });
  };

  _renderIcon = (icon, imageStyle, containerStyle) => () => {
    return (
      <View style={containerStyle}>
        <Image source={icon} style={imageStyle}/>
      </View>
    );
  };

  _resetToRoot = type => {
    const { loggedIn } = this.props;
    const { selectedTab } = this.state;
    if (selectedTab !== type) return;
    switch (type) {
      case tabType.home: {
        this.homeNavigator._navigation.navigate(Routes.HomeNearYou);
        break;
      }
      case tabType.search: {
        this.searchNavigator._navigation.navigate(Routes.SearchRestaurant);
        break;
      }
      case tabType.cart: {
        this.cartNavigator._navigation.navigate(Routes.Cart);
        break;
      }
      case tabType.profile: {
        if (loggedIn) {
          this.profileNavigator._navigation.navigate(Routes.ProfileOption);
        }
        break;
      }
      default: break;
    }
  };

  _selectedTab = (type) => () => {
    this._resetToRoot(type);
    this.setState({ selectedTab: type });
  };

  _isSelected = (type) => {
    return this.state.selectedTab === type;
  };

  _showTabBar = (show) => {
    this.setState({ showTabBar: show });
  };

  render() {
    const { showTabBar } = this.state;
    return (
      <TabNavigator
        tabBarStyle={[styles.tabBar, !showTabBar && styles.hideTabBar]}
        tabBarShadowStyle={{ height: 0 }}
        sceneStyle={[styles.scene, !showTabBar && styles.hideScene]}
      >
        <TabNavigator.Item
          selected={this._isSelected(tabType.home)}
          renderIcon={this._renderIcon(tabIcon.home.inactive, styles.iconHome, styles.item)}
          renderSelectedIcon={
            this._renderIcon(tabIcon.home.active, styles.iconHome, styles.itemSelected)
          }
          onPress={this._selectedTab(tabType.home)}>
          <HomeNavigator
            screenProps={{ tabBar: { show: this._showTabBar } }}
            ref={ref => { this.homeNavigator = ref; }}
          />
        </TabNavigator.Item>

        {/* <TabNavigator.Item
          selected={this._isSelected(tabType.search)}
          renderIcon={this._renderIcon(tabIcon.search.inactive, styles.iconSearch, styles.item)}
          renderSelectedIcon={this._renderIcon(
            tabIcon.search.active,
            styles.iconSearch,
            styles.itemSelected
          )}
          onPress={this._selectedTab(tabType.search)}>
          <SearchNavigator ref={ref => { this.searchNavigator = ref; }}/>
          </TabNavigator.Item> */}

        <TabNavigator.Item
          selected={this._isSelected(tabType.cart)}
          renderIcon={this._renderIcon(tabIcon.cart.inactive, styles.iconCart, styles.item)}
          renderSelectedIcon={
            this._renderIcon(tabIcon.cart.active, styles.iconCart, styles.itemSelected)
          }
          onPress={this._selectedTab(tabType.cart)}
          renderBadge={() => <CardBadgeWithRedux />}
        >
          <CartNavigator
            screenProps={{ tabBar: { show: this._showTabBar } }}
            ref={ref => { this.cartNavigator = ref; }}
          />
        </TabNavigator.Item>

        <TabNavigator.Item
          selected={this._isSelected(tabType.profile)}
          renderIcon={this._renderIcon(tabIcon.profile.inactive, styles.iconProfile, styles.item)}
          renderSelectedIcon={this._renderIcon(
            tabIcon.profile.active,
            styles.iconProfile,
            styles.itemSelected
          )}
          onPress={this._selectedTab(tabType.profile)}>
          <ProfileNavigator ref={ref => { this.profileNavigator = ref; }}/>
        </TabNavigator.Item>
      </TabNavigator>
    );
  }
}

TabbarController.propTypes = {
  loggedIn: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn
});
export default connect(mapStateToProps)(TabbarController);

const CardBadge = ({ numBadge }) => {
  if (numBadge <= 0) return null;
  return (
    <View style={styles.badgeContainer}>
      <Text style={styles.badgeText}>{numBadge}</Text>
    </View>
  );
};

const mapStateToPropsForCardBadge = state => ({
  numBadge: state.cart.totalQuantity
});
const CardBadgeWithRedux = connect(mapStateToPropsForCardBadge)(CardBadge);
