import { createStackNavigator } from 'react-navigation';

import SignUp from '../containers/SignUp';
import EnterCode from '../containers/EnterCode';
import Congratulations from '../containers/Congratulations';
import EnterEmail from '../containers/EnterEmail';
import SubmitPhone from '../containers/SubmitPhone';
import Policy from '../containers/Policy';
import Routes from './routes';

const LoginNavigator = createStackNavigator(
  {
    [Routes.SignUp]: {
      screen: SignUp
    },
    [Routes.EnterCode]: {
      screen: EnterCode
    },
    [Routes.Congratulations]: {
      screen: Congratulations
    },
    [Routes.EnterEmail]: {
      screen: EnterEmail
    },
    [Routes.SubmitPhone]: {
      screen: SubmitPhone
    },
    [Routes.Policy]: {
      screen: Policy
    },
  },
  {
    initialRouteName: Routes.Login,
    headerMode: 'none',
  },
);

export default LoginNavigator;
