import { StyleSheet } from 'react-native';
import { deviceWidth } from '../assets/constants/constant';
import Colors, {
  NEW_BOTTOM_MENU_COLOR
} from '../common/Colors';
import Fonts from '../common/Fonts';

export const tabBarHeight = 63;
export default StyleSheet.create({
  tabBar: {
    height: tabBarHeight,
    overflow: 'hidden',
    marginBottom: -13,
    borderTopWidth: 0.5,
    borderTopColor: '#979797',
  },
  hideTabBar: {
    height: 0
  },
  scene: {
    paddingBottom: tabBarHeight - 13
  },
  hideScene: {
    paddingBottom: 0
  },
  item: {
    width: deviceWidth / 3,
    height: 50,
    // backgroundColor: Colors.OLIVERY_GREEN1,
    backgroundColor: NEW_BOTTOM_MENU_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemSelected: {
    width: deviceWidth / 3,
    height: 50,
    // backgroundColor: Colors.OLIVERY_GREEN2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconHome: {
    resizeMode: 'contain',
    width: 22,
    height: 24,
  },
  iconSearch: {
    resizeMode: 'contain',
    width: 25,
    height: 25,
  },
  iconCart: {
    resizeMode: 'contain',
    width: 24,
    height: 26,
  },
  iconProfile: {
    resizeMode: 'contain',
    width: 19,
    height: 24,
  },
  badgeContainer: {
    position: 'absolute',
    // right: 20,
    // top: 4,
    right: 35,
    top: 6,
    minWidth: 22,
    paddingHorizontal: 4,
    height: 22,
    backgroundColor: Colors.OLIVERY_RED1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  badgeText: {
    fontFamily: Fonts.BOLD,
    fontSize: 12,
    color: Colors.WHITE,
    backgroundColor: Colors.TRANSPARENT
  }
});
