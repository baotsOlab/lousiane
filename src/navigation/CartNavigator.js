import { createStackNavigator } from 'react-navigation';

import Cart from '../containers/Cart';
import NearYou from '../containers/NearYou';
import Payment from '../containers/Payment';
import PaymentSuccess from '../containers/PaymentSuccess';
import PaymentDetail from '../containers/PaymentDetail';
import OrderStatus from '../containers/OrderStatus';
import SearchLocation from '../containers/SearchLocation';
import EnterCode from '../containers/EnterCode';
import HomeMenu from '../containers/HomeMenu';
import HomeMenuDetail from '../containers/HomeMenuDetail';
import Routes from './routes';
import DishDetail from '../containers/DishDetail';

const CartNavigator = createStackNavigator(
  {
    [Routes.Cart]: {
      screen: Cart
    },
    [Routes.NearYou]: {
      screen: NearYou
    },
    [Routes.HomeMenu]: {
      screen: HomeMenu
    },
    [Routes.HomeMenuDetail]: {
      screen: HomeMenuDetail
    },
    [Routes.DishDetail]: {
      screen: DishDetail
    },
    [Routes.Payment]: {
      screen: Payment
    },
    [Routes.PaymentSuccess]: {
      screen: PaymentSuccess
    },
    [Routes.PaymentDetail]: {
      screen: PaymentDetail
    },
    [Routes.OrderStatus]: {
      screen: OrderStatus
    },
    [Routes.SearchLocation]: {
      screen: SearchLocation
    },
    [Routes.EnterCode]: {
      screen: EnterCode
    }
  },
  {
    initialRouteName: Routes.Cart,
    headerMode: 'none',
  },
);

export default CartNavigator;
