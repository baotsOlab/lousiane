import { createStackNavigator } from 'react-navigation';

import Home from '../containers/Home';
import HomeMenu from '../containers/HomeMenu';
import HomeMenuDetail from '../containers/HomeMenuDetail';
import HomeNearYou from '../containers/HomeNearYou';
import SearchLocation from '../containers/SearchLocation';
import Routes from './routes';
import NearYou from '../containers/NearYou';
import DishDetail from '../containers/DishDetail';

const HomeNavigator = createStackNavigator(
  {
    [Routes.Home]: {
      screen: Home
    },
    [Routes.HomeNearYou]: {
      screen: HomeNearYou
    },
    [Routes.HomeMenu]: {
      screen: HomeMenu
    },
    [Routes.HomeMenuDetail]: {
      screen: HomeMenuDetail
    },
    [Routes.DishDetail]: {
      screen: DishDetail
    },
    [Routes.SearchLocation]: {
      screen: SearchLocation
    },
    [Routes.NearYou]: {
      screen: NearYou
    },
  },
  {
    initialRouteName: Routes.HomeMenu,
    headerMode: 'none',
  },
);

export default HomeNavigator;
