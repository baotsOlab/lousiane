import { createStackNavigator } from 'react-navigation';

import HomeMenu from '../containers/HomeMenu';
import HomeMenuDetail from '../containers/HomeMenuDetail';
import SearchRestaurant from '../containers/SearchRestaurant';
import Routes from './routes';
import NearYou from '../containers/NearYou';
import DishDetail from '../containers/DishDetail';

const SearchNavigator = createStackNavigator(
  {
    [Routes.HomeMenu]: {
      screen: HomeMenu
    },
    [Routes.HomeMenuDetail]: {
      screen: HomeMenuDetail
    },
    [Routes.DishDetail]: {
      screen: DishDetail
    },
    [Routes.SearchRestaurant]: {
      screen: SearchRestaurant
    },
    [Routes.NearYou]: {
      screen: NearYou
    },
  },
  {
    initialRouteName: Routes.SearchRestaurant,
    headerMode: 'none',
  },
);

export default SearchNavigator;
