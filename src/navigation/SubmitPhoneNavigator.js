import { createStackNavigator } from 'react-navigation';


import EnterCode from '../containers/EnterCode';
import SubmitPhone from '../containers/SubmitPhone';
import Routes from './routes';

const SubmitPhoneNavigator = createStackNavigator(
  {
    [Routes.EnterCode]: {
      screen: EnterCode
    },
    [Routes.SubmitPhone]: {
      screen: SubmitPhone
    },
  },
  {
    initialRouteName: Routes.SubmitPhone,
    headerMode: 'none',
  },
);

export default SubmitPhoneNavigator;
