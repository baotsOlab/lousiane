export class NetworkError extends Error {
  static name = 'NetworkError';
  constructor(message) {
    super();
    this.message = message || 'Network request failed';
    this.name = NetworkError.name;
  }
}

export class ParseJsonError extends Error {
  static name = 'ParseJsonError';
  constructor() {
    super();
    this.message = 'Wrong data format';
    this.name = ParseJsonError.name;
  }
}

export class ServerError extends Error {
  static name = 'ServerError';
  constructor(message) {
    super();
    this.message = message || 'Oops, it\'s an error from us. Please make another attempt or contact our support line.';
    this.name = ServerError.name;
  }
}
