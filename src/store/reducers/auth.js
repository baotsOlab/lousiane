import fcm from '../../utils/fcm';

const types = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  SET_PROFILE: 'SET_PROFILE',
};

export const actions = {
  login: (token, user) => ({ type: types.LOGIN, payload: { token, user } }),
  logout: () => ({ type: types.LOGOUT }),
  setProfile: (user) => ({ type: types.SET_PROFILE, payload: { user } }),
};

const initialState = {
  loggedIn: false,
  token: undefined,
  user: undefined
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.LOGIN: {
      const { token, user } = payload;
      fcm.updateToken();
      return {
        ...state,
        loggedIn: true,
        token,
        user
      };
    }
    case types.LOGOUT: {
      return {
        ...state,
        loggedIn: false,
        token: undefined,
        user: undefined
      };
    }
    case types.SET_PROFILE: {
      const { user } = payload;
      return {
        ...state,
        user
      };
    }
    default:
      return state;
  }
};

export default reducer;
