import _ from 'lodash';

const types = {
  ADD_CART: 'ADD_CART',
  CLEAR_CART: 'CLEAR_CART',
  UPDATE_QUANTITY: 'UPDATE_QUANTITY',
  UPDATE_NOTE: 'UPDATE_NOTE',
  UPDATE_PRICE_CHANGES: 'UPDATE_PRICE_CHANGES',
  SET_ORDERER_ADDRESS: 'SET_ORDERER_ADDRESS',
};

export const QUANTITY_ACTION = {
  SUBTRACT: 'SUBTRACT',
  ADD: 'ADD'
};

export const actions = {
  /**
   * @param item {{ id, name, price, quantity, restaurant  }}
   * @returns {{type: string, payload: *}}
   */
  addCart: (item) => ({ type: types.ADD_CART, payload: item }),
  clearCart: () => ({ type: types.CLEAR_CART }),
  updateQuantity: (id, type) => ({ type: types.UPDATE_QUANTITY, payload: { id, type } }),
  updateNote: (restaurant, note) => ({ type: types.UPDATE_NOTE, payload: { restaurant, note } }),
  updatePriceChanges: (dishes) => ({ type: types.UPDATE_PRICE_CHANGES, payload: { dishes } }),
  /**
   * @param ordererAddress {{ address: string, lat: number, lng: number }}
   */
  setOrderAddress: (ordererAddress) =>
    ({ type: types.SET_ORDERER_ADDRESS, payload: { ordererAddress } }),
};

const initialState = {
  restaurants: [],
  items: [],
  total: 0,
  totalQuantity: 0,
};

export const existRestaurant = (id, restaurants) => {
  return !_.isNil(restaurants.find((item) => item.id === id));
};

const existItem = (id, items) => {
  return !_.isNil(items.find((item) => item.id === id));
};

const calculateTotal = (newRestaurants, newItems) => {
  const subTotal = [];
  let newTotal = 0;
  let newTotalQuantity = 0;
  newItems.forEach((item) => {
    const totalPrice = parseFloat(item.price) * parseInt(item.quantity, 10);
    newTotal += totalPrice;
    if (subTotal[item.restaurantId]) {
      subTotal[item.restaurantId] += totalPrice;
    } else {
      subTotal[item.restaurantId] = totalPrice;
    }

    newTotalQuantity += parseInt(item.quantity, 10);
  });

  newRestaurants = newRestaurants.filter((res) => {
    if (subTotal[res.id]) {
      res.subTotal = subTotal[res.id];
      return true;
    }
    return false;
  });

  return {
    newRestaurantsWithSubtotal: newRestaurants,
    newTotalQuantity,
    newTotal,
  };
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.ADD_CART: {
      const { restaurants, items } = state;
      const { id, name, price, quantity, restaurant } = payload;
      const newRestaurants = [...restaurants];
      let newItems = [...items];
      if (!existRestaurant(restaurant.id, newRestaurants)) {
        newRestaurants.push({
          id: restaurant.id,
          name: restaurant.name,
          address: restaurant.address,
          lat: restaurant.lat,
          lng: restaurant.lng,
          subTotal: 0,
          note: ''
        });
      }

      if (existItem(id, newItems)) {
        newItems = newItems.map((item) => {
          if (item.id === id) {
            item.quantity += quantity;
          }
          return item;
        });
      } else {
        newItems.push({ id, name, price, quantity, restaurantId: restaurant.id });
      }

      const {
        newTotal,
        newTotalQuantity,
        newRestaurantsWithSubtotal
      } = calculateTotal(newRestaurants, newItems);

      // console.log('newTotal', newTotal);
      // console.log('newTotalQuantity', newTotalQuantity);
      // console.log('newItems', newItems);
      // console.log('newRestaurants', newRestaurants);
      // console.log('newRestaurantsWithSubtotal', newRestaurantsWithSubtotal);

      return {
        ...state,
        total: newTotal,
        totalQuantity: newTotalQuantity,
        items: newItems,
        restaurants: newRestaurantsWithSubtotal
      };
    }
    case types.UPDATE_QUANTITY: {
      const { items, restaurants } = state;
      let newItems = [...items];
      const newRestaurants = [...restaurants];
      newItems = newItems.filter((item) => {
        if (item.id === payload.id) {
          if (payload.type === QUANTITY_ACTION.SUBTRACT) {
            if (item.quantity > 1) {
              item.quantity -= 1;
            } else {
              return false;
            }
          } else {
            item.quantity += 1;
          }
          return true;
        }
        return true;
      });

      const {
        newTotal,
        newTotalQuantity,
        newRestaurantsWithSubtotal
      } = calculateTotal(newRestaurants, newItems);

      return {
        ...state,
        total: newTotal,
        totalQuantity: newTotalQuantity,
        items: newItems,
        restaurants: newRestaurantsWithSubtotal
      };
    }
    case types.UPDATE_NOTE: {
      const { restaurants } = state;
      const newRestaurants = [...restaurants];
      const foundIndex = newRestaurants.findIndex(res => {
        return res.id === payload.restaurant.id;
      });
      if (foundIndex > -1) {
        newRestaurants[foundIndex].note = payload.note;
      }
      return {
        ...state,
        restaurants: newRestaurants
      };
    }
    case types.UPDATE_PRICE_CHANGES: {
      const { dishes } = payload;
      const { restaurants, items } = state;
      const newRestaurants = [...restaurants];
      let newItems = [...items];
      dishes.forEach(dish => {
        newItems = newItems.map((item) => {
          if (item.id === dish.id) {
            return { ...item, ...dish };
          }
          return item;
        });
      });
      const {
        newTotal,
        newTotalQuantity,
        newRestaurantsWithSubtotal
      } = calculateTotal(newRestaurants, newItems);
      return {
        ...state,
        total: newTotal,
        totalQuantity: newTotalQuantity,
        items: newItems,
        restaurants: newRestaurantsWithSubtotal
      };
    }
    case types.CLEAR_CART:
      return { ...state, ...initialState };
    case types.SET_ORDERER_ADDRESS: {
      const { ordererAddress } = payload;
      return {
        ...state,
        ordererAddress,
      };
    }
    default:
      return state;
  }
};

export default reducer;
