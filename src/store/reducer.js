import { combineReducers } from 'redux';
import cart from './reducers/cart';
import auth from './reducers/auth';

export default combineReducers({
  auth,
  cart
});
