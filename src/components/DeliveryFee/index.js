import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import I18n from '../../i18n';
import utils from '../../utils';

class DeliveryFee extends Component {
  _renderDeliveryFee = () => {
    const { total, deliveryFee, deliveryDistance } = this.props;
    const km = parseFloat(deliveryDistance).toFixed(2);
    return (
      <React.Fragment>
        <View style={styles.shipFeeContainer}>
          <Text style={styles.deliveryFee}>
            {I18n.t('deliveryFeeWithKm', { km })}
          </Text>
          <Text style={styles.feePrice}>
            ${utils.numberWithCommas(deliveryFee)}
          </Text>
        </View>
        <View style={styles.totalFeeContainer}>
          <Text style={styles.total}>{I18n.t('total')}</Text>
          <Text style={styles.totalMoney}>
            ${utils.numberWithCommas(total + deliveryFee)}
          </Text>
        </View>
      </React.Fragment>
    );
  };

  _renderGetDeliveryFee = () => {
    const { onGetDeliveryFee } = this.props;
    return (
      <TouchableOpacity onPress={onGetDeliveryFee} style={styles.getDeliveryFeeContainer}>
        <Text style={styles.getDeliveryFeeText}>Get delivery fee</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { getDeliveryFeeFailed } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.feeContainer}>
          {getDeliveryFeeFailed && this._renderGetDeliveryFee()}
          {!getDeliveryFeeFailed && this._renderDeliveryFee()}
        </View>
      </View>
    );
  }
}

DeliveryFee.propTypes = {
  total: PropTypes.number.isRequired,
  deliveryFee: PropTypes.number.isRequired,
  deliveryDistance: PropTypes.number.isRequired,
  getDeliveryFeeFailed: PropTypes.bool.isRequired,
  onGetDeliveryFee: PropTypes.func.isRequired
};
export default DeliveryFee;
