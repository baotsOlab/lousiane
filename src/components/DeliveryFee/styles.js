import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.TRANSPARENT,
    width: deviceWidth - 20,
    alignSelf: 'center'
  },
  feeContainer: {
    backgroundColor: Colors.WHITE,
  },
  shipFeeContainer: {
    marginTop: 20,
    flexDirection: 'row',
  },
  deliveryFee: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY3,
    marginLeft: 10,
  },
  feePrice: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    textAlign: 'right',
    marginRight: 10,
  },
  totalFeeContainer: {
    marginTop: 33,
    flexDirection: 'row',
  },
  total: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_GREEN1,
    marginLeft: 10,
    marginBottom: 22,
  },
  totalMoney: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.BOLD,
    fontSize: 20,
    color: Colors.OLIVERY_GREEN1,
    textAlign: 'right',
    marginRight: 10,
  },
  paymentContainer: {
    backgroundColor: Colors.WHITE,
    marginTop: 4,
  },
  warningPayment: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    marginLeft: 10,
    marginTop: 20,
  },
  oWalletContainer: {
    marginTop: 20,
    flexDirection: 'row',
  },
  oWallet: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY3,
    marginLeft: 10,
  },
  oWalletMoney: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    textAlign: 'right',
    marginRight: 10,
  },
  stripeContainer: {
    marginTop: 10,
    flexDirection: 'row',
    marginBottom: 20,
  },
  paymentStripe: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY3,
    marginLeft: 10,
  },
  stripeMoney: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    textAlign: 'right',
    marginRight: 10,
  },
  getDeliveryFeeContainer: {
    padding: 12,
    alignItems: 'center',
    justifyContent: 'center'
  },
  getDeliveryFeeText: {
    fontSize: 18,
    fontFamily: Fonts.MEDIUM,
    color: Colors.OLIVERY_RED1
  }
});
