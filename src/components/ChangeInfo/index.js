import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import platformHelper from '../../utils/platform';
import { getNumberOnly } from '../../utils/string';
import Colors from '../../common/Colors';

class ChangeInfo extends Component {
  constructor(props) {
    super(props);
    const { value, phoneNumber } = props;
    let transformValue = value;
    if (phoneNumber) {
      transformValue = value || '+';
    }
    this.state = { value: transformValue };
  }

  _apply = () => {
    const { value } = this.state;
    const { onApply } = this.props;
    onApply(value);
  };

  _onChangeText = (key) => (value) => {
    const { phoneNumber } = this.props;
    let transformValue = value;
    if (phoneNumber) {
      transformValue = `+${getNumberOnly(value)}`;
    }
    this.setState({ [key]: transformValue });
  };

  _checkToDisable = (value) => {
    const { phoneNumber } = this.props;
    if (phoneNumber && value.length <= 10) {
      return true;
    }
    return value.trim().length === 0;
  };

  render() {
    const { onClose, placeholder, textInputProps, title, applyText } = this.props;
    const { value } = this.state;
    const behavior = platformHelper.isIOS ? 'padding' : null;
    const disabled = this._checkToDisable(value);
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior={behavior}>
          <View style={styles.editContainer}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={onClose}>
                <Text style={styles.cancel}>Cancel</Text>
              </TouchableOpacity>
              <Text style={styles.title}>{title}</Text>
              <TouchableOpacity onPress={this._apply} disabled={disabled}>
                <Text style={[styles.apply, disabled && styles.disabled]}>
                  {applyText}
                </Text>
              </TouchableOpacity>
            </View>
            <TextInput
              style={styles.nameInput}
              autoFocus={true}
              returnKeyType='done'
              underlineColorAndroid={Colors.TRANSPARENT}
              placeholder={placeholder}
              placeholderTextColor={Colors.OLIVERY_GRAY3}
              onChangeText={this._onChangeText('value')}
              value={value}
              {...textInputProps}
            />
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
ChangeInfo.propTypes = {
  onApply: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  applyText: PropTypes.string,
  textInputProps: PropTypes.object,
  phoneNumber: PropTypes.bool
};

ChangeInfo.defaultProps = {
  value: '',
  placeholder: '',
  textInputProps: {},
  applyText: 'Apply',
  phoneNumber: false
};

export default ChangeInfo;
