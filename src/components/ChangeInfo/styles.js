import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK_OPACITY40,
    justifyContent: 'flex-end',
  },
  editContainer: {
    backgroundColor: Colors.WHITE,
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 21,
  },
  cancel: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    marginLeft: 10,
  },
  title: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    flex: 1,
    textAlign: 'center',
  },
  apply: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GREEN1,
    marginRight: 10,
  },
  disabled: {
    color: Colors.OLIVERY_GRAY1
  },
  nameInput: {
    height: 40,
    width: deviceWidth - 20,
    alignSelf: 'center',
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
    marginTop: 12,
    marginBottom: 12,
  },
});
