import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import styles from './styles';

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { title = '', color = '#000' } = this.props;
    return (
      <View style={styles.container}>
        <ActivityIndicator
          size='large'
          color={color}
          animating
        />
        <Text>{ title }</Text>
      </View>
    );
  }
}
export default Loading;
