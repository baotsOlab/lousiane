import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
    width: deviceWidth - 20,
    alignSelf: 'center',
    marginTop: 20,
  },
  editImage: {
    marginHorizontal: 10,
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  itemContainer: {
    paddingVertical: 20,
    flexDirection: 'row',
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
  },
  itemTitle: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    marginLeft: 10,
  },
  itemContent: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    textAlign: 'right'
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 14,
    marginVertical: 16
  }
});
