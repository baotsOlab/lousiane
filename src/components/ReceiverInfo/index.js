import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Images from '../../assets/Images';
import styles from './styles';
import I18n from '../../i18n';

class ReceiverInfo extends Component {
  _renderItem = (title, content, onEdit) => {
    return (
      <TouchableOpacity style={styles.itemContainer} onPress={onEdit}>
        <Text style={styles.itemTitle}>
          {title}
        </Text>
        <Text style={styles.itemContent}>
          {content}
        </Text>
        <Image style={styles.editImage} source={Images.edit}/>
      </TouchableOpacity>
    );
  };

  render() {
    const {
      name,
      phone,
      address,
      onChangeName,
      onChangePhone,
      onChangeAddress
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>RECEIVER INFORMATION</Text>
        </View>
        {this._renderItem(I18n.t('name'), name, onChangeName)}
        {this._renderItem(I18n.t('mobile'), phone, onChangePhone)}
        {this._renderItem(I18n.t('address'), address, onChangeAddress)}
      </View>
    );
  }
}

ReceiverInfo.propTypes = {
  name: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  onChangeName: PropTypes.func.isRequired,
  onChangePhone: PropTypes.func.isRequired,
  onChangeAddress: PropTypes.func.isRequired,
};
export default ReceiverInfo;
