import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import Background from '../Background';
import SolidButton from '../SolidButton';
import Images from '../../assets/Images';
import styles from './styles';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import WithLoading from '../../hoc/WithLoading';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrCategories: [],
      arrZone: []
    };
  }

  componentDidMount() {
    this.fetchCategoriesAndZones();
  }

  fetchCategoriesAndZones = async () => {
    const { showLoading } = this.props;
    showLoading(true);
    try {
      const [zones, categories] = await Promise.all([api.getZones(), api.getCategories()]);
      this.setState({
        arrZone: zones,
        arrCategories: categories
      });
    } catch (error) {
      errorManager.createError('Getting categories and zones', error);
    } finally {
      showLoading(false);
    }
  };

  clickCategories = (index) => {
    const arr = this.state.arrCategories;
    arr[index].check = !this.state.arrCategories[index].check;
    this.setState({
      arrCategories: arr
    });
  };

  renderCategories = () => {
    return this.state.arrCategories.map((category, index) => {
      if (category.check === true) {
        return <TouchableOpacity
          key={index}
          onPress={() => {
            this.clickCategories(index);
          }}
          style={styles.choosenButton}>
          <Text style={styles.choosenTextColor}>
            {category.name}
          </Text>
        </TouchableOpacity>;
      }
      return <TouchableOpacity
          key={index}
          onPress={() => {
            this.clickCategories(index);
          }}
          style={styles.button}>
          <Text style={styles.textColor}>
            {category.name}
          </Text>
        </TouchableOpacity>;
    });
  };

  clickZones = (index) => {
    const arr = this.state.arrZone;
    arr[index].check = !this.state.arrZone[index].check;
    this.setState({
      arrZone: arr
    });
  };

  renderZones = () => {
    return this.state.arrZone.map((zone, index) => {
      if (zone.check === true) {
        return <TouchableOpacity
          key={index}
          onPress={() => {
            this.clickZones(index);
          }}
          style={styles.choosenButton}>
          <Text style={styles.choosenTextColor}>
            {zone.name}
          </Text>
        </TouchableOpacity>;
      }
      return <TouchableOpacity
          key={index}
          onPress={() => {
            this.clickZones(index);
          }}
          style={styles.button}>
          <Text style={styles.textColor}>
            {zone.name}
          </Text>
        </TouchableOpacity>;
    });
  };

  clearAllFilter = () => {
    const arrCategories = this.state.arrCategories.map((category) => {
      category.check = false;
      return category;
    });
    const arrZone = this.state.arrZone.map((zone) => {
      zone.check = false;
      return zone;
    });
    this.setState({ arrZone, arrCategories });
  };

  clickApplyFilter = () => {
    const { arrCategories, arrZone } = this.state;
    const arr3 = arrCategories.filter((category) => category.check === true);
    const arr4 = arrZone.filter((zone) => zone.check === true);
    this.props.closeModal(arr3, arr4, arr3.length + arr4.length);
  };

  render() {
    return (
      <Background>
        <View style={styles.header}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              this.props.onCloseModal();
            }}
            style={styles.wrapCancelIcion}>
            <Image
              style={styles.closeIcon}
              source={Images.closeIcon}
            />
          </TouchableOpacity>
          <View style={styles.wrapTitle}>
            <Text style={styles.textTitle}>FILTER</Text>
          </View>
          <TouchableOpacity
            style={styles.wrapClearAll}
            activeOpacity={0.99}
            onPress={() => {
              this.clearAllFilter();
            }}
          >
            <Text style={styles.textClear}>Clear all</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.wrapContent}>
          <Text style={styles.boldTitle}>Categories</Text>
          <View style={styles.buttonLayout}>
            {this.renderCategories()}
          </View>
          <Text style={styles.boldTitle}>Zone</Text>
          <View style={styles.buttonLayout}>
            {this.renderZones()}
          </View>
        </View>
        <View style={styles.wrapApplyBtn}>
          <SolidButton
            text='APPLY FILTER'
            onPress={() => {
              this.clickApplyFilter();
            }}
            style={styles.applyButton}
          />
        </View>
      </Background>
    );
  }
}

export default WithLoading(Filter);
