import { StyleSheet, Platform } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    paddingTop: Platform.OS === 'ios' ? 30 : 10,
    paddingBottom: 15,
    paddingHorizontal: 10,
    height: Platform.OS === 'ios' ? 66 : 46,
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  wrapTitle: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    color: Colors.WHITE,
    fontFamily: Fonts.BOLD,
    fontSize: 17
  },
  wrapClearAll: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  textClear: {
    color: Colors.WHITE,
    fontFamily: Fonts.REGULAR,
    fontSize: 14
  },
  wrapCancelIcion: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  closeIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain'
  },
  applyButton: {
    marginBottom: 20,
  },
  wrapApplyBtn: {
    flex: 2,
    justifyContent: 'flex-end',
    paddingHorizontal: 10
  },
  wrapContent: {
    flex: 8,
    marginHorizontal: 10,
    marginTop: 20,
  },
  boldTitle: {
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.MEDIUM,
    fontSize: 16
  },
  button: {
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 80,
    marginHorizontal: 5,
    borderColor: Colors.OLIVERY_GRAY3,
    borderWidth: 1,
    marginBottom: 10,
  },
  buttonLayout: {
    flexDirection: 'row',
    marginLeft: -5,
    marginTop: 12,
    marginBottom: 30,
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  choosenButton: {
    backgroundColor: Colors.OLIVERY_GREEN1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 80,
    marginHorizontal: 5,
    borderColor: Colors.OLIVERY_GREEN1,
    borderWidth: 1,
    marginBottom: 10,
  },
  choosenTextColor: {
    color: Colors.WHITE,
    fontFamily: Fonts.REGULAR,
    fontSize: 14
  },
  textColor: {
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR,
    fontSize: 14
  }
});
