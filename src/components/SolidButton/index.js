import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

class SolidButton extends Component {
  render() {
    const { text = '', style, disabled, onPress } = this.props;
    return (
      <TouchableOpacity
        style={[styles.button, style, disabled && styles.buttonDisabled]}
        onPress={onPress}
        disabled={disabled}
      >
        <Text style={styles.title}>{text.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }
}

SolidButton.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  fontSize: PropTypes.number,
  bold: PropTypes.bool,
  textColor: PropTypes.string,
  disabled: PropTypes.bool
};

SolidButton.defaultProps = {
  disabled: false
};

export default SolidButton;
