import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  button: {
    // backgroundColor: Colors.OLIVERY_GREEN1,
    backgroundColor: Colors.OLIVERY_YELLOW,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    height: 50,
    width: '100%',
    borderWidth: 0.5,
    borderColor: Colors.BUTTON_TEXT_COLOR
  },
  title: {
    textAlign: 'center',
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 18,
    fontFamily: Fonts.BOLD,
  },
  buttonDisabled: {
    backgroundColor: Colors.OLIVERY_GRAY1,
  }
});
