import React, { Component } from 'react';
import {
  View,
  StatusBar,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Colors from '../../common/Colors';

class Background extends Component {
  render() {
    const { children } = this.props;
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <StatusBar
          barStyle='light-content'
          translucent
          backgroundColor={Colors.TRANSPARENT}
        />
        {children}
      </View>
    );
  }
}


Background.propTypes = {
  children: PropTypes.any,
  containerStyle: PropTypes.object
};
export default Background;
