import React, { Component } from 'react';
import {
  View,
  TextInput,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const KEY = {
  BACKSPACE: 'Backspace'
};

class CodeInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text1: '',
      text2: '',
      text3: '',
      text4: '',
    };
  }

  _getTextColor = (text) => {
    return text.length === 0 ? '#CED4DA' : '#004F29';
    // return text.length === 0 ? '#CED4DA' : '#94C93D';
  };

  _onKeyPress = (textInputToFocus) => (e) => {
    if (e.nativeEvent.key === KEY.BACKSPACE) {
      // Return if duration between previous key press and backspace is less than 20ms
      if (Math.abs(this.lastKeyEventTimestamp - e.timeStamp) < 20) return;
      if (textInputToFocus) {
        textInputToFocus.focus();
      }
    } else {
      // Record non-backspace key event time stamp
      this.lastKeyEventTimestamp = e.timeStamp;
    }
  };

  _onFocus = (key, value) => () => {
    this.setState({ [key]: value });
  };

  render() {
    const { container, onInputChange } = this.props;
    const { text1, text2, text3, text4 } = this.state;
    const text1Color = this._getTextColor(text1);
    const text2Color = this._getTextColor(text2);
    const text3Color = this._getTextColor(text3);
    const text4Color = this._getTextColor(text4);
    const commonProps = {
      maxLength: 1,
      underlineColorAndroid: 'transparent',
      keyboardType: 'numeric',
      style: styles.textInput,
    };
    return (
      <View style={[styles.container, container]}>
        <View style={[styles.inputContainer, { borderColor: text1Color }]}>
          <TextInput
            ref={(ref) => { this.textInput1 = ref; }}
            {...commonProps}
            autoFocus={true}
            value={text1}
            onChangeText={text => {
              this.setState({ text1: text });
              if (text.length > 0 && text2.length === 0) { this.textInput2.focus(); }
              const code = text + text2 + text3 + text4;
              onInputChange(code);
            }}
            onFocus={this._onFocus('text1', '')}
          />
        </View>
        <View style={[styles.inputContainer, { borderColor: text2Color }]}>
          <TextInput
            ref={(ref) => { this.textInput2 = ref; }}
            {...commonProps}
            value={text2}
            onChangeText={text => {
              this.setState({ text2: text });
              if (text.length > 0 && text3.length === 0) { this.textInput3.focus(); }
              const code = text1 + text + text3 + text4;
              onInputChange(code);
            }}
            onFocus={this._onFocus('text2', '')}
            onKeyPress={this._onKeyPress(this.textInput1)}
          />
        </View>
        <View style={[styles.inputContainer, { borderColor: text3Color }]}>
          <TextInput
            ref={(ref) => { this.textInput3 = ref; }}
            {...commonProps}
            value={text3}
            onChangeText={text => {
              this.setState({ text3: text });
              if (text.length > 0 && text4.length === 0) { this.textInput4.focus(); }
              const code = text1 + text2 + text + text4;
              onInputChange(code);
            }}
            onFocus={this._onFocus('text3', '')}
            onKeyPress={this._onKeyPress(this.textInput2)}
          />
        </View>
        <View style={[styles.inputContainer, { borderColor: text4Color }]}>
          <TextInput
            ref={(ref) => { this.textInput4 = ref; }}
            {...commonProps}
            value={text4}
            onChangeText={text => {
              this.setState({ text4: text });
              const code = text1 + text2 + text3 + text;
              onInputChange(code);
            }}
            onFocus={this._onFocus('text4', '')}
            onKeyPress={this._onKeyPress(this.textInput3)}
          />
        </View>
      </View>
    );
  }
}

CodeInput.propTypes = {
  onInputChange: PropTypes.func.isRequired
};

export default CodeInput;
