import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';
import { scaleFont, scaleHorizontal, scaleVertical } from '../../utils/size';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: scaleHorizontal(300),
  },
  inputContainer: {
    height: scaleHorizontal(50),
    width: scaleVertical(45),
    borderColor: Colors.OLIVERY_GRAY1,
    borderRadius: 5,
    borderWidth: 1
  },
  textInput: {
    flex: 1,
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    fontSize: scaleFont(20),
    textAlign: 'center',
  },
});
