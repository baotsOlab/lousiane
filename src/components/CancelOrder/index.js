import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import I18n from '../../i18n';

export default class CancelOrder extends Component {
  static propTypes = {
    onPressOk: PropTypes.func.isRequired,
    onPressCancel: PropTypes.func.isRequired,
  };

  render() {
    const { onPressOk, onPressCancel } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.alertContainer}>
          <Text style={styles.title}>{I18n.t('questionCancelOrder')}</Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.okButton} onPress={onPressOk}>
              <Text style={styles.okText}>{I18n.t('ok')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton} onPress={onPressCancel}>
              <Text style={styles.cancelText}>{I18n.t('cancel')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
