import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK_OPACITY40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  alertContainer: {
    width: deviceWidth - 20,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    alignItems: 'center'
  },
  title: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginTop: 30,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30
  },
  okButton: {
    width: 94,
    height: 36,
    marginHorizontal: 5,
    backgroundColor: Colors.WHITE,
    borderColor: Colors.OLIVERY_GREEN1,
    borderWidth: 1,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center'
  },
  okText: {
    fontFamily: Fonts.BOLD,
    fontSize: 14,
    color: Colors.OLIVERY_GREEN1
  },
  cancelButton: {
    width: 94,
    height: 36,
    marginHorizontal: 5,
    borderRadius: 18,
    backgroundColor: Colors.OLIVERY_GREEN1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  cancelText: {
    fontFamily: Fonts.BOLD,
    fontSize: 14,
    color: Colors.WHITE
  },
});
