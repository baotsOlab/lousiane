import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import { QUANTITY_ACTION } from '../../store/reducers/cart';
import utils from '../../utils';

export default class OrderFood extends Component {
  _sub = () => {
    const { item, onQuantityChanged } = this.props;
    onQuantityChanged(item.id, QUANTITY_ACTION.SUBTRACT);
  };

  _add = () => {
    const { item, onQuantityChanged } = this.props;
    onQuantityChanged(item.id, QUANTITY_ACTION.ADD);
  };

  render() {
    const { item } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.minusPlusContainer}>
          <TouchableOpacity style={styles.minusButton} onPress={this._sub}>
            <Text style={styles.minusText}>–</Text>
          </TouchableOpacity>
          <Text style={styles.number}>
            {item.quantity}
          </Text>
          <TouchableOpacity style={styles.plusButton} onPress={this._add}>
            <Text style={styles.plusText}>+</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.content}>
          <Text style={styles.foodName} numberOfLines={1}>
            {item.name}
          </Text>
          <View style={styles.numberPriceContainer}>
            <Text style={styles.price}>
              {`${utils.numberWithCommas(item.price)} x ${item.quantity}`}
            </Text>
            <Text style={styles.totalMoney}>
              ${utils.numberWithCommas(item.price * item.quantity)}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
