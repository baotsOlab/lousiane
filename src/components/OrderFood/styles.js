import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 20,
    flexDirection: 'row',
  },
  minusPlusContainer: {
    height: 38,
    width: 91,
    marginLeft: 10,
    borderRadius: 19,
    borderWidth: 1,
    borderColor: Colors.OLIVERY_GREEN1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  minusButton: {
    width: 26,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  minusText: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_GREEN1,
    textAlign: 'right'
  },
  number: {
    fontFamily: Fonts.BOLD,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    textAlign: 'center'
  },
  plusButton: {
    width: 26,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  plusText: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_GREEN1,
    textAlign: 'left'
  },
  content: {
    flex: 1,
    marginLeft: 15,
    marginRight: 10,
  },
  foodName: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
  },
  numberPriceContainer: {
    marginTop: 2,
    flexDirection: 'row'
  },
  price: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    textAlign: 'left'
  },
  totalMoney: {
    fontFamily: Fonts.BOLD,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    flex: 1,
    textAlign: 'right',
  },
});
