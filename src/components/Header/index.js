import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity
} from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Images from '../../assets/Images';
import styles from './styles';
import Colors from '../../common/Colors';

class Header extends Component {
  renderTitle() {
    const { title } = this.props;
    if (title == null || title === '') {
      return null;
    }
    return (<Text style={styles.headerTitle}>{title}</Text>);
  }

  renderRightButton() {
    const { rightButton, onRightButtonPress, renderRight } = this.props;
    if (_.isFunction(renderRight)) {
      return renderRight();
    }
    switch (rightButton) {
      case 'calling':
        return (
          <TouchableOpacity onPress={onRightButtonPress} style={styles.rightButton}>
            <Image style={styles.rightImage} source={Images.calling}/>
          </TouchableOpacity>
        );
      default:
        return this._renderEmpty();
    }
  }

  renderBackButton() {
    if (this.props.hideBackButton) {
      return this._renderEmpty();
    }
    return (
      <TouchableOpacity style={styles.leftButton} onPress={this.props.onTouchBackButton}>
        <Image style={styles.leftImage} source={Images.back}/>
      </TouchableOpacity>
    );
  }

  _renderEmpty = () => {
    const { title } = this.props;
    if (title == null || title === '') { return null; }
    return (<View style={styles.empty}/>);
  };

  render() {
    const { shouldTransparent } = this.props;
    const backgroundColor = shouldTransparent ? Colors.TRANSPARENT : Colors.OLIVERY_GREEN1;
    return (
      <View style={[styles.container, { backgroundColor }]}>
        <View style={styles.containerContent}>
          {this.renderBackButton()}
          {this.props.children}
          {this.renderTitle()}
          {this.renderRightButton()}
        </View>
      </View>
    );
  }
}

Header.propTypes = {
  shouldTransparent: PropTypes.bool,
  hideBackButton: PropTypes.bool,
  onTouchBackButton: PropTypes.func
};

Header.defaultProps = {
  shouldTransparent: false,
  hideBackButton: false,
  onTouchBackButton: () => {}
};

export default Header;
