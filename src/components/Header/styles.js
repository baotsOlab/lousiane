import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    height: 66,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: Colors.OLIVERY_GREEN1,
  },
  containerContent: {
    backgroundColor: Colors.TRANSPARENT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 8,
  },
  leftButton: {
    backgroundColor: Colors.TRANSPARENT,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    paddingLeft: 5,
    paddingRight: 15
  },
  leftImage: {
    width: 24,
    height: 20,
    resizeMode: 'contain'
  },
  rightButton: {
    backgroundColor: Colors.TRANSPARENT,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 12,
    minWidth: 24,
  },
  rightImage: {
    width: 22,
    height: 22,
    resizeMode: 'contain'
  },
  headerTitle: {
    flex: 1,
    fontSize: 17,
    textAlign: 'center',
    color: Colors.WHITE,
    fontFamily: Fonts.BOLD,
    backgroundColor: Colors.TRANSPARENT,
  },
  empty: {
    width: 48,
    height: 48
  },
});
