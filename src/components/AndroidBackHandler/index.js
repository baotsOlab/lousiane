import { Component } from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import platformHelper from '../../utils/platform';
import { HARDWARE_BACK_PRESS } from '../../assets/constants/constant';

class AndroidBackHandler extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    if (platformHelper.isAndroid) {
      this._didFocusSubscription = props.navigation.addListener('didFocus', () => {
        BackHandler.addEventListener(HARDWARE_BACK_PRESS, props.onBack);
      });
    }
  }

  componentDidMount() {
    if (platformHelper.isAndroid) {
      const { onBack, navigation } = this.props;
      this._willBlurSubscription = navigation.addListener('willBlur', () => {
        BackHandler.removeEventListener(HARDWARE_BACK_PRESS, onBack);
      });
    }
  }

  componentWillUnmount() {
    if (this._didFocusSubscription) {
      this._didFocusSubscription.remove();
    }
    if (this._willBlurSubscription) {
      this._willBlurSubscription.remove();
    }
  }

  render() {
    return null;
  }
}

AndroidBackHandler.propTypes = {
  onBack: PropTypes.func.isRequired
};
export default AndroidBackHandler;
