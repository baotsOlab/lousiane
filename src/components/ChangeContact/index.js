import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import platformHelper from '../../utils/platform';
import Colors from '../../common/Colors';
import I18n from '../../i18n';

class ChangeContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactName: props.contactName,
      contactPhone: props.contactPhone
    };
  }

  _apply = () => {
    const { contactName, contactPhone } = this.state;
    const { onApply } = this.props;
    onApply(contactName, contactPhone);
  };

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value });
  };

  render() {
    const { onClose } = this.props;
    const { contactName, contactPhone } = this.state;
    const behavior = platformHelper.isIOS ? 'padding' : null;
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior={behavior}>
          <View style={styles.editContainer}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={onClose}>
                <Text style={styles.cancel}>{I18n.t('cancel')}</Text>
              </TouchableOpacity>
              <Text style={styles.title}>{I18n.t('contactInfo')}</Text>
              <TouchableOpacity onPress={this._apply}>
                <Text style={styles.apply}>{I18n.t('apply')}</Text>
              </TouchableOpacity>
            </View>
            <TextInput
              style={styles.nameInput}
              autoFocus={true}
              returnKeyType='done'
              underlineColorAndroid={Colors.TRANSPARENT}
              placeholder={I18n.t('name')}
              placeholderTextColor={Colors.OLIVERY_GRAY3}
              onChangeText={this._onChangeText('contactName')}
              value={contactName}
            />
            <TextInput
              style={styles.phoneInput}
              autoFocus={false}
              returnKeyType='done'
              underlineColorAndroid={Colors.TRANSPARENT}
              placeholder={I18n.t('phoneNumber')}
              placeholderTextColor={Colors.OLIVERY_GRAY3}
              keyboardType={'phone-pad'}
              onChangeText={this._onChangeText('contactPhone')}
              value={contactPhone}
            />
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
ChangeContact.propTypes = {
  onApply: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  contactName: PropTypes.string,
  contactPhone: PropTypes.string,
};

ChangeContact.defaultProps = {
  contactName: '',
  contactPhone: ''
};

export default ChangeContact;
