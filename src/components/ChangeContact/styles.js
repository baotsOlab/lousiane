import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK_OPACITY40,
    justifyContent: 'flex-end',
  },
  editContainer: {
    backgroundColor: Colors.WHITE,
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 21,
  },
  cancel: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    marginLeft: 10,
  },
  title: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    flex: 1,
    textAlign: 'center',
  },
  apply: {
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GREEN1,
    marginRight: 10,
  },
  nameInput: {
    height: 33,
    width: deviceWidth - 20,
    alignSelf: 'center',
    marginTop: 32,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
  },
  phoneInput: {
    height: 33,
    width: deviceWidth - 20,
    alignSelf: 'center',
    marginTop: 22,
    marginBottom: 40,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
  },
});
