import { StyleSheet } from 'react-native';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default StyleSheet.create({
  itemContainer: {
    paddingVertical: 20,
    flexDirection: 'row',
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
    backgroundColor: Colors.WHITE,
    marginHorizontal: 10
  },
  itemTitle: {
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    marginLeft: 10,
  },
  itemContent: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
    textAlign: 'right'
  },
  editImage: {
    marginHorizontal: 10,
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  errorContainer: {
    paddingVertical: 20,
    flexDirection: 'row',
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
    backgroundColor: Colors.WHITE,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  errorText: {
    fontSize: 18,
    fontFamily: Fonts.MEDIUM,
    color: Colors.OLIVERY_RED1
  }
});
