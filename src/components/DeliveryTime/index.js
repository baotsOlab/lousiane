import React, { Component } from 'react';
import { Text, Image, TouchableOpacity } from 'react-native';
import { compose } from 'recompose';
import Picker from 'react-native-picker';
import moment from 'moment';
import _ from 'lodash';
import PropTypes from 'prop-types';
import styles from './styles';
import Images from '../../assets/Images';
import i18n from '../../i18n';
import Fonts from '../../common/Fonts';
import api from '../../services/api';
import WithLoading from '../../hoc/WithLoading';
import { STATUS } from '../../assets/constants/constant';
import errorManager from '../../services/errorManager';

const ASAP_TEXT = 'As soon as possible';
const ASAP_VALUES = [''];
const DEFAULT_DELIVERY_TIME = [ASAP_TEXT, ASAP_VALUES[0]];
const ASAP_OPTION = { [ASAP_TEXT]: ASAP_VALUES };

const DELIVERY_TIME_DATE_FORMAT = 'ddd MMMM DD';
const DELIVERY_TIME_TIME_FORMAT = 'HH:mm';
const DELIVERY_TIME_FORMAT = `${DELIVERY_TIME_DATE_FORMAT} ${DELIVERY_TIME_TIME_FORMAT}`;

const deliveryPickerConfig = {
  pickerConfirmBtnText: 'Apply',
  pickerCancelBtnText: 'Cancel',
  pickerTitleText: 'DELIVERY TIME',
  pickerConfirmBtnColor: [148, 200, 61, 1],
  pickerCancelBtnColor: [134, 142, 150, 1],
  pickerTitleColor: [52, 58, 64, 1],
  pickerToolBarBg: [255, 255, 255, 1],
  pickerBg: [255, 255, 255, 1],
  pickerToolBarFontSize: 14,
  pickerFontSize: 14,
  pickerFontColor: [148, 200, 61, 1],
  pickerFontFamily: Fonts.REGULAR,
  pickerTextEllipsisLen: 20,
  pickerRowHeight: 37,
};

/* eslint-disable react/no-deprecated */
class DeliveryTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: STATUS.NONE,
      deliveryTime: DEFAULT_DELIVERY_TIME,
      deliveryTimeForPicker: [ASAP_OPTION],
    };
  }

  componentWillReceiveProps(nextProps) {
    const { restaurants } = this.props;
    if (!_.isEqual(restaurants, nextProps.restaurants)) {
      this._getDeliveryTime(nextProps);
    }
  }

  componentDidMount() {
    this._getDeliveryTime();
  }

  _resetToASAP = () => {
    const { onChangeTime } = this.props;
    this.setState({ deliveryTimeForPicker: [ASAP_OPTION], deliveryTime: DEFAULT_DELIVERY_TIME });
    onChangeTime(this._getISOStringFromTime(DEFAULT_DELIVERY_TIME));
  };

  _getDeliveryTime = async (props = this.props) => {
    this._resetToASAP();
    const { restaurants = [], showLoading, onChangeStatus } = props;
    showLoading(true);
    try {
      const deliveryTimes = await api.getDeliveryTime({
        restaurants: restaurants.map(res => res.id)
      });
      const deliveryTimeForPicker = {};
      deliveryTimes.forEach(deliveryTime => {
        const deliveryTimeMoment = moment(deliveryTime);
        const dateLabel = deliveryTimeMoment.format(DELIVERY_TIME_DATE_FORMAT);
        if (!deliveryTimeForPicker[dateLabel]) {
          deliveryTimeForPicker[dateLabel] = [];
        }
        deliveryTimeForPicker[dateLabel].push(deliveryTimeMoment.format(DELIVERY_TIME_TIME_FORMAT));
      });
      this.setState({
        status: STATUS.SUCCESS,
        deliveryTimeForPicker: [
          ASAP_OPTION,
          ...Object.keys(deliveryTimeForPicker).map(key => {
            return { [key]: deliveryTimeForPicker[key] };
          })
        ]
      });
      onChangeStatus(STATUS.SUCCESS);
    } catch (error) {
      errorManager.createError('Delivery Time', error);
      this.setState({ status: STATUS.FAILED });
      onChangeStatus(STATUS.FAILED);
    } finally {
      showLoading(false);
    }
  };

  _onEdit = () => {
    const { onChangeTime, onShowPicker } = this.props;
    const { deliveryTimeForPicker, deliveryTime } = this.state;
    onShowPicker(true);
    Picker.init({
      ...deliveryPickerConfig,
      pickerData: deliveryTimeForPicker,
      selectedValue: deliveryTime,
      onPickerConfirm: (time) => {
        onChangeTime(this._getISOStringFromTime(time));
        onShowPicker(false);
        this.setState({ deliveryTime: time });
      },
      onPickerCancel: () => {
        onShowPicker(false);
      },
    });
    Picker.show();
  };

  _getISOStringFromTime = (time) => {
    let selectedDeliveryTime;
    if (time.length === 2) {
      // As soon as possible is empty
      if (time[1].trim().length > 0) {
        const selectedMoment = moment(time.join(' '), DELIVERY_TIME_FORMAT);
        selectedDeliveryTime = selectedMoment.toISOString();
      }
    }
    return selectedDeliveryTime;
  };

  render() {
    const { deliveryTime, status } = this.state;
    if (status === STATUS.SUCCESS) {
      return (
        <TouchableOpacity style={styles.itemContainer} onPress={this._onEdit}>
          <Text style={styles.itemTitle}>{i18n.t('deliveryTime')}</Text>
          <Text style={styles.itemContent}>{deliveryTime.join(' ')}</Text>
          <Image style={styles.editImage} source={Images.edit}/>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity style={styles.errorContainer} onPress={() => this._getDeliveryTime()}>
        <Text style={styles.errorText}>Tap here to get delivery time</Text>
      </TouchableOpacity>
    );
  }
}
DeliveryTime.propTypes = {
  restaurants: PropTypes.array.isRequired,
  onChangeTime: PropTypes.func.isRequired,
  onChangeStatus: PropTypes.func.isRequired,
  onShowPicker: PropTypes.func.isRequired,
};

const enhancer = compose(WithLoading);
export default enhancer(DeliveryTime);
