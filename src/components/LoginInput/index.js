import React, { Component } from 'react';
import {
  View,
  TextInput,
  Image
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Images from '../../assets/Images';
import Colors from '../../common/Colors';

class LoginInput extends Component {
  static propTypes = {
    icon: PropTypes.string,
    placeholderText: PropTypes.string,
    keyboardType: PropTypes.string,
    onChangeText: PropTypes.func,
    autoCorrect: PropTypes.bool,
    secureTextEntry: PropTypes.bool,
    value: PropTypes.string,
    onSubmit: PropTypes.func,
    returnKeyType: PropTypes.oneOf(['done', 'go', 'next', 'search', 'send']),
    autoCapitalize: PropTypes.oneOf(['none', 'sentences', 'words', 'characters'])
  };

  static defaultProps = {
    icon: undefined,
  };

  componentDidMount() {
    if (this.props.onRef) {
      this.props.onRef(this);
    }
  }

  componentWillUnmount() {
    if (this.props.onRef) {
      this.props.onRef(undefined);
    }
  }

  focus() {
    this.textInput.focus();
  }

  getImage(key) {
    switch (key) {
      case 'email':
        return Images.email;
      case 'password':
        return Images.password;
      case 'phone':
        return Images.phone;
      case 'user':
        return Images.user;
      default:
        return undefined;
    }
  }

  render() {
    const {
      style, placeholderText, inputStyle, keyboardType, autoCorrect,
      secureTextEntry, value, returnKeyType, autoCapitalize, icon, iconStyle,
      autoFocus = false
    } = this.props;
    return (
      <View style={[styles.container, style]}>
        {
          this.getImage(icon) && (
            <Image style={[styles.icon, iconStyle]} source={this.getImage(icon)}/>
          )
        }
        <TextInput
          ref={(ref) => { this.textInput = ref; }}
          style={[styles.input, inputStyle]}
          placeholder={placeholderText}
          placeholderTextColor={styles.$placeholder}
          onChangeText={(text) => this.props.onChangeText(text)}
          underlineColorAndroid={Colors.TRANSPARENT}
          multiline={false}
          keyboardType={keyboardType}
          autoCorrect={autoCorrect}
          secureTextEntry={secureTextEntry}
          value={value}
          onSubmitEditing={() => (this.props.onSubmit !== undefined ? this.props.onSubmit() : null)}
          returnKeyType={returnKeyType}
          autoCapitalize={autoCapitalize}
          autoFocus={autoFocus}
        />
        {this.props.children}
      </View>
    );
  }
}

export default LoginInput;
