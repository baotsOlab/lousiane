import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    height: 35,
    borderBottomColor: Colors.OLIVERY_GRAY1,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
    flex: 1,
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    paddingLeft: 16,
    paddingTop: 0,
    paddingBottom: 0
  },
  icon: {
    resizeMode: 'contain',
  },
});
