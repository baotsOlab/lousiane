import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';
import WDStyleSheet from '../../utils/WDStyleSheet';

export default WDStyleSheet.create({
  listNoItemContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listNoItemTitle: {
    v_marginTop: 22,
    color: Colors.OLIVERY_BLACK1
  },
  noDataIcon: {
    s_width: 34,
    s_height: 35
  },
  footerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  errorContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorTitle: {
    f_fontSize: 24,
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR
  },
  errorSubtitle: {
    f_fontSize: 18,
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.LIGHT
  },
  reloadButton: {
    v_marginTop: 24,
    width: '80%'
  },
  errorDescription: {
    f_fontSize: 13,
    color: Colors.OLIVERY_BLACK2,
    fontFamily: Fonts.MEDIUM
  }
});
