import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator, FlatList } from 'react-native';
import _ from 'lodash';
import styles from './styles';
import Loading from '../../components/Loading';
import SolidButton from '../SolidButton';
import GeneralError from '../GeneralError';

/**
 * A list supports:
 * - Pull to refresh
 * - Empty state
 * - Error state with Reload button
 * - Load more
 * - Load more error state with Reload button
 * - No more content
 */
class CommonList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      error: null,
      loading: false,
      loadingMore: false,
      canLoadMore: true,
      refreshing: false,
    };
  }

  updateList = (list) => {
    this.setState({ list });
  };

  loadData = async (page = 1, refreshing = false) => {
    const { loadData, numberOfRowsToLoad } = this.props;
    this.setState({ loading: !refreshing, error: null, refreshing });
    let newState = {};
    try {
      const list = await loadData(page, numberOfRowsToLoad);
      newState = { list, canLoadMore: list.length >= numberOfRowsToLoad };
    } catch (error) {
      newState = { error };
    }
    this.setState({ ...newState, loading: false, refreshing: false });
  };

  _renderNoItems = () => (
    <View style={styles.listNoItemContainer}>
      <Text style={styles.listNoItemTitle}>{'No data'}</Text>
    </View>
  );

  _keyExtractor = item => _.get(item, 'id', 0).toString();

  _loadMore = async () => {
    const { supportLoadMore, numberOfRowsToLoad, loadData } = this.props;
    if (!supportLoadMore) return;
    const { list, canLoadMore, loadingMore, error } = this.state;

    if (!canLoadMore || loadingMore || !_.isNil(error)) return;

    const nextPage = Math.round(list.length / numberOfRowsToLoad) + 1;
    this.setState({ loadingMore: true, error: null });
    let newState = {};
    try {
      const moreList = await loadData(nextPage, numberOfRowsToLoad);
      newState = {
        list: [...list, ...moreList],
        canLoadMore: moreList.length >= numberOfRowsToLoad
      };
    } catch (err) {
      newState = { error: err };
    }
    this.setState({ ...newState, loadingMore: false });
  };

  _onTryToLoadMore = () => {
    this.setState({ error: null }, this._loadMore);
  };

  _renderFooter = () => {
    const { footerContainerStyle, supportLoadMore } = this.props;
    if (!supportLoadMore) return null;
    const { error, canLoadMore } = this.state;
    if (!_.isNil(error)) {
      return this._renderError(error, true);
    }

    if (!canLoadMore) {
      return (
        <View style={[styles.footerContainer, footerContainerStyle]}>
          <Text>No more content.</Text>
        </View>
      );
    }

    return (
      <View style={[styles.footerContainer, footerContainerStyle]}>
        <ActivityIndicator
          size='small'
          color='#000'
          animating
        />
      </View>
    );
  };

  _renderError = (error, forLoadMore = false) => {
    const { onReload } = this.props;
    if (forLoadMore) {
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.errorTitle}>Whoops!</Text>
          <Text style={styles.errorSubtitle}>{ error.message }</Text>
          <SolidButton
            text={'RETRY'}
            onPress={this._onTryToLoadMore}
            style={styles.reloadButton}
          />
        </View>
      );
    }
    return (
      <GeneralError
        error={error}
        onReload={() => {
          this.loadData();
          onReload();
        }}
      />
    );
  };

  _onRefresh = () => {
    this.loadData(1, true);
  };

  _renderList = () => {
    const {
      renderItem,
      renderSeparator,
      style,
      keyExtractor = this._keyExtractor,
      renderNoItems = this._renderNoItems,
      renderHeader = () => null,
      contentContainerStyle,
      supportPullToRefresh,
      numColumns
    } = this.props;
    const { list, refreshing, error } = this.state;
    if (list.length === 0) {
      if (_.isNil(error)) {
        return renderNoItems();
      }
      return this._renderError(error, false);
    }
    return (
      <FlatList
        data={list}
        renderItem={renderItem}
        style={style}
        contentContainerStyle={contentContainerStyle}
        keyExtractor={keyExtractor}
        ItemSeparatorComponent={renderSeparator}
        onRefresh={supportPullToRefresh && this._onRefresh}
        refreshing={refreshing}
        ListHeaderComponent={renderHeader}
        ListFooterComponent={this._renderFooter}
        onEndReached={this._loadMore}
        onEndReachedThreshold={0.1}
        extraData={this.state}
        numColumns={numColumns}
        keyboardDismissMode={'on-drag'}
        keyboardShouldPersistTaps={'always'}
      />
    );
  };

  _renderLoading = () => {
    return (<Loading />);
  };

  render() {
    const { loading } = this.state;
    return loading ? this._renderLoading() : this._renderList();
  }
}

CommonList.propTypes = {
  numberOfRowsToLoad: PropTypes.number,
  supportLoadMore: PropTypes.bool,
  supportPullToRefresh: PropTypes.bool,
  loadData: PropTypes.func.isRequired,
  renderItem: PropTypes.func.isRequired,
  renderSeparator: PropTypes.func,
  numColumns: PropTypes.number,
  onReload: PropTypes.func.isRequired
};

CommonList.defaultProps = {
  onReload: () => {},
  numberOfRowsToLoad: 15,
  supportLoadMore: true,
  supportPullToRefresh: true,
  renderSeparator: () => null,
  numColumns: 1
};

export default CommonList;
