import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Images from '../../assets/Images';
import SolidButton from '../SolidButton';

class GeneralError extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      title = 'Whoops!',
      error = {},
      onReload = () => {},
      description,
      buttonText = 'TAP TO RELOAD',
      icon = Images.noConnection,
      onOpenSettings
    } = this.props;
    return (
      <View style={styles.errorContainer}>
        <Image source={icon} />
        <Text style={styles.errorTitle}>{ title }</Text>
        <Text style={styles.errorSubtitle}>{ error.message }</Text>
        { description && <Text style={styles.errorDescription}>{ description }</Text> }
        {
          onOpenSettings && (
            <SolidButton
              text={'OPEN SETTINGS'}
              onPress={onOpenSettings}
              style={styles.reloadButton}
            />
          )
        }
        <SolidButton
          text={buttonText}
          onPress={onReload}
          style={styles.reloadButton}
        />
      </View>
    );
  }
}
GeneralError.propTypes = {
  error: PropTypes.object.isRequired,
  buttonText: PropTypes.string,
  title: PropTypes.string,
  onReload: PropTypes.func.isRequired,
  description: PropTypes.string,
  onOpenSettings: PropTypes.func
};
export default GeneralError;
