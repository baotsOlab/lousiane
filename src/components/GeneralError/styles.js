import WDStyleSheet from '../../utils/WDStyleSheet';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default WDStyleSheet.create({
  errorContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorTitle: {
    f_fontSize: 24,
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR
  },
  errorSubtitle: {
    f_fontSize: 18,
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.LIGHT
  },
  reloadButton: {
    v_marginTop: 24,
    width: '80%'
  }
});
