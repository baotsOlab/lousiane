import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  checkContainer: {
    flexDirection: 'row',
  },
  checkIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  checkTitle: {
    backgroundColor: Colors.TRANSPARENT,
    marginLeft: 10,
    fontFamily: Fonts.REGULAR,
    fontSize: 13,
    color: Colors.OLIVERY_BLACK1,
    flex: 1
  },

});
