import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import PropTypes from 'prop-types';

import Images from '../../assets/Images';
import styles from './styles';

class CheckBox extends Component {
  render() {
    const { checked, onChange, title, style } = this.props;
    return (
      <TouchableOpacity
        style={[styles.checkContainer, style]}
        onPress={() => onChange(!checked)}
      >
        <Image
          style={styles.checkIcon}
          source={checked ? Images.check : Images.uncheck}
        />
        <Text style={styles.checkTitle}>{title}</Text>
      </TouchableOpacity>
    );
  }
}

CheckBox.propTypes = {
  title: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

CheckBox.defaultProps = {
  title: ''
};
export default CheckBox;
