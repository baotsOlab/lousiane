import { Component } from 'react';
import { AppState } from 'react-native';
import PropTypes from 'prop-types';

class AppStateHandler extends Component {
  state = { appState: AppState.currentState };
  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    const { onGoToBackground, onGoToForeground } = this.props;
    const isInactiveOrBackground = this.state.appState.match(/inactive|background/);
    if (isInactiveOrBackground && nextAppState === 'active') {
      onGoToForeground();
    } else if (!isInactiveOrBackground && (nextAppState === 'inactive' || nextAppState === 'background')) {
      onGoToBackground();
    }
    this.setState({ appState: nextAppState });
  };

  render() {
    return null;
  }
}

AppStateHandler.propTypes = {
  onGoToForeground: PropTypes.func,
  onGoToBackground: PropTypes.func,
};

AppStateHandler.defaultProps = {
  onGoToForeground: () => {},
  onGoToBackground: () => {},
};
export default AppStateHandler;
