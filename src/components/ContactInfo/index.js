import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Images from '../../assets/Images';
import styles from './styles';
import I18n from '../../i18n';

class ContactInfo extends Component {
  _renderItem = (title, content, onEdit, editable = true) => {
    return (
      <TouchableOpacity style={styles.itemContainer} onPress={onEdit} disabled={!editable}>
        <Text style={styles.itemTitle}>{title}</Text>
        <Text style={[styles.itemContent, !editable && styles.itemTextDisabled]}>{content}</Text>
        {editable && <Image style={styles.editImage} source={Images.edit}/>}
      </TouchableOpacity>
    );
  };

  render() {
    const {
      name,
      phone,
      address,
      editableName = true,
      editablePhone = true,
      onChangeName,
      onChangePhone,
      onChangeAddress,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{I18n.t('ordererInfo')}</Text>
        </View>
        {this._renderItem(I18n.t('mobile'), phone, onChangePhone, editablePhone)}
        {this._renderItem(I18n.t('name'), name, onChangeName, editableName)}
        {this._renderItem(I18n.t('address'), address, onChangeAddress)}
      </View>
    );
  }
}

ContactInfo.propTypes = {
  name: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  onChangeName: PropTypes.func.isRequired,
  onChangePhone: PropTypes.func.isRequired,
  onChangeAddress: PropTypes.func.isRequired,
};
export default ContactInfo;
