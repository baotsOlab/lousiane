import { StyleSheet } from 'react-native';
import { deviceHeight } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  greenView: {
    height: 76,
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  container: {
    flex: 1,
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  wrapProfile: {
    backgroundColor: Colors.WHITE,
    position: 'absolute',
    height: deviceHeight - 70 - 74,
    top: 74,
    left: 10,
    right: 10,
    borderRadius: 4,
  },
  avatarLayout: {
    margin: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 106,
    height: 106,
    borderRadius: 53,
  },
  wrapCamera: {
    position: 'absolute',
    right: 0,
    top: 72
  },
  backgroundCamera: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 24,
    height: 24,
    borderRadius: 12,
    overflow: 'hidden',
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  cameraImage: {
    width: 12,
    height: 10
  },
  wrapAvatar: {
    flex: 1,
    marginVertical: 5
  },
  wrapInfo: {
    marginBottom: 15,
    marginHorizontal: 10
  },
  title: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 12,
    fontFamily: Fonts.REGULAR
  },
  detail: {
    textAlignVertical: 'top',
    padding: 0,
    height: 16,
    marginTop: 8,
    marginBottom: 8,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
  },
  detailAddress: {
    marginTop: 8,
    marginBottom: 8,
  },
  addressText: {
    color: Colors.OLIVERY_BLACK1,
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
  },
  breakLine: {
    borderBottomWidth: 1,
    borderColor: Colors.OLIVERY_GRAY1
  },
  btnChangePass: {
    marginTop: 15,
    marginBottom: 20,
    marginHorizontal: 10,
  },
  titleBtn: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 12,
    fontFamily: Fonts.BOLD
  },
  errorText: {
    color: Colors.OLIVERY_RED2,
    fontSize: 12,
    fontFamily: Fonts.MEDIUM
  },
  datePicker: {
    minWidth: 180
  },
  saveButton: {
    color: Colors.WHITE,
    fontFamily: Fonts.REGULAR,
    fontSize: 17,
    padding: 6
  },
  disabledText: {
    color: Colors.OLIVERY_GRAY2,
  },
  addressContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addressRowContainer: {
    flex: 0.9
  },
  arrowContainer: {
    flex: 0.1
  },
  arrow: {
    width: 6,
    height: 11,
    marginHorizontal: 12
  }
});
