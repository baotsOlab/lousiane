import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Keyboard,
  ScrollView
} from 'react-native';
import moment from 'moment';
import _ from 'lodash';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import { withFormik } from 'formik';
import ImagePicker from 'react-native-image-crop-picker';
import DatePicker from 'react-native-datepicker';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import WithLoading from '../../hoc/WithLoading';
import Colors from '../../common/Colors';
import { OPEN_CAMERA_CONFIG, OPEN_PICKER_CONFIG } from '../../assets/constants/constant';
import { actions as authActions } from '../../store/reducers/auth';
import { getString, getNumber, isEmail } from '../../utils/string';
import utils from '../../utils';

const saveButtonHitSlop = { top: 22, right: 22, left: 22, bottom: 22 };

class EditProfile extends Component {
  componentDidMount() {
    this.props.validateForm();
  }

  pickSingle = async () => {
    try {
      const image = await ImagePicker.openPicker(OPEN_PICKER_CONFIG);
      this._uploadImage(image);
    } catch (error) {
      errorManager.createError('Selecting a photo', error);
    }
  };

  pickSingleWithCamera = async () => {
    try {
      const image = await ImagePicker.openCamera(OPEN_CAMERA_CONFIG);
      this._uploadImage(image);
    } catch (error) {
      errorManager.createError('Camera', error);
    }
  };

  _uploadImage = async (image) => {
    const { setProfile } = this.props;
    const data = new FormData();
    data.append('avatar', utils.getImageObjectForFormData(image));
    const { showLoading } = this.props;
    showLoading(true);
    try {
      const result = await api.editAvatar(data);
      setProfile(result);
      this.props.setValues(getValues(result));
    } catch (error) {
      errorManager.createError('Uploading image', error);
    } finally {
      showLoading(false);
    }
  };

  handlePress = (i) => {
    switch (i) {
      case 1:
        this.pickSingleWithCamera();
        break;
      case 2:
        this.pickSingle();
        break;
      default: break;
    }
  };

  _returnLocation = (key) => (formattedAddress, { lat, lng }) => {
    const { values } = this.props;
    this.props.setFieldValue(key, { ...values[key], address: formattedAddress, lat, lng });
  };

  _returnTitle = (key) => (title = '') => {
    const { values } = this.props;
    this.props.setFieldValue(key, { ...values[key], title });
  };

  _onSave = async () => {
    Keyboard.dismiss();
    const { showLoading, setProfile, setSubmitting, values } = this.props;
    const {
      name,
      email,
      address1,
      address2,
      address3,
      dob,
    } = values;
    showLoading(true);
    setSubmitting(true);
    try {
      const params = {
        name,
        email,
        address_1: address1.address,
        address_1_title: address1.title,
        address_1_lat: address1.lat,
        address_1_lng: address1.lng,
        address_2: address2.address,
        address_2_title: address2.title,
        address_2_lat: address2.lat,
        address_2_lng: address2.lng,
        address_3: address3.address,
        address_3_title: address3.title,
        address_3_lat: address3.lat,
        address_3_lng: address3.lng,
      };
      if (!_.isNil(dob) && !_.isEmpty(dob)) {
        params.dob = moment(dob).format('YYYY-MM-DD');
      }
      const result = await api.editProfile(params);
      setProfile(result);
      this.props.navigation.goBack();
    } catch (error) {
      errorManager.createError('Edit profile', error);
    } finally {
      showLoading(false);
      setSubmitting(false);
    }
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _onChangeText = (key) => (value) => {
    this.props.setFieldValue(key, value);
  };

  _onSearchLocation = (title, address, returnLocation, returnTitle) => () => {
    this.props.navigation.navigate(Routes.SearchLocation, {
      title,
      returnLocation,
      returnTitle,
      address,
    });
  };

  _renderAddressField = (title, locationTitle, key) => {
    const { values } = this.props;
    const address = _.get(values[key], 'address', '');
    return (
      <TouchableOpacity
        style={styles.wrapInfo}
        onPress={
          this._onSearchLocation(
            locationTitle,
            address,
            this._returnLocation(key),
            this._returnTitle(key),
          )
        }
      >
        <View style={styles.addressContainer}>
          <View style={styles.addressRowContainer}>
            <Text style={[styles.title, { color: Colors.OLIVERY_GREEN1 }]}>{ title }</Text>
            <View style={styles.detailAddress}>
              <Text style={styles.addressText}>{address}</Text>
            </View>
          </View>
          <View style={styles.arrowContainer}>
            <Image source={Images.nextIcon} style={styles.arrow}/>
          </View>
        </View>
        <View style={styles.breakLine} />
      </TouchableOpacity>
    );
  };

  _onBlur = (key) => () => {
    this.props.setFieldTouched(key, true, true);
  };

  _onOpenDatePicker = () => {
    this.datePicker.onPressDate();
  };

  _renderRightButton = () => {
    const { isValid, isSubmitting } = this.props;
    const disabled = !isValid || isSubmitting;
    return (
      <TouchableOpacity hitSlop={saveButtonHitSlop} onPress={this._onSave} disabled={disabled}>
        <Text style={[styles.saveButton, disabled && styles.disabledText]}>SAVE</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { touched, values, errors } = this.props;
    const { name, email, avatar, dob } = values;
    const CANCEL_INDEX = 0;
    const options = ['Cancel', 'Take Photo', 'Choose From Library'];
    const title = 'Select Photo';
    let imageSource = {};
    if (avatar.length > 0) {
      imageSource = { uri: avatar };
    } else {
      imageSource = Images.noAvatar;
    }
    return (
      <Background>
        <Header
          onTouchBackButton={this._goBack}
          title='EDIT PROFILE'
          hideBackButton={false}
          renderRight={this._renderRightButton}
        />
        <View style={styles.greenView} />
        <View style={styles.container} />
        <ScrollView
          style={styles.wrapProfile}
          keyboardShouldPersistTaps={'always'}
          keyboardDismissMode={'on-drag'}
        >
          <View style={styles.avatarLayout}>
            <TouchableOpacity style={styles.wrapAvatar} onPress={() => this.ActionSheet.show()}>
              <Image style={styles.avatar} source={imageSource}/>
              <View style={styles.wrapCamera}>
                <View style={styles.backgroundCamera}>
                  <Image style={styles.cameraImage} source={Images.whiteCamera}/>
                </View>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.wrapInfo}>
            <Text style={styles.title}>Name</Text>
            <TextInput
              returnKeyType='next'
              style={styles.detail}
              underlineColorAndroid={Colors.TRANSPARENT}
              onChangeText={this._onChangeText('name')}
              onSubmitEditing={() => this.emailInput.focus()}
              blueOnSubmit
              value={name}
            />
            <View style={styles.breakLine} />
          </View>

          <View style={styles.wrapInfo}>
            <Text style={styles.title}>Email</Text>
            <TextInput
              ref={ref => { this.emailInput = ref; } }
              returnKeyType='done'
              style={styles.detail}
              underlineColorAndroid={Colors.TRANSPARENT}
              onChangeText={this._onChangeText('email')}
              value={email}
              keyboardType={'email-address'}
              textContentType={'emailAddress'}
              onBlur={this._onBlur('email')}
            />
            <View style={styles.breakLine} />
            {
              touched.email && errors.email &&
              <Text style={styles.errorText}>{errors.email}</Text>
            }
          </View>

          <TouchableOpacity style={styles.wrapInfo} onPress={this._onOpenDatePicker}>
            <Text style={styles.title}>Date of Birth</Text>
            <DatePicker
              ref={ref => { this.datePicker = ref; }}
              date={dob}
              mode="date"
              androidMode="spinner"
              placeholder={dob.length === 0 ? 'Select date' : moment(dob).format('MMMM DD YYYY')}
              format="MMMM DD YYYY"
              minDate="1970-01-01"
              maxDate="2010-01-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              style={styles.datePicker}
              customStyles={{
                dateInput: { borderWidth: 0 },
                placeholderText: { color: Colors.OLIVERY_BLACK1 },
                dateText: { color: Colors.OLIVERY_BLACK1, alignSelf: 'flex-start' },
                dateTouchBody: { height: undefined }
              }}
              onDateChange={this._onChangeText('dob')}
            />

            <View style={styles.breakLine} />
          </TouchableOpacity>

          {this._renderAddressField(`Address 1 (${values.address1.title})`, values.address1.title, 'address1')}
          {this._renderAddressField(`Address 2 (${values.address2.title})`, values.address2.title, 'address2')}
          {this._renderAddressField(`Address 3 (${values.address3.title})`, values.address3.title, 'address3')}
        </ScrollView>
        <ActionSheet
          ref={o => { this.ActionSheet = o; }}
          title={title}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          onPress={this.handlePress}
        />
      </Background>
    );
  }
}

const getValues = (user) => {
  return {
    name: getString(user, 'name'),
    email: getString(user, 'email'),
    dob: getString(user, 'user_profile.dob'),
    avatar: getString(user, 'user_profile.avatar'),
    address1: {
      address: getString(user, 'user_profile.address_1'),
      title: getString(user, 'user_profile.address_1_title', 'Home'),
      lat: getNumber(user, 'user_profile.address_1_lat'),
      lng: getNumber(user, 'user_profile.address_1_lng'),
    },
    address2: {
      address: getString(user, 'user_profile.address_2'),
      title: getString(user, 'user_profile.address_2_title', 'Work'),
      lat: getNumber(user, 'user_profile.address_2_lat'),
      lng: getNumber(user, 'user_profile.address_2_lng'),
    },
    address3: {
      address: getString(user, 'user_profile.address_3'),
      title: getString(user, 'user_profile.address_3_title', 'Other'),
      lat: getNumber(user, 'user_profile.address_3_lat'),
      lng: getNumber(user, 'user_profile.address_3_lng'),
    },
  };
};

const withFormikConfig = {
  isInitialValid: false,
  mapPropsToValues: (props) => {
    const { user } = props;
    return getValues(user);
  },
  validate: (values) => {
    const errors = {};
    if (values.email && values.email.length > 0 && !isEmail(values.email)) {
      errors.email = 'Please input a valid email address.';
    }
    return errors;
  }
};

const mapStateToProps = ({ auth }) => ({ user: auth.user });
const mapDispatchToProps = {
  setProfile: authActions.setProfile
};

const enhancer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withFormik(withFormikConfig),
  WithLoading
);

export default enhancer(EditProfile);
