import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import OpenSettings from 'react-native-open-settings';
import Header from '../../components/Header';
import Background from '../../components/Background';
import { RESTAURANT_STATUS } from '../../assets/constants/constant';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import WithLoading from '../../hoc/WithLoading';
import locationHelper from '../../utils/location';
import Loading from '../../components/Loading';
import CommonList from '../../components/CommonList';
import dataManager from '../../assets/constants/dataManager';
import env from '../../env';
import GeneralError from '../../components/GeneralError';
import AppStateHandler from '../../components/AppStateHandler';

class HomeNearYou extends Component {
  constructor(props) {
    super(props);

    let lat = 0;
    let lng = 0;
    let address = '';
    if (!_.isEmpty(dataManager.selectedLocation)) {
      ({ address, lat, lng } = dataManager.selectedLocation);
    }
    this.state = { address, lat, lng, locationError: null };
  }

  componentDidMount() {
    if (_.isEmpty(dataManager.selectedLocation)) {
      this._getLocation();
    } else if (this.commonList) {
      this.commonList.loadData();
    }
  }

  /* START: GET LOCATION */
  _getLocation = () => {
    const { showLoading } = this.props;
    showLoading(true, { showIndicator: false });
    this.setState({ locationError: null });
    locationHelper.requestLocation(this._onLocation, this._onLocationError);
  };

  _onLocationError = (error) => {
    this.props.showLoading(false);
    this.setState({ locationError: error });
  };

  _onLocation = (lat, lng) => {
    this.props.showLoading(false);
    dataManager.selectedLocation = { lat, lng };
    this.setState({ lat, lng });
    this._getAddress(lat, lng);
    if (this.commonList) this.commonList.loadData();
  };
  /* END: GET LOCATION */

  _loadRestaurants = (lat, lng) => async (page, take) => {
    const result = await api.getRestaurantsNearYou({
      id: 2,
      lat,
      lng,
      page,
      take
    });
    return result.data;
  };

  /**
   * Get address based on latitude and longitude
   * @param lat
   * @param lng
   * @returns {Promise<void>}
   * @private
   */
  _getAddress = async (lat, lng) => {
    const url = env.googleAPI.maps.GET_LOCATION_GOOGLE_MAP_URL.replace('{lat}', lat).replace('{lng}', lng);
    const json = await api.fetchGoogleAPI(url);
    const address = _.get(json, 'results[0].formatted_address', '');
    dataManager.selectedLocation = { ...dataManager.selectedLocation, address };
    this.setState({ address });
  };

  _like = (data, index) => async () => {
    try {
      await api.like(data.id);
      this.updateLikeIcon(index);
    } catch (error) {
      errorManager.createError('Adding to favorites', error);
    }
  };

  updateLikeIcon = (index) => {
    const { list } = this.commonList.state;
    const tempRestaurant = [...list];
    tempRestaurant[index].like = !list[index].like;
    this.commonList.updateList(list);
  };

  _returnLocation = (address, geometry) => {
    const { lat, lng } = geometry;

    this.setState({ address, lat, lng }, () => {
      dataManager.selectedLocation = { address, lat, lng };
      if (this.commonList) this.commonList.loadData();
    });
  };

  _onBack = () => {
    this.props.navigation.goBack();
  };

  _goToSearchLocation = () => {
    this.props.navigation.navigate(Routes.SearchLocation, {
      returnLocation: this._returnLocation,
      address: this.state.address,
    });
  };

  _renderHeader = () => {
    const { loading } = this.props;
    return (
      <Header onTouchBackButton={this._onBack} hideBackButton>
        {
          !loading &&
          (
            <TouchableOpacity
              onPress={this._goToSearchLocation}
              style={styles.locationButton}>
              <Image style={styles.locationIcon} source={Images.locationWhite}/>
              <Text style={styles.LocationTitle} numberOfLines={1}>
                {this.state.address}
              </Text>
            </TouchableOpacity>
          )
        }
      </Header>
    );
  };

  _renderListHeader = () => {
    return (
      <View style={styles.nearYouContainer}>
        <Text style={styles.nearYouText}>GROCERIES NEAR YOU</Text>
      </View>
    );
  };

  _touchItem = (itemId, index) => () => {
    this.props.navigation.navigate(Routes.HomeMenu, {
      restaurantId: itemId,
      onLike: () => this.updateLikeIcon(index)
    });
  };

  _getStatusContainerStyle = (status) => {
    switch (status) {
      case RESTAURANT_STATUS.OPEN:
        return styles.openStatusContainer;
      case RESTAURANT_STATUS.CLOSE:
        return styles.closeStatusContainer;
      default:
        return styles.delayStatusContainer;
    }
  };

  _renderItem = ({ item, index }) => {
    const statusContainer = this._getStatusContainerStyle(item.status);
    const imageUrl = _.chain(item).get('image').defaultTo('').value();
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.cellContainer}
        onPress={this._touchItem(item.id, index)}
      >
        <View style={styles.menuImageContainer}>
          <Image style={styles.menuImage} source={{ uri: imageUrl }}/>
        </View>
        <TouchableOpacity onPress={this._like(item, index)} style={styles.likeContainer}>
          <Image
            style={styles.likeImage}
            source={item.like ? Images.likeWhite : Images.likeEmpty}
          />
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={styles.title} numberOfLines={1}>
            {item.name}
          </Text>
          <View style={statusContainer}>
            <Text style={styles.statusText}>
              {item.status}
            </Text>
          </View>
        </View>
        <Text style={styles.description} numberOfLines={2}>
          {item.address}
        </Text>
      </TouchableOpacity>
    );
  };

  _renderList = () => {
    const { lat, lng } = this.state;
    if (lat > 0 || lng > 0) {
      return (
        <CommonList
          ref={ref => { this.commonList = ref; }}
          renderItem={this._renderItem}
          renderHeader={this._renderListHeader}
          contentContainerStyle={styles.contentContainer}
          loadData={this._loadRestaurants(lat, lng)}
          supportLoadMore
          numberOfRowsToLoad={15}
          onReload={this._onReload}
        />
      );
    }
    return null;
  };

  _onReload = () => {
    if (_.isEmpty(dataManager.selectedLocation)) {
      this._getLocation();
    } else {
      const { lat, lng } = dataManager.selectedLocation;
      this._getAddress(lat, lng);
    }
  };

  _renderLoading = () => {
    const { loadingText } = this.props;
    return (
      <Loading title={loadingText} />
    );
  };

  _renderLocationError = () => {
    return (
      <GeneralError
        icon={Images.locationError}
        title={'Error!'}
        error={{ message: 'Location is not available' }}
        description={'Please check your settings'}
        buttonText={'TAP TO RELOAD'}
        onOpenSettings={() => OpenSettings.openSettings()}
        onReload={this._getLocation}
      />
    );
  };

  _onGoToForeground = () => {
    if (this.state.locationError) {
      this._getLocation();
    }
  };

  render() {
    const { loading } = this.props;
    const { locationError } = this.state;
    return (
      <Background containerStyle={styles.container}>
        {this._renderHeader()}
        { loading && this._renderLoading() }
        { this._renderList() }
        { !_.isNil(locationError) && this._renderLocationError(locationError) }
        <AppStateHandler onGoToForeground={this._onGoToForeground}/>
      </Background>
    );
  }
}

export default WithLoading(HomeNearYou);
