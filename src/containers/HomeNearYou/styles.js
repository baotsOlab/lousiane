import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';
import WDStyleSheet from '../../utils/WDStyleSheet';

export default WDStyleSheet.create({
  container: {
    backgroundColor: Colors.OLIVERY_WHITE
  },
  locationButton: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 10,
    alignItems: 'center',
  },
  locationIcon: {
    width: 14,
    height: 20,
    resizeMode: 'contain',
  },
  LocationTitle: {
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.WHITE,
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
    marginLeft: 10,
  },
  nearYouContainer: {
    width: '100%',
  },
  nearYouText: {
    color: Colors.OLIVERY_BLACK1,
    fontSize: 14,
    fontFamily: Fonts.MEDIUM,
    marginVertical: 15,
    textAlign: 'center'
  },
  cellContainer: {
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    marginBottom: 10,
    padding: 10,
    shadowColor: Colors.OLIVERY_BLACK1,
    shadowOffset: { width: 0, height: 1.5 },
    shadowOpacity: 0.16,
    shadowRadius: 1.5,
    elevation: 1.5
  },
  contentContainer: {
    paddingHorizontal: 10,
    paddingBottom: 20
  },
  menuImageContainer: {
    width: '100%',
    aspectRatio: 335 / 200,
    flexDirection: 'row',
    alignItems: 'stretch',
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  menuImage: {
    flex: 1,
    width: null,
    height: null,
  },
  likeContainer: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 10,
    right: 10,
    width: 44,
    height: 44,
  },
  likeImage: {
    width: 23,
    height: 20,
  },
  titleContainer: {
    flexDirection: 'row',
    marginTop: 13,
    alignItems: 'center'
  },
  title: {
    flex: 1,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  openStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_GREEN3,
    borderRadius: 10,
  },
  delayStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_ORANGE,
    borderRadius: 10,
  },
  closeStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_RED2,
    borderRadius: 10,
  },
  statusText: {
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.WHITE,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
    marginHorizontal: 8,
  },
  description: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 4,
    marginBottom: 10,
    color: Colors.OLIVERY_GRAY3,
    fontSize: 13,
    fontFamily: Fonts.REGULAR,
  },
});
