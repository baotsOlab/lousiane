import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';

import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import styles from './styles';
import Routes from '../../navigation/routes';

export default class PaymentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  render() {
    return (
      <Background
        loading={this.state.isLoading}
      >
        <Header
          onTouchBackButton={() =>
            this.props.navigation.goBack()
          }
          title='PAYMENT'
        />
        <View style={styles.container}>
          <Text style={[styles.item, { marginTop: 20 }]}>
            O-WALLET
          </Text>

          <View style={styles.subContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.title}>
                Balance
              </Text>
              <Text style={styles.title}>
                Balance after payment
              </Text>
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.detailMoney}>
                $26
              </Text>
              <Text style={styles.detailMoney}>
                $0
              </Text>
            </View>
          </View>

          <Text style={[styles.item, { marginTop: 30 }]}>
            PAYMENT STRIPE
          </Text>

          <View style={styles.subContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.title}>
                Card number
              </Text>
              <Text style={styles.title}>
                Name on card
              </Text>
              <Text style={styles.title}>
                Expireation date
              </Text>
              <Text style={styles.title}>
                Payment amount
              </Text>
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.detail}>
                4466 6959 8965 1102
              </Text>
              <Text style={styles.detail}>
                Charlie Puth
              </Text>
              <Text style={styles.detail}>
                06/19
              </Text>
              <Text style={styles.detailMoney}>
                $100
              </Text>
            </View>
          </View>

          <View style={styles.applyButtonContainer}>
            <SolidButton
              text='Order Status'
              onPress={() => this.props.navigation.navigate(Routes.OrderStatus)}
              fontSize={18}
              bold={true}
            />
          </View>
        </View>
      </Background>
    );
  }
}