import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    flexDirection: 'row',
  },
  textContainer: {
    marginLeft: 10,
  },
  item: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginLeft: 10,
  },
  title: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY3,
    marginTop: 10
  },
  detail: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginTop: 10
  },
  detailMoney: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginTop: 10
  },
  applyButtonContainer: {
    flex: 1,
    width: deviceWidth - 20,
    marginBottom: 20,
    justifyContent: 'flex-end',
    alignSelf: 'center'
  },
});
