import { StyleSheet } from 'react-native';
import { deviceHeight } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  greenView: {
    height: 76,
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  container: {
    flex: 1,
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  wrapProfile: {
    backgroundColor: Colors.WHITE,
    position: 'absolute',
    height: deviceHeight - 70 - 74,
    top: 74,
    left: 10,
    right: 10,
    borderRadius: 4,
  },
  avatarLayout: {
    margin: 15,
    flexDirection: 'row',
  },
  avatar: {
    width: 106,
    height: 106,
    borderRadius: 53,
  },
  wrapAvatar: {
    flex: 1,
    marginVertical: 5
  },
  wrapEditBtn: {
    flex: 1,
    alignItems: 'flex-end'
  },
  editText: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
    marginLeft: 5
  },
  wrapEmpty: {
    flex: 1
  },
  wrapInfo: {
    marginBottom: 23,
    marginHorizontal: 10
  },
  title: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 12,
    fontFamily: Fonts.REGULAR
  },
  detail: {
    marginTop: 8,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 14,
    fontFamily: Fonts.REGULAR
  },
  editBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: 44,
    minHeight: 44,
  },
  editIcon: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    tintColor: Colors.OLIVERY_GREEN1
  },
  btnChangePass: {
    marginTop: 5,
    marginBottom: 30,
    marginHorizontal: 10,
  },
  titleBtn: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 12,
    fontFamily: Fonts.BOLD
  },
  warningText: {
    color: Colors.OLIVERY_ORANGE,
    fontSize: 12,
    marginTop: 4,
    fontWeight: '600'
  }
});
