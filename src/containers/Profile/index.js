import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { compose } from 'recompose';
import _ from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import WithLoading from '../../hoc/WithLoading';
import I18n from '../../i18n';
import { getString } from '../../utils/string';
import { actions as authActions } from '../../store/reducers/auth';

class Profile extends Component {
  _goBack = () => {
    this.props.navigation.goBack();
  };

  _goTo = (routeName, params = {}) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  render() {
    const { user } = this.props;
    const name = getString(user, 'name');
    const email = getString(user, 'email');
    const phone = getString(user, 'phone');
    const dob = getString(user, 'user_profile.dob');
    const addressHome = getString(user, 'user_profile.address_1');
    const addressHomeTitle = getString(user, 'user_profile.address_1_title', 'Home');
    const addressWork = getString(user, 'user_profile.address_2');
    const addressWorkTitle = getString(user, 'user_profile.address_2_title', 'Work');
    const addressOther = getString(user, 'user_profile.address_3');
    const addressOtherTitle = getString(user, 'user_profile.address_3_title', 'Other');
    const avatarUrl = getString(user, 'user_profile.avatar');
    const isEmailVerified = _.get(user, 'is_email_verified', 0) === 1;
    let imageSource = {};
    if (avatarUrl.length > 0) {
      imageSource = { uri: avatarUrl };
    } else {
      imageSource = Images.noAvatar;
    }

    return (
      <Background>
        <Header onTouchBackButton={this._goBack} title={I18n.t('profile')} hideBackButton={false}/>
        <View style={styles.greenView}/>
        <View style={styles.container}/>
        <View style={styles.wrapProfile}>
          <ScrollView>
            <View style={styles.avatarLayout}>
              <View style={styles.wrapEmpty}/>
              <View style={styles.wrapAvatar}>
                <Image style={styles.avatar} source={imageSource}/>
              </View>

              <View style={styles.wrapEditBtn}>
                <TouchableOpacity onPress={this._goTo(Routes.EditProfile)} style={styles.editBtn}>
                  <Image style={styles.editIcon} source={Images.editIcon}/>
                  <Text style={styles.editText}>Edit</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Name</Text>
              <Text style={styles.detail}>{name}</Text>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Email Address</Text>
              <Text style={styles.detail}>{email}</Text>
              {
                !isEmailVerified &&
                <Text style={styles.warningText}>
                  This email is not verified yet. Please check your inbox!
                </Text>
              }
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Date of Birth</Text>
              <Text style={styles.detail}>{ dob && moment(dob, 'YYYY-MM-DD').format('MMMM DD YYYY')}</Text>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Phone Number</Text>
              <Text style={styles.detail}>{phone}</Text>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Address 1 ({addressHomeTitle})</Text>
              <Text style={styles.detail}>{addressHome}</Text>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Address 2 ({addressWorkTitle})</Text>
              <Text style={styles.detail}>{addressWork}</Text>
            </View>

            <View style={styles.wrapInfo}>
              <Text style={styles.title}>Address 3 ({addressOtherTitle})</Text>
              <Text style={styles.detail}>{addressOther}</Text>
            </View>
          </ScrollView>
        </View>
      </Background>
    );
  }
}

const mapStateToProps = ({ auth }) => ({ user: auth.user });
const mapDispatchToProps = {
  setProfile: authActions.setProfile
};

const enhancer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  WithLoading
);

export default enhancer(Profile);
