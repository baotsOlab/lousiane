import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import Background from '../../components/Background';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import WithLoading from '../../hoc/WithLoading';
import navigationUtils from '../../utils/navigation';
import { actions as authActions } from '../../store/reducers/auth';
import { getString } from '../../utils/string';
import utils from '../../utils';

class ProfileOption extends Component {
  _navigateTo(routeName) {
    navigationUtils.reset({
      dispatch: this.props.navigation.dispatch,
      routes: [{ name: routeName }]
    });
  }

  _logout = async () => {
    const { showLoading, logout } = this.props;
    showLoading(true);
    try {
      await api.logout();
    } finally {
      logout();
      showLoading(false);
      this._navigateTo(Routes.LoginRequired);
    }
  };

  _renderRowLogout = () => {
    return (
      <TouchableOpacity activeOpacity={1} style={styles.wrapRow} onPress={this._logout}>
        <Image style={styles.logout} source={Images.logout}/>
        <Text style={styles.titleText}>Log out</Text>
      </TouchableOpacity>
    );
  };

  _goTo = (routeName) => () => {
    this.props.navigation.navigate(routeName);
  };

  render() {
    const { user } = this.props;
    const name = _.get(user, 'name', '');
    const wallet = _.get(user, 'user_profile.wallet', 0);
    const avatarUrl = getString(user, 'user_profile.avatar');
    let imageSource = {};
    if (avatarUrl.length > 0) {
      imageSource = { uri: avatarUrl };
    } else {
      imageSource = Images.noAvatar;
    }
    return (
      <Background>
        <ScrollView>
          <View style={styles.wrapCoverPhoto}>
            <ImageBackground style={styles.coverPhoto}>
              <View style={styles.darkBackground}>
                <View style={styles.wrapAvatarName}>
                  <Image style={styles.avatar} source={imageSource} />
                  <Text style={styles.name}>{name}</Text>
                </View>
              </View>
            </ImageBackground>
          </View>
          <View style={styles.wrapRow}>
            <Image style={styles.oWalletImage} source={Images.oWallet}/>
            <Text style={styles.titleText}>O-wallet</Text>
            <View style={styles.balance}>
              <Text style={styles.balanceText}>
                Your balance: ${utils.numberWithCommas(wallet)}
              </Text>
            </View>
          </View>
          <View style={styles.breakLine} />
          <TouchableOpacity onPress={this._goTo(Routes.OrderHistory)} activeOpacity={1}>
            <View style={styles.wrapRow}>
              <Image style={styles.orderHistory} source={Images.orderHistory}/>
              <Text style={styles.titleText}>Order History</Text>
              <View style={styles.balance}>
                <Image style={styles.nextIcon} source={Images.nextIcon}/>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.breakLine} />
          <TouchableOpacity onPress={this._goTo(Routes.Profile)} activeOpacity={1}>
            <View style={styles.wrapRow}>
              <Image style={styles.profile} source={Images.profileIcon}/>
              <Text style={styles.titleText}>Profile</Text>
              <View style={styles.balance}>
                <Image style={styles.nextIcon} source={Images.nextIcon}/>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.breakLine} />
          <TouchableOpacity onPress={this._goTo(Routes.Help)} activeOpacity={1}>
            <View style={styles.wrapRow}>
              <Image style={styles.help} source={Images.help}/>
              <Text style={styles.titleText}>Help</Text>
              <View style={styles.balance}>
                <Image style={styles.nextIcon} source={Images.nextIcon}/>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.breakLine} />
          {this._renderRowLogout()}
        </ScrollView>
      </Background>
    );
  }
}

ProfileOption.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};

const mapDispatchToProp = {
  logout: authActions.logout,
  setProfile: authActions.setProfile
};
const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn,
  user: state.auth.user,
});

const enhancer = compose(
  WithLoading,
  connect(mapStateToProps, mapDispatchToProp)
);
export default enhancer(ProfileOption);
