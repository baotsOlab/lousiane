import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  wrapCoverPhoto: {
    backgroundColor: Colors.TRANSPARENT,
    aspectRatio: 400 / 250,
  },
  coverPhoto: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.OLIVERY_GREEN1,
  },
  darkBackground: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  wrapAvatarName: {
    alignItems: 'center',
    marginTop: 15
  },
  avatar: {
    width: 126,
    height: 126,
    borderRadius: 63,
  },
  name: {
    marginTop: 15,
    fontFamily: Fonts.BOLD,
    fontSize: 18,
    color: Colors.WHITE
  },
  wrapRow: {
    flexDirection: 'row',
    marginVertical: 20,
    marginHorizontal: 10,
  },
  balance: {
    flex: 1.5,
    alignItems: 'flex-end',
  },
  titleText: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginLeft: 17
  },
  balanceText: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY3
  },
  breakLine: {
    borderBottomWidth: 1,
    marginHorizontal: 10,
    borderColor: Colors.OLIVERY_GRAY1
  },
  oWalletImage: {
    width: 19,
    height: 19,
  },
  nextIcon: {
    width: 7,
    height: 18,
    resizeMode: 'contain'
  },
  orderHistory: {
    width: 18,
    height: 19,
  },
  profile: {
    width: 19,
    height: 19,
  },
  help: {
    width: 20,
    height: 20,
  },
  logout: {
    width: 17,
    height: 17,
  }
});
