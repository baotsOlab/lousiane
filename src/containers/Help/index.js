import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Background from '../../components/Background';
import Header from '../../components/Header';
import helpQuestion from '../../assets/constants/helpQuestion.json';
import Images from '../../assets/Images';
import styles from './styles';
import phoneHelper from '../../utils/phone';
import dataManager from '../../assets/constants/dataManager';

export default class Help extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrAbout: helpQuestion.AboutOlivery,
      arrUsing: helpQuestion.UsingOlivery,
      arrMyOrder: helpQuestion.MyOrder,
      arrInviteFriend: helpQuestion.InviteFriend,
      arrAnythingElse: helpQuestion.AnythingElse
    };
  }

  _onPress = (key, index) => () => {
    const newArray = [...this.state[key]];
    newArray[index].isShow = !newArray[index].isShow;
    this.setState({ [key]: newArray });
  };

  renderQuestionnaire = (title, key) => {
    return (
      <View key={key}>
        <Text style={styles.helpTitle}>{ title }</Text>
        {
          this.state[key].map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                activeOpacity={1}
                onPress={this._onPress(key, index)}
                style={styles.wrapContent}
              >
                <View style={styles.wrapQuestionRow}>
                  <Text style={styles.questionText}>{item.question}</Text>
                  <View style={styles.arrowIcon}>
                    <Image
                      style={styles.arrowImage}
                      source={item.isShow ? Images.arrowUp : Images.arrowDown}
                    />
                  </View>
                </View>
                { item.isShow && <Text style={styles.answerText}>{ item.answered }</Text>}
                <View style={styles.breakLine}/>
              </TouchableOpacity>
            );
          })
        }
      </View>
    );
  };

  _call = () => {
    phoneHelper.call(dataManager.phoneNumber);
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Background>
        <Header
          onTouchBackButton={this._goBack}
          title='HELP'
          hideBackButton={false}
          rightButton='calling'
          onRightButtonPress={this._call}
        />
        <ScrollView>
          {
            [
              { title: 'About OLIVERY', key: 'arrAbout' },
              { title: 'Using OLIVERY', key: 'arrUsing' },
              { title: 'Questions About My Order', key: 'arrMyOrder' },
              { title: 'Inviting Friends', key: 'arrInviteFriend' },
              { title: 'Anything Else?', key: 'arrAnythingElse' }
            ].map(item => this.renderQuestionnaire(item.title, item.key))
          }
        </ScrollView>
      </Background>
    );
  }
}
