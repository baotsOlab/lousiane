import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  helpTitle: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 20,
    fontFamily: Fonts.BOLD,
    marginVertical: 20,
    marginLeft: 10,
  },
  wrapContent: {
    marginHorizontal: 10
  },
  breakLine: {
    borderBottomWidth: 1,
    marginVertical: 20,
    borderColor: Colors.OLIVERY_GRAY1
  },
  wrapQuestionRow: {
    flex: 1,
    flexDirection: 'row'
  },
  questionText: {
    flex: 3,
    alignItems: 'flex-start',
    color: Colors.OLIVERY_BLACK2,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  arrowIcon: {
    flex: 1,
    alignItems: 'flex-end',
  },
  answerText: {
    marginTop: 6,
    color: Colors.OLIVERY_GRAY3,
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
  },
  arrowImage: {
    width: 11,
    height: 16,
    resizeMode: 'contain'
  }
});
