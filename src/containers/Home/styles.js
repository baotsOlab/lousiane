import { StyleSheet } from 'react-native';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
    marginBottom: 10,
    overflow: 'hidden',
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 10,
  },
  image: {
    flex: 1,
    width: null,
    height: null,
  },
  maskContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: Colors.BLACK_OPACITY30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  maskText: {
    fontFamily: Fonts.MEDIUM,
    fontSize: 24,
    color: Colors.WHITE,
  },
  comingSoonText: {
    fontFamily: Fonts.BOLD,
    fontSize: 18,
    color: Colors.WHITE
  }
});
