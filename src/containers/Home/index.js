import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import I18n from '../../i18n';

export default class Home extends Component {
  _goToGrocery = () => {
    this.props.navigation.navigate(Routes.HomeNearYou, { type: 2 });
  };

  _renderMask = (title, subtitle) => {
    return (
      <View style={styles.maskContainer}>
        <Text style={styles.maskText}>{title}</Text>
        { subtitle && <Text style={styles.comingSoonText}>{subtitle}</Text>}
      </View>
    );
  };

  render() {
    return (
      <Background>
        <Header title='HOME' hideBackButton />
        <View style={styles.container}>
          <TouchableOpacity style={styles.imageContainer} onPress={this._goToGrocery}>
            <Image source={Images.grocery} style={styles.image}/>
            {this._renderMask(I18n.t('grocery'))}
          </TouchableOpacity>
          <View style={styles.imageContainer}>
            <Image source={Images.restaurant} style={styles.image} />
            {this._renderMask(I18n.t('restaurant'), I18n.t('comingSoon'))}
          </View>
        </View>
      </Background>
    );
  }
}
