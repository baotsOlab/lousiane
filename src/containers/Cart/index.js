import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  Modal,
  ScrollView,
  Alert,
  TouchableOpacity
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import Picker from 'react-native-picker';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import Background from '../../components/Background';
import Header from '../../components/Header';
import ContactInfo from '../../components/ContactInfo';
import ReceiverInfo from '../../components/ReceiverInfo';
import DeliveryFee from '../../components/DeliveryFee';
import CheckBox from '../../components/CheckBox';
import OrderFood from '../../components/OrderFood';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import { actions } from '../../store/reducers/cart';
import WithLoading from '../../hoc/WithLoading';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import SolidButton from '../../components/SolidButton';
import ChangeInfo from '../../components/ChangeInfo';
import AndroidBackHandler from '../../components/AndroidBackHandler';
import Colors from '../../common/Colors';
import I18n from '../../i18n';
import { getString } from '../../utils/string';
import utils from '../../utils';
import DeliveryTime from '../../components/DeliveryTime';
import { ERROR_CODE, STATUS } from '../../assets/constants/constant';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../../services/subscriptionManager';

const EditType = {
  name: 0,
  phone: 1,
  address: 2,
  receiverName: 4,
  receiverPhone: 5,
  receiverAddress: 6,
};

/* eslint-disable react/no-deprecated */
class Cart extends Component {
  constructor(props) {
    super(props);
    const { user } = props.auth;
    const { ordererAddress } = props.cart;
    const name = getString(user, 'name');
    const phone = getString(user, 'phone');
    const ordererAddressName = _.get(ordererAddress, 'address', '');
    const ordererLat = _.get(ordererAddress, 'lat', 0);
    const ordererLng = _.get(ordererAddress, 'lng', 0);
    this.state = {
      modalVisible: false,
      modalType: EditType.name,
      isPickerShow: false,
      deliveryDistance: 0,
      deliveryFee: 0,
      gotDeliveryFeeStatus: STATUS.NONE,
      putInFrontDoor: false,
      hasReceiverInfo: false,
      ordererInfo: { name, phone, address: ordererAddressName, lat: ordererLat, lng: ordererLng, },
      receiverInfo: { name: '', phone: '', address: '', lat: 0, lng: 0, },
      getDeliveryTimeStatus: STATUS.NONE
    };

    this._didFocusSubscription = this.props.navigation.addListener('didFocus', this._didFocus);
  }

  _didFocus = () => {
    this.props.screenProps.tabBar.show(true);
  };

  _onAndroidBack = () => {
    const { modalVisible, isPickerShow } = this.state;
    if (modalVisible) {
      this.setState({ modalVisible: false });
      return true;
    }
    if (isPickerShow) {
      this.setState({ isPickerShow: false });
      Picker.hide();
      return true;
    }
    return false;
  };

  componentDidMount() {
    this._getDeliveryFee();
  }

  componentWillUnmount() {
    if (this._didFocusSubscription) {
      this._didFocusSubscription.remove();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { restaurants } = this.props.cart;
    if (!_.isEqual(restaurants, nextProps.cart.restaurants)) {
      this._getDeliveryFee(nextProps);
    }

    if (this.props.auth.loggedIn !== nextProps.auth.loggedIn && !nextProps.auth.loggedIn) {
      this.setState({
        ordererInfo: {
          ...this.state.ordererInfo,
          name: '',
          phone: ''
        }
      });
    }

    if (nextProps.auth.loggedIn) {
      const { user: prevUser } = this.props.auth;
      const prevName = getString(prevUser, 'name');
      const prevPhone = getString(prevUser, 'phone');

      const { user } = nextProps.auth;
      const name = getString(user, 'name');
      const phone = getString(user, 'phone');

      if (prevName !== name || prevPhone !== phone) {
        this.setState({
          ordererInfo: {
            ...this.state.ordererInfo,
            name,
            phone
          }
        });
      }
    }
  }

  _goTo = (routeName, params = {}) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  _renderHeader = () => {
    const { cart } = this.props;
    const firstRestaurant = cart.restaurants[0];
    return (
      <TouchableOpacity
        style={styles.nearYouContainer}
        onPress={this._goTo(Routes.NearYou, {
          mainRestaurantId: cart.restaurants[0].id,
          restaurantName: firstRestaurant.name
        })}
      >
        <Text style={styles.restaurantNearYou}>See Groceries near {firstRestaurant.name}</Text>
        <Image style={styles.arrowImage} source={Images.arrow}/>
      </TouchableOpacity>
    );
  };

  _isValid = () => {
    const {
      ordererInfo,
      hasReceiverInfo,
      receiverInfo,
      gotDeliveryFeeStatus,
      getDeliveryTimeStatus
    } = this.state;
    const errorTitle = 'Something is missing...';
    if (ordererInfo.phone.length === 0) {
      Alert.alert(errorTitle, 'Please enter your phone.');
      return false;
    }

    if (ordererInfo.name.length === 0) {
      Alert.alert(errorTitle, 'Please enter your name.');
      return false;
    }

    if (
      ordererInfo.address.length === 0 ||
      (ordererInfo.lng === 0 && ordererInfo.lng === 0)
    ) {
      Alert.alert(errorTitle, 'Please select or input your address.');
      return false;
    }

    if (hasReceiverInfo) {
      if (receiverInfo.name.length === 0) {
        Alert.alert(errorTitle, "Please enter receiver's name.");
        return false;
      }

      if (receiverInfo.phone.length === 0) {
        Alert.alert(errorTitle, "Please enter receiver's phone.");
        return false;
      }

      if (
        receiverInfo.address.length === 0 ||
        (receiverInfo.lng === 0 && receiverInfo.lng === 0)
      ) {
        Alert.alert(errorTitle, "Please select or input receiver's address.");
        return false;
      }
    }

    if (gotDeliveryFeeStatus === STATUS.FAILED) {
      Alert.alert(errorTitle, 'Delivery fee is invalid. Please try to get Delivery fee again!');
      return false;
    }
    if (getDeliveryTimeStatus === STATUS.FAILED) {
      Alert.alert(errorTitle, 'Delivery time is invalid. Please try to get Delivery time again!');
      return false;
    }
    return true;
  };

  _getRestaurantNote = (resId) => {
    const { cart } = this.props;
    const restaurant = cart.restaurants.find(res => res.id === resId);
    return _.isNil(restaurant) ? '' : restaurant.note;
  };

  _getCheckoutParams = () => {
    const { cart } = this.props;
    const {
      ordererInfo,
      receiverInfo,
      hasReceiverInfo,
      deliveryFee,
      deliveryDistance,
      deliveryTime,
      putInFrontDoor
    } = this.state;
    const dishes = cart.items.map(item => {
      return {
        id: item.id,
        quantity: item.quantity,
        price: item.price,
        note: this._getRestaurantNote(item.restaurantId)
      };
    });
    const contactInfo = hasReceiverInfo ? receiverInfo : ordererInfo;
    return {
      dish: dishes,
      total_price: parseFloat(cart.total + deliveryFee).toFixed(2),

      // Orderer info
      user_name: ordererInfo.name,
      user_address: ordererInfo.address,
      user_address_lat: ordererInfo.lat,
      user_address_lng: ordererInfo.lng,

      // Receiver info
      delivery_name: contactInfo.name,
      delivery_mobile: contactInfo.phone,
      delivery_address: contactInfo.address,
      lat: contactInfo.lat,
      lng: contactInfo.lng,

      contact_info: `${contactInfo.name} / ${contactInfo.phone}`,
      distance: deliveryDistance,
      delivery_time: deliveryTime,
      delivery_fee: parseFloat(deliveryFee).toFixed(2),
      note: '',
      id_main_restaurant: cart.restaurants[0].id,

      putInFrontDoor
    };
  };

  _confirmPayment = async () => {
    if (!this._isValid()) return;
    const { showLoading, clearCart } = this.props;
    const checkoutParams = this._getCheckoutParams();

    showLoading(true);
    try {
      const profile = await api.getProfile();
      const wallet = _.get(profile, 'user_profile.wallet', 0);
      // check to have enough money in Wallet?
      if (wallet >= checkoutParams.total_price) {
        showLoading(true, { title: 'Paying with O-wallet...' });
        const result = await api.checkout(checkoutParams);
        const { id } = result;
        clearCart();
        this._goTo(Routes.PaymentSuccess, { id })();
      } else {
        this._goTo(Routes.Payment, {
          checkoutParams,
          onRefreshPriceChanges: this._refreshPriceChanges
        })();
      }
    } catch (error) {
      const errorTitle = 'Checking out';
      if (error.code === ERROR_CODE.PRICE_CHANGE) {
        Alert.alert(
          errorTitle,
          error.message,
          [
            {
              text: 'OK',
              onPress: this._refreshPriceChanges
            }
          ],
          { cancelable: false }
        );
      } else {
        errorManager.createError(errorTitle, error);
      }
    } finally {
      showLoading(false);
    }
  };

  _refreshPriceChanges = async () => {
    const { updatePriceChanges, cart, showLoading } = this.props;
    const dishesId = cart.items.map(item => item.id);
    showLoading(true);
    try {
      const result = await api.getDishesPrice({ dishes: dishesId });
      updatePriceChanges(result);
      subscriptionManager.notify(SUBSCRIPTION_TYPE.DISHES_PRICE_CHANGES, result);
    } catch (error) {
      errorManager.createError('Refresh price changes', error);
    } finally {
      showLoading(false);
    }
  };

  _onCheckBoxChange = (key) => (value, callback = () => {}) => {
    this.setState({ [key]: value }, callback);
  };

  _onGetDeliveryFee = () => {
    this._getDeliveryFee();
  };

  _renderDeliveryFee = () => {
    const { cart } = this.props;
    const { deliveryFee, deliveryDistance, gotDeliveryFeeStatus } = this.state;
    return (
      <DeliveryFee
        onGetDeliveryFee={this._onGetDeliveryFee}
        getDeliveryFeeFailed={gotDeliveryFeeStatus === STATUS.FAILED}
        total={cart.total}
        deliveryFee={deliveryFee}
        deliveryDistance={deliveryDistance}
      />
    );
  };

  _renderOrdererInfo = () => {
    const { auth } = this.props;
    const { ordererInfo } = this.state;
    return (
      <ContactInfo
        editablePhone={!auth.loggedIn}
        name={ordererInfo.name}
        phone={ordererInfo.phone}
        address={ordererInfo.address}
        onChangeName={this._editInfo(EditType.name)}
        onChangePhone={this._editInfo(EditType.phone)}
        onChangeAddress={this._editInfo(EditType.address)}
      />
    );
  };

  _showModal = (visible) => () => {
    this.setState({ modalVisible: visible, });
  };

  _editInfo = (type) => () => {
    this.setState({ modalType: type });
    switch (type) {
      case EditType.address: {
        const { ordererInfo } = this.state;
        this._goTo(Routes.SearchLocation, {
          returnLocation: this._changeAddress,
          address: ordererInfo.address
        })();
        break;
      }
      case EditType.receiverAddress: {
        const { receiverInfo } = this.state;
        this._goTo(Routes.SearchLocation, {
          returnLocation: this._changeReceiverAddress,
          address: receiverInfo.address
        })();
        break;
      }
      default: {
        this._showModal(true)();
        break;
      }
    }
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _renderEmpty = () => {
    return (
      <View style={styles.emptyCartContainer}>
        <Image source={Images.emptyCart} style={styles.imageEmptyCart} />
        <Text style={styles.emptyCartTitle}>{I18n.t('emptyCart')}</Text>
        <Text style={styles.emptyCartSubtitle}>{I18n.t('emptyCartDescription')}</Text>
      </View>
    );
  };

  _renderCartItem = (item) => {
    const { updateQuantity } = this.props;
    return (
      <OrderFood key={item.id} item={item} onQuantityChanged={updateQuantity}/>
    );
  };

  _updateNote = (res) => value => {
    const { updateNote } = this.props;
    updateNote(res, value);
  };

  _renderRestaurant = (res) => {
    const { cart } = this.props;
    return (
      <React.Fragment key={res.id}>
        <View style={[styles.OrderFoodContainer, { marginBottom: 5 }]}>
          <Text style={styles.restaurantName}>{res.name}</Text>
          <Text style={styles.restaurantAddress}>{res.address}</Text>
          {
            cart.items.map((item) => {
              return (item.restaurantId === res.id) ? this._renderCartItem(item) : null;
            })
          }
          <View style={styles.subTotalContainer}>
            <Text style={styles.subTotal}>{I18n.t('subtotal')}</Text>
            <Text style={styles.subTotalMoney}>${utils.numberWithCommas(res.subTotal)}</Text>
          </View>
        </View>
        <View style={[styles.OrderFoodContainer, styles.addNoteContainer]}>
          <TextInput
            placeholder={I18n.t('addNote')}
            onChangeText={this._updateNote(res)}
            value={res.note}
            style={styles.noteInput}
            multiline
            underlineColorAndroid={Colors.TRANSPARENT}
          />
        </View>
      </React.Fragment>
    );
  };

  _onCheckReceiverDiff = (value) => {
    this._onCheckBoxChange('hasReceiverInfo')(value, this._getDeliveryFee);
  };

  _renderConfirmation = () => {
    const { putInFrontDoor, hasReceiverInfo } = this.state;
    return (
      <View style={styles.confirmPaymentContainer}>
        <CheckBox
          title={I18n.t('putInFrontDoor')}
          onChange={this._onCheckBoxChange('putInFrontDoor')}
          checked={putInFrontDoor}
        />
        <CheckBox
          title={I18n.t('receiverDiff')}
          onChange={this._onCheckReceiverDiff}
          checked={hasReceiverInfo}
          style={styles.checkBoxReceiver}
        />
      </View>
    );
  };

  _renderReceiverInfo = () => {
    const { hasReceiverInfo, receiverInfo } = this.state;
    if (!hasReceiverInfo) return null;

    return (
      <ReceiverInfo
        name={receiverInfo.name}
        phone={receiverInfo.phone}
        address={receiverInfo.address}
        onChangeName={this._editInfo(EditType.receiverName)}
        onChangePhone={this._editInfo(EditType.receiverPhone)}
        onChangeAddress={this._editInfo(EditType.receiverAddress)}
      />
    );
  };

  _renderConfirmButton = () => {
    return (
      <View style={styles.confirmPaymentContainer}>
        <SolidButton
          text={I18n.t('payment')}
          onPress={this._confirmPayment}
          style={styles.paymentButton}
        />
      </View>
    );
  };

  _onChangeDeliveryTime = (deliveryTime) => {
    this.setState({ deliveryTime });
  };

  _onChangeStatusGetDeliveryTime = (status) => {
    this.setState({ getDeliveryTimeStatus: status });
  };

  _renderDeliveryTime = () => {
    const { cart } = this.props;
    return (
      <DeliveryTime
        restaurants={cart.restaurants}
        onChangeTime={this._onChangeDeliveryTime}
        onShowPicker={this._showPicker}
        onChangeStatus={this._onChangeStatusGetDeliveryTime}
      />
    );
  };

  _showPicker = (show) => {
    this.setState({ isPickerShow: show });
  };

  _renderCart = (cart) => {
    const { restaurants } = cart;
    return (
      <ScrollView keyboardDismissMode={'on-drag'}>
        {this._renderHeader()}
        {
          restaurants.map((res, index) => {
            return (this._renderRestaurant(res, index));
          })
        }
        {this._renderDeliveryFee()}
        {this._renderOrdererInfo()}
        {this._renderDeliveryTime()}
        {this._renderConfirmation()}
        {this._renderReceiverInfo()}
        {this._renderConfirmButton()}
      </ScrollView>
    );
  };

  _changeAddress = (formattedAddress, geometry) => {
    const { setOrderAddress } = this.props;
    const { ordererInfo } = this.state;
    const ordererAddress = {
      address: formattedAddress,
      lat: geometry.lat,
      lng: geometry.lng
    };
    this.setState({
      ordererInfo: {
        ...ordererInfo,
        ...ordererAddress
      }
    }, this._getDeliveryFee);

    // saved orderer's address to store for the next checkout
    setOrderAddress(ordererAddress);
  };

  _changeReceiverAddress = (formattedAddress, geometry) => {
    const { receiverInfo } = this.state;
    const receiverAddress = {
      address: formattedAddress,
      lat: geometry.lat,
      lng: geometry.lng
    };
    this.setState({
      receiverInfo: {
        ...receiverInfo,
        ...receiverAddress
      }
    }, this._getDeliveryFee);
  };

  _onChangeOrdererPhone = async (phone) => {
    this._showModal(false)();

    const { showLoading } = this.props;
    showLoading(true, { title: `Sending code to ${phone}...` });
    try {
      await api.sendSmsCode({ phone, app_target: 'customer' });
      this._goTo(Routes.EnterCode, {
        phone,
        onVerified: this._onVerifiedOrdererPhone
      })();
    } catch (error) {
      errorManager.createError('Sending a code', error);
    } finally {
      showLoading(false);
    }
  };

  _onVerifiedOrdererPhone = (phone) => {
    const { ordererInfo } = this.state;
    this.setState({ ordererInfo: { ...ordererInfo, phone } });
  };

  _renderContentModal = () => {
    const { modalType, receiverInfo, ordererInfo } = this.state;
    switch (modalType) {
      case EditType.name: {
        return (
          <ChangeInfo
            title={'NAME'}
            placeholder={'Enter name'}
            value={ordererInfo.name}
            onApply={this._onChangeOrdererInfo('name')}
            onClose={this._showModal(false)}
          />
        );
      }
      case EditType.phone: {
        return (
          <ChangeInfo
            title={'PHONE NUMBER'}
            placeholder={'Enter phone'}
            value={ordererInfo.phone}
            applyText={'Verify'}
            onApply={this._onChangeOrdererPhone}
            onClose={this._showModal(false)}
            textInputProps={{
              keyboardType: 'phone-pad',
              maxLength: 15
            }}
            phoneNumber
          />
        );
      }
      case EditType.receiverName: {
        return (
          <ChangeInfo
            title={'NAME'}
            placeholder={"Enter receiver's name"}
            value={receiverInfo.name}
            onApply={this._onChangeReceiverInfo('name')}
            onClose={this._showModal(false)}
          />
        );
      }
      case EditType.receiverPhone: {
        return (
          <ChangeInfo
            title={'PHONE NUMBER'}
            placeholder={"Enter receiver's phone number"}
            value={receiverInfo.phone}
            onApply={this._onChangeReceiverInfo('phone')}
            onClose={this._showModal(false)}
            textInputProps={{
              keyboardType: 'phone-pad',
              maxLength: 15
            }}
            phoneNumber
          />
        );
      }
      default: return null;
    }
  };

  _onChangeOrdererInfo = (key) => (value) => {
    const { ordererInfo } = this.state;
    this.setState({ ordererInfo: { ...ordererInfo, [key]: value } });
    this._showModal(false)();
  };

  _onChangeReceiverInfo = (key) => (value) => {
    const { receiverInfo } = this.state;
    this.setState({ receiverInfo: { ...receiverInfo, [key]: value } });
    this._showModal(false)();
  };

  _getDeliveryFee = async (props = this.props) => {
    const { ordererInfo, receiverInfo, hasReceiverInfo } = this.state;
    const { cart, showLoading } = props;

    let shouldCalculateFee = true;
    if (cart.restaurants.length === 0) {
      shouldCalculateFee = false;
    } else if (hasReceiverInfo) {
      if (receiverInfo.address.length === 0 || (receiverInfo.lat === 0 && receiverInfo.lng === 0)) {
        shouldCalculateFee = false;
      }
    } else {
      if (ordererInfo.address.length === 0 || (ordererInfo.lat === 0 && ordererInfo.lng === 0)) {
        shouldCalculateFee = false;
      }
    }

    if (shouldCalculateFee) {
      const { lat, lng } = hasReceiverInfo ? receiverInfo : ordererInfo;
      const listResIds = cart.restaurants.map(res => res.id);
      const params = { list: listResIds, lat, lng };
      showLoading(true, { title: 'Getting delivery fee...' });
      try {
        const deliveryFee = await api.getDeliveryFee(params);
        this.setState({
          deliveryDistance: deliveryFee.distance,
          deliveryFee: deliveryFee.price,
          gotDeliveryFeeStatus: STATUS.SUCCESS,
        });
      } catch (error) {
        errorManager.createError('Delivery Information', error);
        this.setState({
          deliveryDistance: 0,
          deliveryFee: 0,
          gotDeliveryFeeStatus: STATUS.FAILED,
        });
      } finally {
        showLoading(false);
      }
    } else {
      this.setState({
        deliveryDistance: 0,
        deliveryFee: 0,
        gotDeliveryFeeStatus: STATUS.NONE,
      });
    }
  };

  render() {
    const { cart, navigation } = this.props;
    const { modalVisible, isPickerShow } = this.state;
    return (
      <Background containerStyle={styles.container}>
        <AndroidBackHandler navigation={navigation} onBack={this._onAndroidBack}/>
        <Header onTouchBackButton={this._goBack} hideBackButton title='YOUR CART'/>
        {cart.items.length === 0 ? this._renderEmpty() : this._renderCart(cart)}
        <Modal
          animationType={'fade'}
          transparent={true}
          visible={modalVisible}
          supportedOrientations={['portrait']}
          onRequestClose={this._onAndroidBack}
        >
          {this._renderContentModal()}
        </Modal>
        {isPickerShow && <View style={styles.overlay}/>}
      </Background>
    );
  }
}

Cart.propTypes = {
  updateQuantity: PropTypes.func.isRequired,
  updateNote: PropTypes.func.isRequired,
  clearCart: PropTypes.func.isRequired,
  updatePriceChanges: PropTypes.func.isRequired,
  setOrderAddress: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  cart: PropTypes.object.isRequired,
};

const mapStateToProps = ({ cart, auth }) => ({ cart, auth });

const mapDispatchToProps = {
  updateQuantity: actions.updateQuantity,
  updateNote: actions.updateNote,
  clearCart: actions.clearCart,
  updatePriceChanges: actions.updatePriceChanges,
  setOrderAddress: actions.setOrderAddress,
};

const enhancer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  WithLoading
);

export default enhancer(Cart);
