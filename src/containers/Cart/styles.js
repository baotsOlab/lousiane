import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.OLIVERY_WHITE,
  },
  overlay: {
    height: '100%',
    width: '100%',
    backgroundColor: Colors.BLACK_OPACITY40,
    position: 'absolute'
  },
  listView: {
    flex: 1,
  },
  restaurantInfoContainer: {
    backgroundColor: Colors.WHITE,
    width: deviceWidth - 20,
    alignSelf: 'center',
  },
  restaurantName: {
    fontFamily: Fonts.MEDIUM,
    fontSize: 20,
    color: Colors.OLIVERY_GREEN1,
    marginTop: 20,
    marginLeft: 10,
  },
  restaurantAddress: {
    fontFamily: Fonts.REGULAR,
    fontSize: 13,
    color: Colors.OLIVERY_GRAY3,
    marginTop: 5,
    marginLeft: 10,
  },
  OrderFoodContainer: {
    backgroundColor: Colors.WHITE,
    width: deviceWidth - 20,
    alignSelf: 'center',
  },
  subTotalNoteContainer: {
    width: deviceWidth - 20,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE
  },
  dash: {
    width: deviceWidth - 40,
    height: 1,
    marginHorizontal: 10,
    marginTop: 30,
    alignSelf: 'center',
  },
  subTotalContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: Colors.OLIVERY_GRAY1,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 15,
    paddingBottom: 15,
    width: deviceWidth - 40,
  },
  subTotal: {
    fontFamily: Fonts.BOLD,
    fontSize: 14,
    color: Colors.OLIVERY_GRAY3,
  },
  subTotalMoney: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    flex: 1,
    textAlign: 'right',
    marginRight: 10,
  },
  noteContainer: {
    width: deviceWidth - 20,
    alignSelf: 'center',
    borderBottomColor: Colors.OLIVERY_WHITE,
    borderBottomWidth: 10,
  },
  noteInput: {
    width: deviceWidth - 40,
    alignSelf: 'center',
    fontFamily: Fonts.REGULAR,
    fontSize: 13,
    color: Colors.OLIVERY_BLACK1,
  },
  nearYouContainer: {
    backgroundColor: Colors.WHITE,
    width: deviceWidth - 20,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 4,
    flexDirection: 'row',
    alignItems: 'center'
  },
  restaurantNearYou: {
    flex: 1,
    fontFamily: Fonts.REGULAR,
    fontSize: 13,
    color: Colors.OLIVERY_GREEN1,
    marginVertical: 16,
    marginLeft: 10,
  },
  arrowImage: {
    width: 6,
    height: 10,
    marginRight: 10,
    resizeMode: 'contain',
  },

  emptyCartContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageEmptyCart: {
    width: 144,
    height: 143
  },
  emptyCartTitle: {
    fontSize: 24,
    marginTop: 20
  },
  emptyCartSubtitle: {
    fontSize: 16,
    marginTop: 10,
    color: Colors.OLIVERY_GRAY3
  },
  addNoteContainer: {
    marginBottom: 10,
    padding: 10,
    paddingBottom: 15
  },
  paymentButton: {
    marginTop: 0,
    marginBottom: 20,
  },
  confirmPaymentContainer: {
    backgroundColor: Colors.TRANSPARENT,
    alignSelf: 'center',
    width: deviceWidth - 20,
    marginTop: 20,
  },
  checkBoxReceiver: {
    marginTop: 12,
  }
});
