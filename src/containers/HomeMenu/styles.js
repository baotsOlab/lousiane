import { StyleSheet } from 'react-native';
import { disPlayScale } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.OLIVERY_WHITE
  },
  contentContainer: {
    paddingTop: 60
  },
  overlay: {
    height: '100%',
    width: '100%',
    backgroundColor: Colors.BLACK_OPACITY40,
    position: 'absolute'
  },
  imageContainer: {
    width: '100%',
    aspectRatio: 375 / 175,
    flexDirection: 'row',
    alignItems: 'stretch',
    position: 'absolute'
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  headerContainer: {
    width: '100%',
  },
  restaurantContainer: {
    width: '94.6667%',
    alignSelf: 'center',
    overflow: 'hidden',
    paddingBottom: 12,
    backgroundColor: Colors.WHITE,
  },
  statusLikeContainer: {
    flexDirection: 'row',
    width: '100%',
  },
  openStatusContainer: {
    height: 20,
    marginLeft: 20,
    marginVertical: 10,
    backgroundColor: Colors.OLIVERY_GREEN3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  delayStatusContainer: {
    height: 20,
    marginLeft: 20,
    marginVertical: 10,
    backgroundColor: Colors.OLIVERY_ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  closeStatusContainer: {
    height: 20,
    marginLeft: 20,
    marginVertical: 10,
    backgroundColor: Colors.OLIVERY_RED2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  status: {
    color: Colors.WHITE,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
    marginHorizontal: 8,
  },
  likeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  numOfLike: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 14,
    fontFamily: Fonts.REGULAR
  },
  likeButton: {
    width: 16,
    height: 14,
    marginRight: 20,
    marginLeft: 6
  },
  name: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 20,
    fontFamily: Fonts.MEDIUM,
    marginLeft: 20,
  },
  address: {
    color: Colors.OLIVERY_BLACK2,
    fontSize: 13,
    fontFamily: Fonts.REGULAR,
    marginTop: 5,
    marginLeft: 20,
  },
  timeOpen: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 13,
    fontFamily: Fonts.REGULAR,
    marginTop: 5,
    marginLeft: 20,
  },
  mainDishesContainer: {
    backgroundColor: Colors.OLIVERY_WHITE,
    width: '94.6667%',
    alignSelf: 'center'
  },
  mainDishes: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 14,
    fontFamily: Fonts.MEDIUM,
    marginVertical: 20,
  },
  cellContainer: {
    backgroundColor: Colors.WHITE,
    width: '45.86666666667%',
    marginLeft: 10 * disPlayScale,
  },
  menuImageContainer: {
    width: '97.093%',
    aspectRatio: 172 / 106,
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 5 * disPlayScale,
    alignSelf: 'center',
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  menuImage: {
    flex: 1,
    width: null,
    height: null
  },
  menuTitle: {
    backgroundColor: Colors.TRANSPARENT,
    marginLeft: 10,
    marginTop: 10,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  menuNumOfItems: {
    backgroundColor: Colors.TRANSPARENT,
    marginLeft: 10,
    marginTop: 5,
    color: Colors.OLIVERY_GRAY2,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
  },
  bottomSpace: {
    backgroundColor: Colors.OLIVERY_WHITE,
    width: '100%',
    height: 10,
    marginTop: 10
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
