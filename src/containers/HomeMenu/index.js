import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import { compose } from 'recompose';
import Header from '../../components/Header';
import Background from '../../components/Background';
import { convertToHourString } from '../../utils/string';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import WithLoading from '../../hoc/WithLoading';
import Loading from '../../components/Loading';
import { RESTAURANT_STATUS } from '../../assets/constants/constant';
import WithError from '../../hoc/WithError';
import I18n from '../../i18n';

/* eslint-disable react/no-deprecated */
class HomeMenu extends Component {
  restaurantId = 9;
  constructor(props) {
    super(props);
    this.state = {
      isLike: false,
      restaurantInfo: {},
    };
  }

  componentDidMount() {
    // const { restaurantId } = this.props.navigation.state.params;
    this._getRestaurantInfo()();
  }

  // componentWillReceiveProps(nextProps) {
  //   const { restaurantId: prevRestaurantId } = this.props.navigation.state.params;
  //   const { restaurantId } = nextProps.navigation.state.params;
  //   if (prevRestaurantId !== restaurantId) {
  //     this._getRestaurantInfo(restaurantId)();
  //   }
  // }

  _getRestaurantInfo = () => async () => {
    const { showLoading, showError } = this.props;
    showLoading(true, { title: 'Getting grocery/restaurant information...', showIndicator: false });
    showError('', null);
    try {
      const result = await api.getRestaurantInfo(this.restaurantId);
      this.setState({
        restaurantInfo: result,
        isLike: result.data.like
      });
    } catch (error) {
      showError('Cannot get restaurant info', error, this._getRestaurantInfo());
    } finally {
      showLoading(false);
    }
  };

  _like = (id) => async () => {
    const { isLike, restaurantInfo } = this.state;
    this.setState({ isLike: !isLike });
    try {
      await api.like(id);
      const { onLike = () => {} } = this.props.navigation.state.params;
      onLike();
      let likeNumber = restaurantInfo.data.like_number;
      likeNumber += (!isLike ? 1 : -1);
      this.setState({
        restaurantInfo: {
          ...restaurantInfo,
          data: {
            ...restaurantInfo.data,
            like_number: likeNumber
          }
        }
      });
    } catch (error) {
      errorManager.createError('Adding to favorites', error);
      this.setState({ isLike });
    }
  };

  _getStyleByStatus = (status) => {
    switch (status) {
      case RESTAURANT_STATUS.OPEN:
        return styles.openStatusContainer;
      case RESTAURANT_STATUS.CLOSE:
        return styles.closeStatusContainer;
      default:
        return styles.delayStatusContainer;
    }
  };

  _renderHeader = (info) => () => {
    if (!info) return null;
    const { isLike } = this.state;
    const {
      id,
      status,
      like_number: numLikes,
      name,
      address,
      working_date: workingDate,
      from,
      to,
      timezone
    } = info;
    const statusContainer = this._getStyleByStatus(status);
    return (
      <View style={styles.headerContainer}>
        <View style={styles.restaurantContainer}>
          <View style={styles.statusLikeContainer}>
            <View style={statusContainer}>
              <Text style={styles.status}>{status}</Text>
            </View>
            <View style={styles.likeContainer}>
              <Text style={styles.numOfLike}>{numLikes}</Text>
              <TouchableOpacity style={styles.likeButton} onPress={this._like(id)}>
                <Image style={styles.image} source={isLike ? Images.like : Images.likeGray} />
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.address}>{address}</Text>
          <Text style={styles.timeOpen}>
            {
              this._workingDate(workingDate)
            } | {
              convertToHourString(from, timezone)} - {convertToHourString(to, timezone)
            }
          </Text>
        </View>
        <View style={styles.mainDishesContainer}>
          <Text style={styles.mainDishes}>{I18n.t('shoppingMenu')}</Text>
        </View>
      </View>
    );
  };

  _renderItem = ({ item }) => {
    const { image = '', name, items } = item;
    return (
      <TouchableOpacity style={styles.cellContainer} onPress={this._touchItem(item)}>
        <View style={styles.menuImageContainer}>
          <Image style={styles.menuImage} source={{ uri: image }}/>
        </View>
        <Text style={styles.menuTitle} numberOfLines={1}>{name}</Text>
        <Text style={styles.menuNumOfItems} numberOfLines={1}>
          {I18n.t('items', { count: items })}
        </Text>
        <View style={styles.bottomSpace}/>
      </TouchableOpacity>
    );
  };

  _workingDate = (workingDates) => {
    return workingDates.map((workingDate, index) => {
      if (index === 0) {
        return workingDate;
      }
      return ` - ${workingDate}`;
    });
  };

  _touchItem = (item) => () => {
    const { restaurantInfo } = this.state;
    const { navigation } = this.props;
    navigation.navigate(Routes.HomeMenuDetail, {
      menuId: item.id,
      restaurantInfo
    });
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _keyExtractor = item => item.id;

  _renderContent = () => {
    const { restaurantInfo } = this.state;
    const menu = _.get(restaurantInfo, 'menu', []);
    return (
      <FlatList
        contentContainerStyle={styles.contentContainer}
        keyExtractor={this._keyExtractor}
        data={menu}
        numColumns={2}
        renderItem={this._renderItem}
        ListHeaderComponent={this._renderHeader(restaurantInfo.data)}
        stickyHeaderIndices={[0]}
      />
    );
  };

  _renderLoading = () => {
    const { loadingText } = this.props;
    return (
      <View style={styles.loadingContainer}>
        <Loading title={loadingText} />
      </View>
    );
  };

  _renderHeaderImage = () => {
    const { restaurantInfo } = this.state;
    const image = _.get(restaurantInfo, 'data.image', '');
    const imageSource = _.isEmpty(image) ? Images.food : { uri: image };
    return (
      <View style={styles.imageContainer}>
        <Image source={imageSource} style={styles.image}/>
      </View>
    );
  };

  render() {
    const { loading } = this.props;
    return (
      <Background containerStyle={styles.container}>
        { !loading && this._renderHeaderImage() }
        <Header onTouchBackButton={this._goBack} shouldTransparent hideBackButton={true}/>
        { loading ? this._renderLoading() : this._renderContent() }
      </Background>
    );
  }
}

const enhancer = compose(
  WithLoading,
  WithError
);
export default enhancer(HomeMenu);
