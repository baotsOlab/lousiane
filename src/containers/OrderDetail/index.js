import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
} from 'react-native';
import _ from 'lodash';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import WithLoading from '../../hoc/WithLoading';
import errorManager from '../../services/errorManager';
import api from '../../services/api';
import Loading from '../../components/Loading';
import { getString, getNumber, convertToTimeUTCString } from '../../utils/string';
import cartUtils from '../../utils/cartUtils';
import utils from '../../utils';

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderInfo: {}
    };
  }

  componentDidMount() {
    this._loadOrderDetail();
  }

  _getStatusIcon = (status) => {
    const icon = cartUtils.getOrderStatusIcon(status);
    return <Image style={styles.tickIcon} source={icon}/>;
  };

  _loadOrderDetail = async () => {
    const { id } = this.props.navigation.state.params;
    const { showLoading } = this.props;
    showLoading(true, { showIndicator: false });
    try {
      const result = await api.getOrderDetail(id);
      this.setState({ orderInfo: result });
    } catch (error) {
      errorManager.createError('Order Detail', error);
    } finally {
      showLoading(false);
    }
  };

  _detailDishes = (dishes) => {
    return dishes.map((meals) => {
      return (
        <View key={meals.dish.id} style={styles.mealNameRow}>
          <View style={styles.col1}>
            <Text style={styles.mealName}>{meals.dish.name}</Text>
          </View>
          <View style={styles.col2}>
            <Text style={styles.mealPrice}>
              ${utils.numberWithCommas(meals.dish.price)} x {meals.quantity}
            </Text>
          </View>
        </View>
      );
    });
  };

  detailRestaurant = (detail) => {
    return detail.map(({ restaurant, dishes }) => {
      return (
        <View key={restaurant.id}>
          <View style={styles.marginHorizontalContent}>
            <Text style={styles.restauentName}>{restaurant.name}</Text>
            <View style={styles.wrapLocation}>
              <View style={styles.wrapLocationImg}>
                <Image style={styles.locationIcon} source={Images.locationIcon}/>
              </View>
              <Text style={styles.locationText}>{restaurant.address}</Text>
            </View>
            <View style={styles.wrapMeal}>
              {this._detailDishes(dishes)}
            </View>
          </View>
          <View style={styles.breakLine} />
        </View>
      );
    });
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _renderContent = () => {
    const { orderInfo } = this.state;
    const orderId = getNumber(orderInfo, 'cart.id');
    const orderDate = getString(orderInfo, 'cart.created_at');
    const status = getString(orderInfo, 'cart.status');
    const totalPrice = getNumber(orderInfo, 'cart.total_price');
    const shipFee = getNumber(orderInfo, 'cart.delivery_fee');
    const distance = getNumber(orderInfo, 'cart.distance');
    const detail = _.chain(orderInfo).get('detail').defaultTo([]).value();

    let subTotal = 0;
    if (!_.isEmpty(orderInfo)) {
      subTotal = parseFloat(totalPrice) - parseFloat(shipFee);
    }
    return (
      <ScrollView style={styles.scrollViewStyle} contentContainerStyle={styles.wrapContent}>
        <View style={styles.marginHorizontalContent}>
          <Text style={styles.date}>{convertToTimeUTCString(orderDate)}</Text>
          <View style={styles.orderIdRow}>
            <View style={styles.col1}>
              <Text style={styles.orderIdText}>Order ID: {orderId}</Text>
            </View>
            <View style={styles.col2}>
              {this._getStatusIcon(status)}
              <Text style={styles.statusOrder}>{status}</Text>
            </View>
          </View>
        </View>

        {this.detailRestaurant(detail)}

        <View style={styles.marginHorizontalContent}>
          <View style={styles.verticalFee}>
            <View style={styles.feeRow}>
              <View style={styles.col1}>
                <Text style={styles.mealName}>Subtotal</Text>
              </View>
              <View style={styles.col2}>
                <Text style={styles.mealPrice}>${utils.numberWithCommas(subTotal)}</Text>
              </View>
            </View>
            <View style={styles.feeRow}>
              <View style={styles.col1}>
                <Text style={styles.mealName}>Shipping fee ({distance} km)</Text>
              </View>
              <View style={styles.col2}>
                <Text style={styles.mealPrice}>${utils.numberWithCommas(shipFee)}</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.marginHorizontalContent}>
          <View style={styles.totalRow}>
              <View style={styles.col1Total}>
                <Text style={styles.totalText}>TOTAL</Text>
              </View>
              <View style={styles.col2}>
                <Text style={styles.totalPrice}>${utils.numberWithCommas(totalPrice)}</Text>
              </View>
            </View>
        </View>
      </ScrollView>
    );
  };

  render() {
    const { loading } = this.props;
    return (
      <Background containerStyle={styles.container}>
        <Header onTouchBackButton={this._goBack} title='ORDER DETAIL'/>
        { loading && <Loading/> }
        { !loading && this._renderContent() }
      </Background>
    );
  }
}

export default WithLoading(OrderDetail);
