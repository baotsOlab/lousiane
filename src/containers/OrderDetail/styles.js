import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.OLIVERY_GRAY1,
  },
  scrollViewStyle: {
    margin: 10,
  },
  wrapContent: {
    backgroundColor: Colors.WHITE,
    paddingVertical: 20,
  },
  marginHorizontalContent: {
    marginHorizontal: 10,
  },
  date: {
    fontSize: 12,
    fontFamily: Fonts.MEDIUM,
    color: Colors.OLIVERY_GRAY3
  },
  orderIdRow: {
    flex: 1,
    flexDirection: 'row'
  },
  col1: {
    flex: 3,
  },
  col2: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  orderIdText: {
    fontSize: 24,
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_GREEN1
  },
  statusOrder: {
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_GRAY3
  },
  tickIcon: {
    marginRight: 5,
    width: 14,
    height: 14,
    resizeMode: 'contain'
  },
  restauentName: {
    marginTop: 20,
    fontSize: 16,
    fontFamily: Fonts.BOLD,
    color: Colors.OLIVERY_BLACK2
  },
  wrapLocation: {
    flex: 1,
    flexDirection: 'row',
  },
  locationIcon: {
    width: 9,
    height: 12,
    resizeMode: 'contain'
  },
  wrapLocationImg: {
    justifyContent: 'center',
    marginRight: 6,
  },
  locationText: {
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_GRAY3
  },
  mealNameRow: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 4
  },
  mealName: {
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_BLACK1
  },
  mealPrice: {
    fontSize: 14,
    fontFamily: Fonts.REGULAR,
    color: Colors.OLIVERY_GRAY3
  },
  wrapMeal: {
    marginTop: 11,
    marginBottom: 16
  },
  breakLine: {
    borderBottomWidth: 1,
    borderColor: Colors.OLIVERY_GRAY1
  },
  feeRow: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 4
  },
  verticalFee: {
    marginTop: 16,
    marginBottom: 8
  },
  totalRow: {
    flex: 1,
    flexDirection: 'row',
  },
  totalText: {
    fontSize: 14,
    fontFamily: Fonts.BOLD,
    color: Colors.OLIVERY_GREEN1
  },
  totalPrice: {
    fontSize: 18,
    fontFamily: Fonts.BOLD,
    color: Colors.OLIVERY_GREEN1
  },
  col1Total: {
    flex: 3,
    justifyContent: 'center'
  }
});
