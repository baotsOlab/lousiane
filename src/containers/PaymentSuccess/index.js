import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
} from 'react-native';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import I18n from '../../i18n';
import dataManager from '../../assets/constants/dataManager';

/* eslint-disable react/no-deprecated */
class PaymentSuccess extends Component {
  componentDidMount() {
    dataManager.fetchProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isFocused && nextProps.cart.restaurants.length > 0) {
      nextProps.navigation.goBack();
    }
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _goToPaymentDetail = () => {
    const { navigation } = this.props;
    const { id } = navigation.state.params;
    this.props.navigation.navigate(Routes.OrderStatus, { id });
  };

  render() {
    return (
      <Background>
        <Header onTouchBackButton={this._goBack} title='PAYMENT'/>
        <View style={styles.container}>
          <Text style={styles.success}>{I18n.t('success')}</Text>
          <View style={styles.successImageContainer}>
            <Image style={styles.successImage} source={Images.successCircle}/>
          </View>
          <Text style={styles.description}>
            You have successfully made an order at Olivery.
            Thank you.
          </Text>
          <View style={styles.applyButtonContainer}>
            <SolidButton
              text={I18n.t('continue')}
              onPress={this._goToPaymentDetail}
              fontSize={18}
              bold={true}
            />
          </View>
        </View>
      </Background>
    );
  }
}

const mapStateToProps = ({ cart }) => ({ cart });
const enhancer = compose(
  withNavigationFocus,
  connect(mapStateToProps)
);
export default enhancer(PaymentSuccess);
