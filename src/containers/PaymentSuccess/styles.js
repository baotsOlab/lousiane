import { StyleSheet } from 'react-native';
import { deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  success: {
    fontFamily: Fonts.BOLD,
    fontSize: 24,
    textAlign: 'center',
    color: Colors.OLIVERY_GREEN1,
    marginVertical: 40,
  },
  successImageContainer: {
    width: '44.8%',
    aspectRatio: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  successImage: {
    flex: 1,
    width: null,
    height: null
  },
  description: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    textAlign: 'center',
    color: Colors.OLIVERY_BLACK1,
    marginTop: 40,
    marginHorizontal: 40,
  },
  applyButtonContainer: {
    flex: 1,
    width: deviceWidth - 20,
    marginBottom: 20,
    justifyContent: 'flex-end'
  },
});
