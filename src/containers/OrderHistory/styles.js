import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  containerStyle: {
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  wrapContentItem: {
    backgroundColor: Colors.WHITE,
    marginTop: 10
  },
  wrapContent: {
    marginVertical: 15,
    marginHorizontal: 10,
  },
  rowContent: {
    flex: 1,
    flexDirection: 'row'
  },
  rowContent1: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 12,
    marginBottom: 8
  },

  colId: {
    flex: 1,
  },
  colDate: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  orderIdText: {
    color: Colors.OLIVERY_BLACK1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM
  },
  dateText: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 12,
    fontFamily: Fonts.MEDIUM
  },
  wrapTitle: {
    flex: 1
  },
  wrapDetail: {
    flex: 4.5
  },
  breakLine: {
    borderBottomWidth: 1,
    borderColor: Colors.OLIVERY_GRAY1
  },
  colStatus: {
    flex: 2.5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  colDetail: {
    flex: 1,
    alignItems: 'center',
    borderLeftWidth: 1,
    borderColor: Colors.OLIVERY_GRAY1,
    paddingTop: 15,
    paddingBottom: 15
  },
  detailText: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 14,
    fontFamily: Fonts.BOLD
  },
  title: {
    color: Colors.OLIVERY_GRAY2,
    fontSize: 16,
    fontFamily: Fonts.REGULAR
  },
  detail: {
    color: Colors.OLIVERY_BLACK2,
    fontSize: 16,
    fontFamily: Fonts.REGULAR
  },
  status: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 14,
    fontFamily: Fonts.REGULAR
  },
  tickIcon: {
    marginHorizontal: 10,
    width: 14,
    height: 14,
    resizeMode: 'contain'
  },
  footerContainerStyle: {
    marginVertical: 12,
  }
});
