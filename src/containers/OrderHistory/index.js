import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import Background from '../../components/Background';
import Header from '../../components/Header';
import { convertToTimeUTCString } from '../../utils/string';
import styles from './styles';
import Routes from '../../navigation/routes';
import WithLoading from '../../hoc/WithLoading';
import api from '../../services/api';
import CommonList from '../../components/CommonList';
import { NOTIFICATION_TYPE, ORDER_PHASES } from '../../assets/constants/constant';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../../services/subscriptionManager';
import utils from '../../utils';
import cartUtils from '../../utils/cartUtils';

class OrderHistory extends Component {
  componentDidMount() {
    subscriptionManager.subscribe(SUBSCRIPTION_TYPE.NOTIFICATION, this._onNotification);
    this.commonList.loadData();
  }

  componentWillUnmount() {
    subscriptionManager.unsubscribe(SUBSCRIPTION_TYPE.NOTIFICATION, this._onNotification);
  }

  _onNotification = (notificationData) => {
    const payload = JSON.parse(notificationData.data);
    const { cart_id: cartId, status, type } = payload;
    if (type !== NOTIFICATION_TYPE.ORDER_STATUS) return;
    if (this.commonList) {
      const list = [...this.commonList.state.list];
      const foundIndex = list.findIndex(item => item.id === cartId);
      if (foundIndex > -1) {
        list[foundIndex].status = status;
      }
      this.commonList.updateList(list);
    }
  };

  _getStatusIcon = (status) => {
    const icon = cartUtils.getOrderStatusIcon(status);
    return <Image style={styles.tickIcon} source={icon}/>;
  };

  _goTo = (routeName, params = {}) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  _goToDetail = (item) => () => {
    switch (item.status) {
      case ORDER_PHASES.CONFIRMATION:
      case ORDER_PHASES.PICKUP: {
        this._goTo(Routes.OrderStatus, { id: item.id, shouldGoBack: true })();
        break;
      }
      default: {
        this._goTo(Routes.OrderDetail, { id: item.id })();
      }
    }
  };

  _renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.wrapContentItem}>
        <View style={styles.wrapContent}>
          <View style={styles.rowContent}>
            <View style={styles.colId}>
              <Text style={styles.orderIdText}>Order ID: {item.id}</Text>
            </View>
            <View style={styles.colDate}>
              <Text style={styles.dateText}>{item.created_at ? convertToTimeUTCString(item.created_at) : ''}</Text>
            </View>
          </View>
          <View style={styles.rowContent1}>
            <View style={styles.wrapTitle}>
              <Text style={styles.title}>Partner:</Text>
            </View>
            <View style={styles.wrapDetail}>
              <Text style={styles.detail}>{item.partner.join(' - ')}</Text>
            </View>
          </View>
          <View style={styles.rowContent}>
            <View style={styles.wrapTitle}>
              <Text style={styles.title}>Total:</Text>
            </View>
            <View style={styles.wrapDetail}>
              <Text style={styles.detail}>${utils.numberWithCommas(item.total_price)}</Text>
            </View>
          </View>
        </View>
        <View style={styles.breakLine}/>
        <View style={styles.rowContent}>
          <View style={styles.colStatus}>
            {this._getStatusIcon(item.status)}
            <Text style={styles.status}>{item.status}</Text>
          </View>
          <TouchableOpacity
            onPress={this._goToDetail(item)}
            style={styles.colDetail}>
            <Text style={styles.detailText}>DETAIL</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  _loadData = async (page, take) => {
    const result = await api.getOrders({ page, take });
    return result.data;
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Background containerStyle={styles.containerStyle}>
        <Header onTouchBackButton={this._goBack} title='ORDER HISTORY'/>
        <CommonList
          ref={ref => { this.commonList = ref; }}
          loadData={this._loadData}
          renderItem={this._renderItem}
          footerContainerStyle={styles.footerContainerStyle}
        />
      </Background>
    );
  }
}

export default WithLoading(OrderHistory);
