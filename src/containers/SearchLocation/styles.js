import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapSearchView: {
    flexDirection: 'row',
  },
  searchSection: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 10
  },
  wrapClearIcon: {
    paddingVertical: 20
  },
  cancelFrame: {
    width: 16,
    height: 16,
    resizeMode: 'contain'
  },
  input: {
    flex: 1,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    paddingVertical: 20,
    marginHorizontal: 12
  },
  flatListStyle: {
    backgroundColor: Colors.OLIVERY_GRAY1,
  },
  rowContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    backgroundColor: Colors.WHITE,
  },
  detailLocationStyle: {
    fontSize: 12,
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    marginLeft: 10,
  },

  typeText: {
    fontSize: 12,
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    marginLeft: 10,
  },

  noResultView: {
    alignItems: 'center'
  },

  noResultText: {
    fontFamily: Fonts.REGULAR,
    fontSize: 12,
    color: Colors.OLIVERY_BLACK1,
    marginVertical: 12,
    marginHorizontal: 12
  },

  breakLine: {
    marginTop: 20,
    borderBottomWidth: 1,
    borderColor: Colors.OLIVERY_GRAY1,
  },
  breakLineSearch: {
    borderBottomWidth: 3,
    borderColor: Colors.OLIVERY_GRAY1
  },
  locationIcon: {
    width: 10,
    height: 14,
    resizeMode: 'contain'
  },
  wrapRowItem: {
    flexDirection: 'row'
  },
  edit: {
    width: 15,
    height: 12,
    margin: 8
  },
  inputTitle: {
    flex: 1,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_GREEN1,
    paddingVertical: 20,
    marginHorizontal: 12
  }
});
