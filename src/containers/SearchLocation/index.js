import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  TextInput,
  Keyboard
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  ADDRESS_TYPES,
} from '../../assets/constants/constant';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Colors from '../../common/Colors';
import { getNumber, getString } from '../../utils/string';
import env from '../../env';
import api from '../../services/api';

const clearButtonHitSlop = {
  top: 22, right: 22, bottom: 22, left: 22
};

class SearchLocation extends Component {
  _cachedLocations = {};

  constructor(props) {
    super(props);
    const { params } = props.navigation.state;

    const userLocations = [];
    if (!_.has(params, 'title')) {
      const { user } = props;
      [
        { key: 'address_1' },
        { key: 'address_2' },
        { key: 'address_3' },
      ].forEach(({ key }) => {
        const title = getString(user, `user_profile.${key}_title`);
        const address = getString(user, `user_profile.${key}`);
        const lat = getNumber(user, `user_profile.${key}_lat`);
        const lng = getNumber(user, `user_profile.${key}_lng`);
        if (address.length > 0) {
          userLocations.push({
            type: title,
            formatted_address: address,
            geometry: { location: { lat, lng } }
          });
        }
      });
    }
    this.state = {
      address: _.get(params, 'address', ''),
      listLocation: [...userLocations],
      userLocations,
      title: params.title
    };
    this.searchDebounce = _.debounce(this.search, 400);
  }

  componentDidMount() {
    this.search();
  }

  _onPressItem = (item) => () => {
    Keyboard.dismiss();
    const { navigation } = this.props;
    const { state: { params: { returnLocation, returnTitle } } } = navigation;
    returnLocation(
      item.formatted_address,
      item.geometry.location,
    );
    if (returnTitle) {
      setTimeout(() => returnTitle(this.state.title), 0);
    }
    navigation.goBack();
  };

  search = async () => {
    const { address, userLocations } = this.state;
    let listLocation = [];
    if (this._cachedLocations[address]) {
      listLocation = this._cachedLocations[address];
    } else {
      const url = env.googleAPI.maps.GET_LOCATIONS_GOOGLE_MAP_URL.replace('{address}', address);
      const json = await api.fetchGoogleAPI(url);
      listLocation = json.results.filter(result => {
        return (
          result.types.indexOf(ADDRESS_TYPES.COLLOQUIAL_AREA) === -1 &&
          result.types.indexOf(ADDRESS_TYPES.COUNTRY) === -1
        );
      });
      this._cachedLocations[address] = listLocation;
    }
    this.setState({ listLocation: [...listLocation, ...userLocations] });
  };

  keyExtractor = (item) => item.place_id;

  renderItem = ({ item }) => (
    <TouchableOpacity
      key={item.place_id}
      style={styles.rowContainer}
      onPress={this._onPressItem(item)}
      activeOpacity={1}
    >
      <View style={styles.wrapRowItem}>
        <Image style={styles.locationIcon} source={Images.locationGrayIcon}/>
        <Text style={styles.detailLocationStyle}>
          {item.type && <Text style={styles.typeText}>{item.type}: </Text>}
          {item.formatted_address}
        </Text>
      </View>
      <View style={styles.breakLine} />
    </TouchableOpacity>
  );

  renderNoResult = () => {
    const text = this.state.address ? 'We cannot find the address you are typing. Please refine your search term or try another keyword.' : '';
    return (
      <View style={styles.noResultView}>
        <Text style={styles.noResultText}>{text}</Text>
      </View>
    );
  };

  _clear = () => {
    this.setState({ address: '' }, this.search);
  };

  _clearTitle = () => {
    this.setState({ title: '' });
  };

  _onChangeText = (value) => {
    this.setState({ address: value, }, this.searchDebounce);
  };

  _onChangeAddressTitle = (value) => {
    this.setState({ title: value, });
  };

  _goBack = () => {
    Keyboard.dismiss();
    const { navigation } = this.props;
    const { state: { params: { returnTitle } } } = navigation;
    if (returnTitle) {
      returnTitle(this.state.title);
    }
    navigation.goBack();
  };

  _renderInputTitle = () => {
    const { title } = this.state;
    return (
      <React.Fragment>
        <View style={styles.wrapSearchView}>
          <View style={styles.searchSection}>
            <TextInput
              style={styles.inputTitle}
              placeholder={'Address title'}
              placeholderTextColor={Colors.OLIVERY_GRAY3}
              underlineColorAndroid="transparent"
              autoCorrect={true}
              onChangeText={this._onChangeAddressTitle}
              value={title}
            />
            {
              title !== '' &&
              (
                <TouchableOpacity
                  style={styles.wrapClearIcon}
                  onPress={this._clearTitle}
                  hitSlop={clearButtonHitSlop}
                >
                  <Image style={styles.cancelFrame} source={Images.clearIcon}/>
                </TouchableOpacity>
              )
            }
            <Image source={Images.edit} style={styles.edit}/>
          </View>
        </View>
        <View style={styles.breakLineSearch} />
      </React.Fragment>
    );
  };

  render() {
    const { returnTitle } = this.props.navigation.state.params;
    const { address, listLocation } = this.state;
    return (
      <Background>
        <Header title='LOCATION' onTouchBackButton={this._goBack} />
        <View style={styles.container}>
          {returnTitle && this._renderInputTitle()}
          <View style={styles.wrapSearchView}>
            <View style={styles.searchSection}>
              <TextInput
                style={styles.input}
                placeholder={'Enter address'}
                placeholderTextColor={Colors.OLIVERY_GRAY3}
                underlineColorAndroid="transparent"
                autoCorrect={false}
                onChangeText={this._onChangeText}
                value={address}
                autoFocus={true}
              />
              {
                address !== '' &&
                (
                  <TouchableOpacity
                    style={styles.wrapClearIcon}
                    onPress={this._clear}
                    hitSlop={clearButtonHitSlop}
                  >
                    <Image style={styles.cancelFrame} source={Images.clearIcon}/>
                  </TouchableOpacity>
                )
              }
              <Image source={Images.edit} style={styles.edit}/>
            </View>

          </View>
          <View style={styles.breakLineSearch} />
          <FlatList
            keyboardShouldPersistTaps={'always'}
            style={styles.flatListStyle}
            keyExtractor={this.keyExtractor}
            data={listLocation}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderNoResult}
          />
        </View>

      </Background>
    );
  }
}


const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps)(SearchLocation);
