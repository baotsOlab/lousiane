import { StyleSheet } from 'react-native';
import { disPlayScale } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  greenView: {
    height: 110,
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  wrapSearchSection: {
    marginHorizontal: 10,
    marginTop: 40,
    marginBottom: 20,
    backgroundColor: Colors.WHITE,
    flex: 1,
    borderRadius: 60,
  },
  wrapSearchRow: {
    marginHorizontal: 20,
    marginTop: 17,
    marginBottom: 16,
    flex: 1,
    backgroundColor: Colors.WHITE,
    flexDirection: 'row'
  },
  searchIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain'
  },
  input: {
    flex: 1,
    fontFamily: Fonts.REGULAR,
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    padding: 0
  },
  filterIcon: {
    width: 19,
    height: 17,
    resizeMode: 'contain'
  },
  cellContainer: {
    aspectRatio: 1,
    width: '45.33333333%',
    marginLeft: 10 * disPlayScale,
    marginRight: 5 * disPlayScale,
    marginTop: 20,
  },
  menuImageContainer: {
    width: '100%',
    aspectRatio: 170 / 100,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  menuImage: {
    flex: 1,
    width: null,
    height: null
  },
  statusLikeContainer: {
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
  },
  openStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_GREEN3,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  closeStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_RED2,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  delayStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_ORANGE,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  statusText: {
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.WHITE,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
    marginHorizontal: 8,
  },
  likeContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  likeImage: {
    width: 17,
    height: 15,
    marginTop: 9,
    marginRight: 6
  },
  title: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 10,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  description: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 5,
    color: Colors.OLIVERY_GRAY3,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
  },
  chefImage: {
    marginTop: 40,
    marginLeft: 19,
    width: 132,
    height: 182,
    resizeMode: 'contain'
  },
  noResultContainer: {
    flex: 1,
    alignItems: 'center'
  },
  titleNoResult: {
    color: Colors.OLIVERY_BLACK1,
    fontSize: 24,
    fontFamily: Fonts.REGULAR,
    marginTop: 40
  },
  detailNoResult: {
    color: Colors.OLIVERY_GRAY3,
    fontSize: 16,
    fontFamily: Fonts.REGULAR,
    marginTop: 10
  },
  wrapNumber: {
    position: 'absolute',
    backgroundColor: Colors.OLIVERY_GREEN1,
    minWidth: 14,
    height: 14,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    top: 7,
    right: 22,
  },
  badgeNumber: {
    color: Colors.WHITE,
    fontSize: 9,
    fontFamily: Fonts.BOLD,
  }
});
