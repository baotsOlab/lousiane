import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import _ from 'lodash';
import Background from '../../components/Background';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import Colors from '../../common/Colors';
import CommonList from '../../components/CommonList';
import errorManager from '../../services/errorManager';

class SearchRestaurant extends Component {
  constructor(props) {
    super(props);
    this.state = { search: '' };
    this.searchDebounce = _.debounce(this._search, 400);
  }

  componentDidMount() {
    this._search();
  }

  _search = () => {
    if (this.commonList) {
      this.commonList.loadData();
    }
  };

  _loadData = async (page, take) => {
    const { search } = this.state;
    const result = await api.searchRestaurants({ search, page, take });
    return result.data;
  };

  _onChangeSearchText = (value) => {
    this.setState({ search: value }, this.searchDebounce);
  };

  _goTo = (routeName, params) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  _like = (data, index) => async () => {
    try {
      await api.like(data.id);
      this.updateLikeIcon(index);
    } catch (error) {
      errorManager.createError('Adding to favorites', error);
    }
  };

  updateLikeIcon = (index) => {
    const { list } = this.commonList.state;
    const tempRestaurant = [...list];
    tempRestaurant[index].like = !list[index].like;
    this.commonList.updateList(list);
  };

  _renderItem = ({ item, index }) => {
    const { status, name, address, like, image } = item;
    const imageSource = _.isEmpty(image) ? Images.food : { uri: image };
    return (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={this._goTo(Routes.HomeMenu, {
          restaurantId: item.id,
          onLike: () => this.updateLikeIcon(index)
        })}
      >
        <View style={styles.menuImageContainer}>
          <Image style={styles.menuImage} source={imageSource}/>
        </View>
        <View style={styles.statusLikeContainer}>
          <View style={styles.statusContainer}>
            <Text style={styles.statusText}>{status}</Text>
          </View>
          <TouchableOpacity style={styles.likeContainer} onPress={this._like(item, index)} >
            <Image style={styles.likeImage} source={like ? Images.likeWhite : Images.likeEmpty}/>
          </TouchableOpacity>
        </View>
        <Text style={styles.title} numberOfLines={1}>{ name }</Text>
        <Text style={styles.description} numberOfLines={2}>{address}</Text>
      </TouchableOpacity>
    );
  };

  _renderNoItems = () => (
    <View style={styles.noResultContainer}>
      <Image style={styles.chefImage} source={Images.chefImage} />
       <Text style={styles.titleNoResult}>No result found</Text>
       <Text style={styles.detailNoResult}>Sorry! We can't find what you're looking for</Text>
    </View>
  );

  _renderSearchField = () => {
    const { search } = this.state;
    return (
      <View style={styles.greenView}>
        <View style={styles.wrapSearchSection}>
          <View style={styles.wrapSearchRow}>
            <View style={{ flex: 1 }}>
              <Image style={styles.searchIcon} source={Images.searchIcon}/>
            </View>
            <View style={{ flex: 10 }}>
              <TextInput
                style={styles.input}
                placeholder='Search groceries'
                placeholderTextColor={Colors.OLIVERY_GRAY2}
                underlineColorAndroid={Colors.TRANSPARENT}
                autoCorrect={false}
                onChangeText={this._onChangeSearchText}
                value={search}
                autoFocus
                clearButtonMode
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <Background>
        {this._renderSearchField()}
        <CommonList
          ref={ref => { this.commonList = ref; }}
          loadData={this._loadData}
          renderItem={this._renderItem}
          numColumns={2}
          supportPullToRefresh={false}
          renderNoItems={this._renderNoItems}
        />
      </Background>
    );
  }
}

export default SearchRestaurant;
