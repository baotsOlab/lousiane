import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import _ from 'lodash';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from 'react-native-underline-tabbar';
import Header from '../../components/Header';
import Background from '../../components/Background';
import styles from './styles';
import WithLoading from '../../hoc/WithLoading';
import api from '../../services/api';
import { getString } from '../../utils/string';
import CommonList from '../../components/CommonList';
import { actions, QUANTITY_ACTION } from '../../store/reducers/cart';
import Colors from '../../common/Colors';
import utils from '../../utils';
import cartUtils from '../../utils/cartUtils';
import Routes from '../../navigation/routes';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../../services/subscriptionManager';

const actionHitSlop = { top: 18, right: 18, bottom: 18, left: 18 };

class HomeMenuDetail extends Component {
  lists = {};
  constructor(props) {
    super(props);
    const menuId = _.get(props.navigation.state.params, 'menuId', 0);
    const restaurantInfo = _.get(props.navigation.state.params, 'restaurantInfo', {});
    this.state = {
      menuId,
      restaurantInfo
    };
  }

  componentDidMount() {
    this._loadData(this.state.menuId, false);
    subscriptionManager.subscribe(SUBSCRIPTION_TYPE.DISHES_PRICE_CHANGES, this._onPricesChanges);
  }

  _loadData = (menuId, forceReload) => {
    const list = this.lists[menuId];
    if (list && list.ref && list.ref.loadData) {
      if (forceReload) {
        list.ref.loadData();
      } else {
        if (list.loaded) return;
        list.ref.loadData();
      }
      this.lists[menuId] = {
        ...this.lists[menuId],
        loaded: true
      };
    }
  };

  componentWillUnmount() {
    subscriptionManager.unsubscribe(SUBSCRIPTION_TYPE.DISHES_PRICE_CHANGES, this._onPricesChanges);
  }

  _onPricesChanges = (dishes = []) => {
    dishes.forEach(dish => {
      this._loadData(dish.menu_id, true);
    });
  };

  _addToCart = (item) => async () => {
    const { restaurantInfo } = this.state;
    const { addItemToCart, showLoading, restaurants, navigation } = this.props;
    const isValid = await cartUtils.isValidToAddToCart({
      restaurants,
      restaurantInfo,
      showLoading,
      navigation
    });
    if (!isValid) return;
    const { id, name, price } = item;
    addItemToCart({
      id,
      name,
      price,
      quantity: 1,
      restaurant: this.state.restaurantInfo.data
    });
  };

  _decreaseQuantityInCart = (item) => () => {
    const { updateQuantity } = this.props;
    updateQuantity(item.id, QUANTITY_ACTION.SUBTRACT);
  };

  _getQuantity = (id) => {
    const { itemsInCart } = this.props;
    const foundItem = itemsInCart.find(item => item.id === id);
    if (_.isNil(foundItem)) return 0;
    return foundItem.quantity;
  };

  _renderAddDeleteButtons = (item) => {
    const quantity = this._getQuantity(item.id);
    if (quantity > 0) {
      return (
        <View style={styles.addAndDeleteButtonContainer}>
          <TouchableOpacity hitSlop={actionHitSlop} onPress={this._decreaseQuantityInCart(item)}>
            <Text style={styles.actionMinusText}>-</Text>
          </TouchableOpacity>
          <Text style={styles.quantityText} pointerEvents={'none'}>{quantity}</Text>
          <TouchableOpacity hitSlop={actionHitSlop} onPress={this._addToCart(item)}>
            <Text style={styles.actionPlusText}>+</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <TouchableOpacity
        hitSlop={actionHitSlop}
        style={styles.addButton}
        onPress={this._addToCart(item)}
      >
        <Text style={styles.addTitle}>+</Text>
      </TouchableOpacity>
    );
  };

  _goTo = (routeName, params) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  _renderItem = ({ item }) => {
    const { restaurantInfo } = this.state;
    const image = getString(item, 'image');
    const name = getString(item, 'name');
    const description = getString(item, 'description');
    const price = getString(item, 'price');
    return (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={this._goTo(Routes.DishDetail, { item, restaurantInfo })}
      >
        <Image style={styles.dishImage} source={{ uri: image }} resizeMode={'contain'}/>
        <View style={styles.infoContainer}>
          <Text style={styles.dishTitle} numberOfLines={1}>{name}</Text>
          <Text style={styles.dishDescription} numberOfLines={2}>{description}</Text>
          <View style={styles.priceContainer}>
            <Text style={styles.price}>${utils.numberWithCommas(price)}</Text>
            {this._renderAddDeleteButtons(item)}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _onChangeTab = ({ i }) => {
    const { menu = [] } = this.state.restaurantInfo;
    this._loadData(menu[i].id, false);
  };

  render() {
    const { restaurantInfo, menuId } = this.state;
    const { data = {}, menu = [] } = restaurantInfo;
    const name = getString(data, 'name');
    const initialIndex = Math.max(0, menu.findIndex(item => item.id === menuId));
    return (
      <Background containerStyle={styles.container}>
        <Header onTouchBackButton={this._goBack} title={name}/>
        <ScrollableTabView
          initialPage={initialIndex}
          tabBarInactiveTextColor={Colors.OLIVERY_GRAY2}
          tabBarActiveTextColor={Colors.OLIVERY_BLACK1}
          onChangeTab={this._onChangeTab}
          style={{
            backgroundColor: Colors.TABBAR_BACKGROUND,
            shadowColor: Colors.OLIVERY_BLACK1,
            shadowOffset: { width: 0, height: 1.5 },
            shadowOpacity: 0.16,
            shadowRadius: 1.5,
            elevation: 1.5,
          }}
          renderTabBar={() => <TabBar
            underlineColor={Colors.OLIVERY_GREEN1}
            underlineHeight={3}
            tabMargin={0}
            tabStyles={{
              tab: {
                marginLeft: 0,
                paddingHorizontal: 8,
                paddingBottom: 13 // 10 + 3 (underlineHeight)
              }
            }}
            tabBarTextStyle={{
              fontSize: 13
            }}
            activeTabTextStyle={{
              fontSize: 13,
              fontWeight: '500'
            }}
          />}
        >
          {
            menu.map(item => {
              return (
                <CommonList
                  key={item.id.toString()}
                  ref={ref => {
                    if (this.lists[item.id]) {
                      this.lists[item.id].ref = ref;
                    } else {
                      this.lists[item.id] = { ref, loaded: false };
                    }
                  }}
                  tabLabel={{ label: item.name.toUpperCase() }}
                  renderItem={this._renderItem}
                  contentContainerStyle={styles.dishContentContainer}
                  loadData={() => api.getDishes(item.id)}
                  supportLoadMore={false}
                  supportPullToRefresh
                  style={{
                    marginTop: -6
                  }}
                />
              );
            })
          }

        </ScrollableTabView>
      </Background>
    );
  }
}

const mapStateToProps = (state) => ({
  restaurants: state.cart.restaurants,
  itemsInCart: state.cart.items
});
const mapDispatchToProps = {
  addItemToCart: actions.addCart,
  updateQuantity: actions.updateQuantity
};

const enhancer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  WithLoading
);
export default enhancer(HomeMenuDetail);
