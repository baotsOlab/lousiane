import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.OLIVERY_WHITE,
  },
  cellContainer: {
    flexDirection: 'row',
    marginHorizontal: 9,
    marginBottom: 9,
    backgroundColor: Colors.WHITE,
    padding: 8
  },
  dishImage: {
    // width: 129,
    width: 125,
    height: 94
  },
  infoContainer: {
    flex: 1,
    marginLeft: 12
  },
  dishTitle: {
    marginTop: 2,
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.OLIVERY_BLACK2,
    fontSize: 16,
    fontFamily: Fonts.REGULAR,
  },
  dishDescription: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 5,
    marginRight: 10,
    color: Colors.OLIVERY_GRAY3,
    fontSize: 11,
    fontFamily: Fonts.REGULAR,
  },
  priceContainer: {
    flex: 1,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  price: {
    flex: 1,
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 18,
    fontFamily: Fonts.MEDIUM,
    marginBottom: 4,
  },
  addButton: {
    height: 26,
    // backgroundColor: Colors.OLIVERY_GREEN1,
    backgroundColor: Colors.OLIVERY_YELLOW,
    borderWidth: 0.5,
    borderColor: Colors.BUTTON_BORDER,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 13,
  },
  addTitle: {
    backgroundColor: Colors.TRANSPARENT,
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 18,
    fontFamily: Fonts.BOLD,
    marginHorizontal: 8,
  },
  dishContentContainer: {
    paddingTop: 20,
  },
  addAndDeleteButtonContainer: {
    // backgroundColor: Colors.OLIVERY_GREEN1,
    backgroundColor: Colors.OLIVERY_YELLOW,
    height: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 13,
    borderWidth: 0.5,
    borderColor: Colors.BUTTON_BORDER
  },
  quantityText: {
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 14,
    marginHorizontal: 6,
    minWidth: 26,
    textAlign: 'center'
  },
  actionPlusText: {
    marginRight: 12,
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  actionMinusText: {
    marginLeft: 12,
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 18,
  }
});
