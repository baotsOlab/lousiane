import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';
import Background from '../../components/Background';
import LoginInput from '../../components/LoginInput';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import styles from './styles';
import Routes from '../../navigation/routes';
import WithLoading from '../../hoc/WithLoading';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import Colors from '../../common/Colors';

class EnterEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  _onBack = () => {
    this.props.navigation.goBack();
  };

  _sendCode = async () => {
    const { showLoading, navigation } = this.props;
    const { email } = this.state;
    showLoading(true);
    try {
      await api.sendCode({ email });
      navigation.navigate(Routes.EnterCode, { email });
    } catch (error) {
      errorManager.createError('Enter Email', error);
    } finally {
      showLoading(false);
    }
  };

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value });
  };

  render() {
    return (
      <Background>
        <Header
          onTouchBackButton={this._onBack}
          title='O.LIVERY'
        />
        <View style={styles.container}>
          <Text style={styles.title}>
            Enter your Email
          </Text>
          <Text style={styles.guide}>
            The new password will be sent to your email
          </Text>
          <LoginInput
            placeholderText='Your email'
            onChangeText={this._onChangeText('email')}
            value={this.state.email}
            style={styles.email}
            inputStyle={styles.emailInput}
            autoCapitalize='none'
            keyboardType='email-address'
            onSubmit={this._sendCode}
            returnKeyType='next'
          />
          <SolidButton
            text='Confirm'
            onPress={this._sendCode}
            fontSize={17}
            bold={true}
            textColor={Colors.OLIVERY_BLACK1}
            style={styles.goHomeButton}
          />
        </View>
      </Background>
    );
  }
}

export default WithLoading(EnterEmail);
