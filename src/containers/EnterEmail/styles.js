import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '78.6667%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 40
  },
  guide: {
    fontSize: 16,
    textAlign: 'center',
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 10
  },
  email: {
    marginTop: 52,
  },
  emailInput: {
    paddingLeft: 0,
  },
  goHomeButton: {
    height: 52,
    marginTop: 40
  },
});
