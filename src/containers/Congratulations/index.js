import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';

import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import styles from './styles';
import Colors from '../../common/Colors';

class Congratulations extends Component {
  _onClose = () => {
    this.props.screenProps.onLoginSuccess();
  };

  _onBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Background>
        <Header
          onTouchBackButton={this._onBack}
          title='O.LIVERY'
        />
        <View style={styles.container}>
          <Text style={styles.title}>
            CONGRATULATIONS
          </Text>
          <Text style={styles.guide}>
            You have successfully registered
            Olivery Application
          </Text>
          <SolidButton
            text='OK'
            onPress={this._onClose}
            fontSize={17}
            bold={true}
            textColor={Colors.OLIVERY_BLACK1}
            style={styles.goHomeButton}
          />
        </View>
      </Background>
    );
  }
}

export default Congratulations;
