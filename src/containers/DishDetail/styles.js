import { StyleSheet } from 'react-native';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  imageContainer: {
    padding: 10,
    height: 281,
  },
  mainImage: {
    width: '100%',
    height: '100%'
  },
  titleAndPriceContainer: {
    flexDirection: 'row',
    paddingHorizontal: 28,
    paddingTop: 17,
    paddingBottom: 27
  },
  title: {
    flex: 1,
    fontSize: 22,
    color: Colors.OLIVERY_BLACK1,
    paddingRight: 12
  },
  price: {
    fontSize: 26,
    color: Colors.OLIVERY_GREEN1,
    fontFamily: Fonts.BOLD
  },
  addToCartContainer: {
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: Colors.OLIVERY_GRAY3,
    alignItems: 'center'
  },
  addToCartButtonsContainer: {
    flexDirection: 'row',
    marginTop: 12,
    height: 41,
    width: '80%',
    // backgroundColor: Colors.OLIVERY_GREEN1,
    backgroundColor: Colors.OLIVERY_YELLOW,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    borderWidth: 0.5,
    borderColor: Colors.BUTTON_BORDER
  },
  addToCartText: {
    color: Colors.OLIVERY_GREEN1,
    fontSize: 18,
    marginVertical: 12,
    fontFamily: Fonts.MEDIUM
  },
  quantityText: {
    fontSize: 18,
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontFamily: Fonts.BOLD,
    marginHorizontal: 16
  },
  actionPlusText: {
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  actionMinusText: {
    // color: Colors.WHITE,
    color: Colors.BUTTON_TEXT_COLOR,
    fontSize: 18,
  },
  descriptionLabel: {
    color: Colors.OLIVERY_GRAY2,
    fontSize: 10,
    fontFamily: Fonts.REGULAR
  },
  descriptionText: {
    marginVertical: 6,
    color: Colors.OLIVERY_BLACK2,
    fontSize: 12,
    fontFamily: Fonts.REGULAR
  },
  descriptionContainer: {
    marginTop: 6,
    marginHorizontal: 28
  }
});
