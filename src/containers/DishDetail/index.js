import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import { compose } from 'recompose';
import _ from 'lodash';
import connect from 'react-redux/es/connect/connect';
import styles from './styles';
import { getString } from '../../utils/string';
import Header from '../../components/Header';
import utils from '../../utils';
import cartUtils from '../../utils/cartUtils';
import { actions, QUANTITY_ACTION } from '../../store/reducers/cart';
import WithLoading from '../../hoc/WithLoading';

const actionHitSlop = { top: 18, right: 18, bottom: 18, left: 18 };

class DishDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurantInfo: this.props.navigation.state.params.restaurantInfo
    };
  }

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _addToCart = (item) => async () => {
    const { restaurantInfo } = this.state;
    const { addItemToCart, restaurants, showLoading, navigation } = this.props;
    const isValid = await cartUtils.isValidToAddToCart({
      restaurants,
      restaurantInfo,
      showLoading,
      navigation
    });
    if (!isValid) return;
    const { id, name, price } = item;
    addItemToCart({
      id,
      name,
      price,
      quantity: 1,
      restaurant: this.state.restaurantInfo.data
    });
  };

  _decreaseQuantityInCart = (item) => () => {
    const { updateQuantity } = this.props;
    updateQuantity(item.id, QUANTITY_ACTION.SUBTRACT);
  };

  _getQuantity = (id) => {
    const { itemsInCart } = this.props;
    const foundItem = itemsInCart.find(item => item.id === id);
    if (_.isNil(foundItem)) return 0;
    return foundItem.quantity;
  };

  render() {
    const { item } = this.props.navigation.state.params;
    const image = getString(item, 'image');
    const name = getString(item, 'name');
    const description = getString(item, 'description');
    const price = getString(item, 'price');
    const quantity = this._getQuantity(item.id);
    return (
      <View style={styles.container}>
        <Header onTouchBackButton={this._goBack}/>
        <ScrollView>
          <View style={styles.imageContainer}>
            <Image source={{ uri: image }} style={styles.mainImage} resizeMode={'contain'} />
          </View>
          <View style={styles.titleAndPriceContainer}>
            <Text style={styles.title}>{name}</Text>
            <Text style={styles.price}>${utils.numberWithCommas(price)}</Text>
          </View>
          <View style={styles.addToCartContainer}>
            <View style={styles.addToCartButtonsContainer}>
              <TouchableOpacity
                hitSlop={actionHitSlop}
                onPress={this._decreaseQuantityInCart(item)}
              >
                <Text style={styles.actionMinusText}>-</Text>
              </TouchableOpacity>
              <Text style={styles.quantityText} pointerEvents={'none'}>{quantity}</Text>
              <TouchableOpacity hitSlop={actionHitSlop} onPress={this._addToCart(item)}>
                <Text style={styles.actionPlusText}>+</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.addToCartText}>ADD TO CART</Text>
          </View>
          {
            !_.isNil(description) && description.length > 0 && (
              <View style={styles.descriptionContainer}>
                <Text style={styles.descriptionLabel}>Description</Text>
                <Text style={styles.descriptionText}>
                  {description}
                </Text>
              </View>
            )
          }
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  restaurants: state.cart.restaurants,
  itemsInCart: state.cart.items
});
const mapDispatchToProps = {
  addItemToCart: actions.addCart,
  updateQuantity: actions.updateQuantity
};

const enhancer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  WithLoading
);
export default enhancer(DishDetail);
