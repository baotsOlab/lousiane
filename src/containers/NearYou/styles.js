import { StyleSheet } from 'react-native';
import { disPlayScale } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.OLIVERY_WHITE
  },

  cellContainer: {
    aspectRatio: 1,
    width: '45.33333333%',
    marginLeft: 10 * disPlayScale,
    marginRight: 5 * disPlayScale,
    marginTop: 20,
  },
  menuImageContainer: {
    width: '100%',
    aspectRatio: 170 / 100,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  menuImage: {
    flex: 1,
    width: null,
    height: null
  },
  statusLikeContainer: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  openStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_GREEN3,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  closeStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_RED2,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  delayStatusContainer: {
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.OLIVERY_ORANGE,
    borderRadius: 10,
    marginLeft: 6,
    marginTop: 6,
  },
  statusText: {
    backgroundColor: Colors.TRANSPARENT,
    color: Colors.WHITE,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
    marginHorizontal: 8,
  },
  likeContainer: {
    width: 44,
    height: 44,
    alignItems: 'flex-end'
  },
  likeImage: {
    width: 17,
    height: 15,
    marginTop: 9,
    marginRight: 6
  },
  title: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 10,
    color: Colors.OLIVERY_BLACK1,
    fontSize: 16,
    fontFamily: Fonts.MEDIUM,
  },
  description: {
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 5,
    color: Colors.OLIVERY_GRAY3,
    fontSize: 12,
    fontFamily: Fonts.REGULAR,
  },
});
