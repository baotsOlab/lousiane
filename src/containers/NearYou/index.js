import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import Header from '../../components/Header';
import Background from '../../components/Background';
import Images from '../../assets/Images';
import styles from './styles';
import api from '../../services/api';
import CommonList from '../../components/CommonList';
import Routes from '../../navigation/routes';
import errorManager from '../../services/errorManager';
import { RESTAURANT_STATUS } from '../../assets/constants/constant';

export default class NearYou extends Component {
  componentDidMount() {
    if (this.commonList) {
      this.commonList.loadData();
    }
  }

  _loadData = async (page, take) => {
    const { params } = this.props.navigation.state;
    const result = await api.getSuggestedRestaurants({
      id_restaurant: params.mainRestaurantId,
      page,
      take
    });
    return result.data;
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _goTo = (routeName, params) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  _like = (data, index) => async () => {
    try {
      await api.like(data.id);
      this.updateLikeIcon(index);
    } catch (error) {
      errorManager.createError('Adding to favorites', error);
    }
  };

  updateLikeIcon = (index) => {
    const { list } = this.commonList.state;
    const tempRestaurant = [...list];
    tempRestaurant[index].like = !list[index].like;
    this.commonList.updateList(list);
  };

  _getStyleByStatus = (status) => {
    switch (status) {
      case RESTAURANT_STATUS.OPEN:
        return styles.openStatusContainer;
      case RESTAURANT_STATUS.CLOSE:
        return styles.closeStatusContainer;
      default:
        return styles.delayStatusContainer;
    }
  };

  _renderItem = ({ item, index }) => {
    const { status, name, address, like, image } = item;
    const imageSource = _.isEmpty(image) ? Images.food : { uri: image };
    const statusContainer = this._getStyleByStatus(status);
    return (
      <TouchableOpacity
        style={styles.cellContainer}
        onPress={this._goTo(Routes.HomeMenu, {
          restaurantId: item.id,
          onLike: () => this.updateLikeIcon(index)
        })}
      >
        <View style={styles.menuImageContainer}>
          <Image style={styles.menuImage} source={imageSource}/>
        </View>
        <View style={styles.statusLikeContainer}>
          <View style={statusContainer}>
            <Text style={styles.statusText}>{status}</Text>
          </View>
          <TouchableOpacity style={styles.likeContainer} onPress={this._like(item, index)} >
            <Image style={styles.likeImage} source={like ? Images.likeWhite : Images.likeEmpty}/>
          </TouchableOpacity>
        </View>
        <Text style={styles.title} numberOfLines={1}>{ name }</Text>
        <Text style={styles.description} numberOfLines={2}>{address}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { restaurantName = '' } = this.props.navigation.state.params;
    return (
      <Background containerStyle={styles.container}>
        <Header onTouchBackButton={this._goBack} title={`Near ${restaurantName}`} />
        <CommonList
          ref={ref => { this.commonList = ref; }}
          loadData={this._loadData}
          renderItem={this._renderItem}
          numColumns={2}
        />
      </Background>
    );
  }
}
