import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';
import { scaleFont, scaleVertical } from '../../utils/size';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '78.6667%',
    alignSelf: 'center',
  },
  title: {
    fontSize: scaleFont(24),
    textAlign: 'center',
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: scaleVertical(40)
  },
  guide: {
    fontSize: scaleFont(16),
    textAlign: 'center',
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: scaleVertical(10)
  },
  codeInputContainer: {
    width: '85.07%',
    alignSelf: 'center',
    marginTop: scaleVertical(40),
  },
  confirmButton: {
    height: 52,
    marginTop: 40
  },
  resendLink: {
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  resendText: {
    fontSize: 13,
    textAlign: 'center',
    color: Colors.OLIVERY_BLACK1,
    textDecorationLine: 'underline',
    fontFamily: Fonts.REGULAR,
  },
});
