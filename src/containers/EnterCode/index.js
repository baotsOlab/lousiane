import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Platform,
  Keyboard,
  Alert
} from 'react-native';
import _ from 'lodash';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import CodeInput from '../../components/CodeInput';
import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import dataManager from '../../assets/constants/dataManager';
import styles from './styles';
import WithLoading from '../../hoc/WithLoading';
import errorManager from '../../services/errorManager';
import api from '../../services/api';
import Colors from '../../common/Colors';
import I18n from '../../i18n';
import navigationUtils from '../../utils/navigation';
import { actions as authActions } from '../../store/reducers/auth';

class EnterCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      phone: this.props.navigation.state.params.phone,
    };
  }

  _verifyCode = async () => {
    Keyboard.dismiss();
    const { code, phone } = this.state;
    const { showLoading, navigation, login } = this.props;
    showLoading(true);
    const params = {
      phone,
      verify_token: code,
      device_id: dataManager.deviceId,
      device_type: Platform.OS,
      app_target: 'customer'
    };
    try {
      const result = await api.verifySmsCode(params);
      const { token, user } = result;
      login(token, user);
      const { params: navigationParams } = navigation.state;
      if (_.has(navigationParams, 'resetToRoute')) {
        navigationUtils.reset({
          dispatch: navigation.dispatch,
          routes: [{ name: navigationParams.resetToRoute }]
        });
      } else {
        navigationParams.onVerified(phone);
        navigation.goBack();
      }
    } catch (error) {
      errorManager.createError('Checking your code', error);
    } finally {
      showLoading(false);
    }
  };

  _sendCode = async () => {
    Keyboard.dismiss();
    const { phone } = this.props.navigation.state.params;
    const { showLoading } = this.props;
    showLoading(true);
    const params = { phone, app_target: 'customer' };
    try {
      await api.sendSmsCode(params);
      Alert.alert('Send code', `We have sent a code to ${phone}, please check your phone!`);
    } catch (error) {
      errorManager.createError('Sending a code', error);
    } finally {
      showLoading(false);
    }
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value });
  };

  render() {
    return (
      <Background>
        <Header title='CONFIRMATION CODE' onTouchBackButton={this._goBack}/>
        <ScrollView keyboardShouldPersistTaps={'always'}>
          <KeyboardAvoidingView style={styles.container}>
            <Text style={styles.title}>Enter confirmation code</Text>
            <Text style={styles.guide}>A 4-digit code was sent to your mobile phone</Text>
            <CodeInput container={styles.codeInputContainer} onInputChange={this._onChangeText('code')}/>
            <SolidButton
              text={I18n.t('confirm')}
              onPress={this._verifyCode}
              fontSize={17}
              bold={true}
              textColor={Colors.OLIVERY_BLACK1}
              style={styles.confirmButton}
            />
            <TouchableOpacity style={styles.resendLink} onPress={this._sendCode}>
              <Text style={styles.resendText}>Resend confirmation code</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </Background>
    );
  }
}

const mapDispatchToProps = {
  login: authActions.login
};

const enhancer = compose(
  connect(null, mapDispatchToProps),
  WithLoading,
);
export default enhancer(EnterCode);
