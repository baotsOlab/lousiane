import React, { Component } from 'react';
import {
  Text,
  View,
  Keyboard,
} from 'react-native';
import { withFormik } from 'formik';
import { compose } from 'recompose';
import Background from '../../components/Background';
import LoginInput from '../../components/LoginInput';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import styles from './styles';
import Routes from '../../navigation/routes';
import WithLoading from '../../hoc/WithLoading';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import Colors from '../../common/Colors';
import I18n from '../../i18n';
import { getNumberOnly } from '../../utils/string';

class SubmitPhone extends Component {
  state = { phone: '+' };
  _onChangeText = (key) => (value) => {
    const transformedValue = `+${getNumberOnly(value)}`;
    this.props.setFieldValue(key, transformedValue);
  };

  _onSubmit = async () => {
    Keyboard.dismiss();
    const { showLoading, navigation, setSubmitting, values } = this.props;
    const { phone } = values;
    showLoading(true);
    setSubmitting(true);
    try {
      await api.sendSmsCode({
        phone,
        app_target: 'customer'
      });
      navigation.navigate(Routes.EnterCode, { phone, resetToRoute: Routes.LoginRequired });
    } catch (error) {
      errorManager.createError('Submitting phone number', error);
    } finally {
      showLoading(false);
      setSubmitting(false);
    }
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { values: { phone }, isValid, isSubmitting } = this.props;
    return (
      <Background>
        <Header title={I18n.t('loginNow')} onTouchBackButton={this._goBack} />
        <View style={styles.container}>
          <Text style={styles.title}>Submit your phone number</Text>
          <LoginInput
            placeholderText='Phone number'
            onChangeText={this._onChangeText('phone')}
            value={phone}
            style={styles.phone}
            inputStyle={styles.phoneInput}
            autoCapitalize='none'
            keyboardType='phone-pad'
            onSubmit={this._onSubmit}
            returnKeyType='done'
            autoFocus
          />
          <SolidButton
            disabled={isSubmitting || !isValid}
            text='Submit'
            onPress={this._onSubmit}
            fontSize={17}
            bold={true}
            textColor={Colors.OLIVERY_BLACK1}
            style={styles.submitButton}
          />
        </View>
      </Background>
    );
  }
}

const withFormikConfig = {
  mapPropsToValues: () => {
    return { phone: '+' };
  },
  validate: (values) => {
    const errors = {};
    if (values.phone && values.phone.length <= 10) {
      errors.phone = 'Phone number must have minimum length = 10';
    }
    return errors;
  }
};

const enhancer = compose(
  withFormik(withFormikConfig),
  WithLoading
);
export default enhancer(SubmitPhone);
