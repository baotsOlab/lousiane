import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import _ from 'lodash';
import Background from '../../components/Background';
import Header from '../../components/Header';
import CancelOrder from '../../components/CancelOrder';
import Images from '../../assets/Images';
import styles from './styles';
import { ORDER_PHASES, ORDER_STATUS, NOTIFICATION_TYPE } from '../../assets/constants/constant';
import api from '../../services/api';
import WithLoading from '../../hoc/WithLoading';
import errorManager from '../../services/errorManager';
import Routes from '../../navigation/routes';
import navigationUtils from '../../utils/navigation';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../../services/subscriptionManager';
import Loading from '../../components/Loading';
import AppStateHandler from '../../components/AppStateHandler';

class OrderStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showCancel: false,
      allowToCancel: true,
      orderStatusProgress: [
        {
          id: ORDER_PHASES.SUBMITTED,
          status: ORDER_STATUS.TODO,
          title: 'Order submitted',
          titleStyle: { marginTop: 51 },
          image: Images.statusSubmit,
          imageStyle: { width: 28, height: 40, marginTop: 40 }
        },
        {
          id: ORDER_PHASES.CONFIRMATION,
          status: ORDER_STATUS.TODO,
          title: 'Order confirmation',
          titleStyle: { marginTop: 55 },
          image: Images.statusConfirmation,
          imageStyle: { width: 28, height: 38, marginTop: 35 }
        },
        {
          id: ORDER_PHASES.ASSIGNED,
          status: ORDER_STATUS.TODO,
          title: 'Order assigned to Partner',
          titleStyle: { marginTop: 56 },
          image: Images.statusAssign,
          imageStyle: { width: 37, height: 33, marginTop: 40 }
        },
        {
          id: ORDER_PHASES.PICKUP,
          status: ORDER_STATUS.TODO,
          title: 'Order picked up by Driver',
          titleStyle: { marginTop: 58 },
          image: Images.statusPickup,
          imageStyle: { width: 32, height: 40, marginTop: 40 }
        },
        {
          id: ORDER_PHASES.DELIVERED,
          status: ORDER_STATUS.TODO,
          title: 'Order delivered to Customer',
          titleStyle: { marginTop: 54 },
          image: Images.statusDelivered,
          imageStyle: { width: 56, height: 28, marginTop: 40 }
        },
      ]
    };
  }

  componentDidMount() {
    this._loadOrderStatus(true)();
    subscriptionManager.subscribe(SUBSCRIPTION_TYPE.NOTIFICATION, this._onNotification);
  }

  componentWillUnmount() {
    subscriptionManager.unsubscribe(SUBSCRIPTION_TYPE.NOTIFICATION, this._onNotification);
  }

  _loadOrderStatus = (loadingVisible) => async () => {
    const { showLoading, navigation } = this.props;
    const { id } = navigation.state.params;
    showLoading(loadingVisible, { showIndicator: false });
    try {
      const result = await api.getOrderDetail(id);
      const currentOrderPhase = _.get(result, 'cart.status', ORDER_PHASES.CONFIRMATION);
      this._setOrderStatusProgress(currentOrderPhase);
    } catch (error) {
      errorManager.createError('Loading order status', error);
    } finally {
      showLoading(false);
    }
  };

  _onNotification = (notificationData) => {
    const { navigation } = this.props;
    const payload = JSON.parse(notificationData.data);
    const { cart_id: cartId, status, type } = payload;
    if (type !== NOTIFICATION_TYPE.ORDER_STATUS) return;
    const { id } = navigation.state.params;
    if (id !== cartId) return;
    this._setOrderStatusProgress(status);
  };

  _setOrderStatusProgress = (status) => {
    const { orderStatusProgress } = this.state;
    let found = false;
    const newOrderStatusProgress = orderStatusProgress.map(orderStatus => {
      let progressStatus = ORDER_STATUS.DONE;
      if (orderStatus.id === status) {
        found = true;
        progressStatus = ORDER_STATUS.IN_PROGRESS;
      } else if (found) {
        progressStatus = ORDER_STATUS.TODO;
      }
      return {
        ...orderStatus,
        status: progressStatus
      };
    });
    this.setState({
      orderStatusProgress: newOrderStatusProgress,
      allowToCancel: status === ORDER_PHASES.CONFIRMATION
    });
  };

  _showModal = (visible) => () => {
    this.setState({ showModal: visible, showCancel: false });
  };

  _renderModal = () => {
    const { showModal } = this.state;
    return (
      <Modal
        animationType={'fade'}
        transparent={true}
        visible={showModal}
        supportedOrientations={['portrait']}
      >
        <CancelOrder
          onPressOk={this._onCancel}
          onPressCancel={this._showModal(false)}
        />
      </Modal>
    );
  };

  _onCancel = async () => {
    this._showModal(false)();
    const { showLoading, navigation } = this.props;
    const { id, shouldGoBack } = navigation.state.params;
    showLoading(true);
    try {
      await api.cancelOrder({ id });
      if (shouldGoBack) {
        this._goBack();
      } else {
        this._navigateTo(Routes.Cart);
      }
    } catch (error) {
      errorManager.createError('Cancelling an order', error);
    } finally {
      showLoading(false);
    }
  };

  _navigateTo = (routeName) => {
    navigationUtils.reset({
      dispatch: this.props.navigation.dispatch,
      routes: [{ name: routeName }]
    });
  };

  _renderCancelButton = () => {
    return (
      <TouchableOpacity style={styles.cancelContainer} onPress={this._showModal(true)}>
        <View style={styles.triangleShape}/>
        <View style={styles.cancelButton}>
          <Text style={styles.cancelText}>Cancel order</Text>
        </View>
      </TouchableOpacity>
    );
  };

  _showCancelButton = () => {
    this.setState({ showCancel: true });
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _renderDotMenu = () => {
    return (
      <TouchableOpacity style={styles.editButton} onPress={this._showCancelButton}>
        <View style={styles.tinyDot}/>
        <View style={styles.tinyDot}/>
        <View style={styles.tinyDot}/>
      </TouchableOpacity>
    );
  };

  _renderTimelineStatus = () => {
    const { orderStatusProgress } = this.state;
    return (
      <View style={styles.dotLineContainer}>
        {
          orderStatusProgress.map((orderPhase, index) => {
            const lastItem = orderStatusProgress.length === index + 1;
            const { id, status } = orderPhase;
            let imageSource = null;
            let lineStyle = styles.lineGray;
            switch (status) {
              case ORDER_STATUS.TODO: {
                imageSource = Images.dotGray;
                break;
              }
              case ORDER_STATUS.IN_PROGRESS: {
                imageSource = Images.dotGreen;
                break;
              }
              case ORDER_STATUS.DONE: {
                imageSource = Images.dotGreenCheck;
                lineStyle = styles.lineGreen;
                break;
              }
              default: return null;
            }
            return (
              <React.Fragment key={id}>
                <Image style={styles.dotImage} source={imageSource}/>
                { !lastItem && <View style={lineStyle}/> }
              </React.Fragment>
            );
          })
        }
      </View>
    );
  };

  _renderIcons = () => {
    const { orderStatusProgress } = this.state;
    return (
      <View style={styles.statusImageContainer}>
        {
          orderStatusProgress.map(orderPhase => {
            const { id, status, image, imageStyle } = orderPhase;
            const imageStyles = [
              imageStyle,
              status === ORDER_STATUS.TODO && styles.statusImageOpaque,
              (
                status === ORDER_STATUS.IN_PROGRESS ||
                status === ORDER_STATUS.DONE
              ) && styles.statusImage,
            ];
            return (<Image key={id} style={imageStyles} source={image}/>);
          })
        }
      </View>
    );
  };

  _renderTitles = () => {
    const { orderStatusProgress, allowToCancel } = this.state;
    return (
      <View style={styles.statusTextContainer}>
        {
          orderStatusProgress.map(orderPhase => {
            const { id, status, title, titleStyle } = orderPhase;
            let textStyle = null;
            switch (status) {
              case ORDER_STATUS.TODO:
                textStyle = styles.statusTextGray;
                break;
              case ORDER_STATUS.IN_PROGRESS:
                textStyle = styles.statusTextGreen;
                break;
              case ORDER_STATUS.DONE:
                textStyle = styles.statusTextBlack;
                break;
              default: break;
            }
            if (id === ORDER_PHASES.CONFIRMATION) {
              const { showCancel } = this.state;
              return (
                <View style={titleStyle}>
                  <Text style={textStyle}>
                    { title }
                  </Text>
                  {allowToCancel && this._renderDotMenu()}
                  {showCancel && this._renderCancelButton()}
                </View>
              );
            }

            return (
              <Text key={id} style={[textStyle, titleStyle]}>
                { title }
              </Text>
            );
          })
        }
      </View>
    );
  };

  _renderContent = () => {
    return (
      <React.Fragment>
        <ScrollView style={styles.scrollView} alwaysBounceVertical={false}>
          <View style={styles.container}>
            {this._renderTimelineStatus()}
            {this._renderIcons()}
            {this._renderTitles()}
          </View>
        </ScrollView>
        {this._renderModal()}
      </React.Fragment>
    );
  };

  render() {
    const { loading } = this.props;
    return (
      <Background>
        <Header onTouchBackButton={this._goBack} title='ORDER STATUS'/>
        <AppStateHandler onGoToForeground={this._loadOrderStatus(false)} />
        {loading && <Loading/>}
        {!loading && this._renderContent()}
      </Background>
    );
  }
}

export default WithLoading(OrderStatus);
