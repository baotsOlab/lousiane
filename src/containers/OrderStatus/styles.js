import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  container: {
    flexDirection: 'row',
  },
  dotLineContainer: {
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 50,
  },
  dotImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  lineGreen: {
    height: 56,
    width: 1,
    backgroundColor: Colors.OLIVERY_GREEN1
  },
  lineGray: {
    height: 56,
    width: 1,
    backgroundColor: Colors.OLIVERY_GRAY1
  },
  statusImageContainer: {
    alignItems: 'center',
    marginLeft: 15,
  },
  statusImage: {
    resizeMode: 'contain',
  },
  statusImageOpaque: {
    resizeMode: 'contain',
    opacity: 0.4,
  },
  statusTextContainer: {
    marginLeft: 16,
    flex: 1,
  },
  statusTextBlack: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
  },
  statusTextGreen: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_GREEN1,
  },
  statusTextGray: {
    fontFamily: Fonts.REGULAR,
    fontSize: 16,
    color: Colors.OLIVERY_GRAY1,
  },
  editButton: {
    width: 60,
    height: 20,
    position: 'absolute',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  tinyDot: {
    width: 4,
    height: 4,
    borderRadius: 2,
    backgroundColor: Colors.OLIVERY_GRAY2,
    marginHorizontal: 2,
  },

  cancelContainer: {
    marginTop: 20,
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  cancelButton: {
    width: 88,
    height: 35,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.OLIVERY_WHITE
  },
  triangleShape: {
    marginLeft: 58,
    width: 0,
    height: 0,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 10,
    borderStyle: 'solid',
    backgroundColor: Colors.TRANSPARENT,
    borderLeftColor: Colors.TRANSPARENT,
    borderRightColor: Colors.TRANSPARENT,
    borderBottomColor: Colors.OLIVERY_WHITE
  },
  cancelText: {
    fontFamily: Fonts.REGULAR,
    fontSize: 12,
    color: Colors.OLIVERY_BLACK1,
    textAlign: 'center'
  },
});
