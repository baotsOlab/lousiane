import React, { Component } from 'react';
import {
  WebView,
} from 'react-native';

import Background from '../../components/Background';
import Header from '../../components/Header';
import styles from './styles';

const PolicyHTML = require('../../assets/html/policy.html');

export default class Policy extends Component {
  render() {
    return (
      <Background>
        <Header
          onTouchBackButton={() =>
            this.props.navigation.goBack()
          }
          title='O.LIVERY'
        />
        <WebView
          source={PolicyHTML}
          style={styles.webView}>
        </WebView>
      </Background>
    );
  }
}