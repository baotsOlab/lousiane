import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '78.6667%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    // color: Colors.OLIVERY_BLACK1,
    color: Colors.TEXT_COLOR,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 5
  },
  guide: {
    fontSize: 16,
    textAlign: 'center',
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR,
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 7
  },
  link: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  confirmButton: {
    height: 52,
    marginTop: 40,
    width: '78.6667%',
    alignSelf: 'center'
  },
  createAccountContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 30,
  },
  dontHaveAccount: {
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    fontFamily: Fonts.REGULAR,
    marginRight: 4
  },
  createAccount: {
    fontSize: 14,
    color: Colors.OLIVERY_BLACK1,
    textDecorationLine: 'underline',
    fontFamily: Fonts.REGULAR,
  },
  chefImage: {
    width: 111,
    height: 144,
  },
  wrapChefImage: {
    alignItems: 'center',
    marginTop: 70
  }
});
