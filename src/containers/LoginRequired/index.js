import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView
} from 'react-native';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import I18n from '../../i18n';
import Colors from '../../common/Colors';
import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import navigationUtils from '../../utils/navigation';

/* eslint-disable react/no-deprecated */
class LoginRequired extends Component {
  componentDidMount() {
    this._resetRoute(this.props.loggedIn);
  }

  componentWillReceiveProps(nextProps) {
    this._resetRoute(nextProps.loggedIn);
  }

  _resetRoute = (loggedIn) => {
    if (loggedIn) {
      navigationUtils.reset({
        dispatch: this.props.navigation.dispatch,
        routes: [{ name: Routes.ProfileOption }]
      });
    }
  };

  _goTo = (routeName, params = {}) => () => {
    this.props.navigation.navigate(routeName, params);
  };

  render() {
    return (
      <Background>
        <Header title={I18n.t('profile')} hideBackButton={true}/>
        <ScrollView>
          <View style={styles.wrapChefImage}>
            <Image style={styles.chefImage} source={Images.new_chef}/>
          </View>
          <View>
            <Text style={styles.title}>{I18n.t('loginRequired')}</Text>
            <Text style={[styles.guide, { paddingBottom: 110 }]}>{I18n.t('loginRequiredDescription')}</Text>
            <SolidButton
              text={I18n.t('loginNow')}
              onPress={this._goTo(Routes.SubmitPhone)}
              fontSize={ 17 }
              bold={true}
              textColor={Colors.OLIVERY_GREEN1}
              style={styles.confirmButton}
            />

          </View>
        </ScrollView>
      </Background>
    );
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.auth.loggedIn
});

const enhancer = compose(connect(mapStateToProps));
export default enhancer(LoginRequired);
