import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Background from '../../components/Background';
import LoginInput from '../../components/LoginInput';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import styles from './styles';
import Routes from '../../navigation/routes';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import WithLoading from '../../hoc/WithLoading';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      email: '',
      password: '',
      phone: ''
    };
  }

  goToPolicy() {
    this.props.navigation.navigate(Routes.Policy);
  }

  _callApiSignUp = async () => {
    const { navigation, showLoading } = this.props;
    const { userName, email, password, phone } = this.state;
    showLoading(true);
    try {
      await api.signUp({
        name: userName,
        email,
        password,
        phone
      });
      navigation.navigate(Routes.EnterCode, { phone });
    } catch (error) {
      errorManager.createError('Sign Up', error);
    } finally {
      showLoading(false);
    }
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value });
  };

  _renderTermAndCondition = () => {
    return (
      <View style={styles.termContainer}>
        <View style={styles.privacyContainer}>
          <Text style={styles.termText}>
            {'By signing up, I agree to '}
          </Text>

          <TouchableOpacity
            onPress={() => this.goToPolicy()}
          >
            <Text style={styles.termLink}>
              Privacy Policy
            </Text>
          </TouchableOpacity>
          <Text style={styles.termText}>
            {' and '}
          </Text>
        </View>
        <TouchableOpacity>
          <Text style={styles.termLink}>
            "APP"s Privacy Policy
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <Background>
        <Header
          onTouchBackButton={this._goBack}
          title='SIGN UP'
        />
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
          alwaysBounceVertical={false}
        >
          <KeyboardAvoidingView style={styles.container}>
            <LoginInput
              icon='user'
              iconStyle={{ width: 16, height: 16 }}
              placeholderText='User name'
              onChangeText={this._onChangeText('userName')}
              value={this.state.userName}
              style={styles.userName}
              autoCapitalize='none'
              onSubmit={() => this.emailTextInput.focus()}
              returnKeyType='next'
            />
            <LoginInput
              icon='email'
              iconStyle={{ width: 16, height: 12 }}
              onRef={ref => { this.emailTextInput = ref; }}
              placeholderText='Email'
              onChangeText={this._onChangeText('email')}
              value={this.state.email}
              style={styles.email}
              autoCapitalize='none'
              keyboardType='email-address'
              onSubmit={() => this.passwordTextInput.focus()}
              returnKeyType='next'
            />
            <LoginInput
              icon='password'
              iconStyle={{ width: 14, height: 17, marginLeft: 1 }}
              onRef={ref => { this.passwordTextInput = ref; }}
              placeholderText='Password'
              onChangeText={this._onChangeText('password')}
              value={this.state.password}
              style={styles.password}
              autoCapitalize='none'
              secureTextEntry={true}
              onSubmit={() => this.phoneTextInput.focus()}
              returnKeyType='next'
            />
            <LoginInput
              icon='phone'
              iconStyle={{ width: 15, height: 16 }}
              onRef={ref => { this.phoneTextInput = ref; }}
              placeholderText='Phone number'
              onChangeText={this._onChangeText('phone')}
              value={this.state.phone}
              style={styles.phone}
              autoCapitalize='none'
              keyboardType='phone-pad'
              onSubmit={this._callApiSignUp}
              returnKeyType='done'
            />
            <SolidButton
              text='CREATE ACCOUNT'
              onPress={this._callApiSignUp}
              style={styles.signUpButton}
            />
            {this._renderTermAndCondition()}
          </KeyboardAvoidingView>
        </ScrollView>
      </Background>
    );
  }
}

export default WithLoading(SignUp);
