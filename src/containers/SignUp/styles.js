import { StyleSheet } from 'react-native';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  container: {
    flex: 1,
    width: '78.6667%',
    alignSelf: 'center',
  },
  contentContainer: {
    flex: 1,
    width: '78.6667%',
    alignSelf: 'center',
  },
  userName: {
    marginTop: 32,
  },
  email: {
    marginTop: 22,
  },
  password: {
    marginTop: 22,
  },
  phone: {
    marginTop: 22,
  },
  signUpButton: {
    marginTop: 40,
  },
  termContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.TRANSPARENT,
    marginTop: 30,
  },
  privacyContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  termText: {
    fontSize: 13,
    color: Colors.OLIVERY_GRAY3,
    fontFamily: Fonts.REGULAR,
  },
  termLink: {
    fontSize: 13,
    textAlign: 'center',
    color: Colors.OLIVERY_BLACK1,
    textDecorationLine: 'underline',
    fontFamily: Fonts.REGULAR,
  },
});
