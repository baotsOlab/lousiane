import { StyleSheet } from 'react-native';
import { deviceHeight, deviceWidth } from '../../assets/constants/constant';
import Fonts from '../../common/Fonts';
import Colors from '../../common/Colors';

export default StyleSheet.create({
  field: {
    width: '100%',
    color: '#449aeb',
    borderColor: Colors.OLIVERY_GREEN1,
    borderWidth: 1,
    borderRadius: 5,
  },
  paymentCardContainer: {
    alignItems: 'center',
    margin: 24,
  },
  scrollView: {
    flex: 1,
  },
  container: {
    height: deviceHeight - 60 - 66,
    width: deviceWidth - 20,
    alignSelf: 'center',
  },
  paymentStripe: {
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    color: Colors.OLIVERY_BLACK1,
    marginTop: 20,
  },
  threeCardContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  cardImageContainer: {
    width: '30.5%',
    aspectRatio: 108 / 50,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  cardImage: {
    flex: 1,
    width: null,
    height: null
  },
  applyButtonContainer: {
    flex: 1,
    paddingHorizontal: 24
  },
});
