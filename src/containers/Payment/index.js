import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Alert
} from 'react-native';
import stripe, { PaymentCardTextField } from 'tipsi-stripe';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import Background from '../../components/Background';
import SolidButton from '../../components/SolidButton';
import Header from '../../components/Header';
import Images from '../../assets/Images';
import styles from './styles';
import Routes from '../../navigation/routes';
import Colors from '../../common/Colors';
import I18n from '../../i18n';
import env from '../../env';
import api from '../../services/api';
import errorManager from '../../services/errorManager';
import { actions } from '../../store/reducers/cart';
import WithLoading from '../../hoc/WithLoading';
import navigationUtils from '../../utils/navigation';
import { ERROR_CODE } from '../../assets/constants/constant';

stripe.setOptions({
  publishableKey: env.stripe.publishableKey,
  merchantId: 'MERCHANT_ID',
  androidPayMode: 'test',
});

class Payment extends Component {
  state = { isValid: false, params: {} };
  componentDidMount() {
    this.props.screenProps.tabBar.show(false);
  }

  _goBack = () => {
    this.props.screenProps.tabBar.show(true);
    this.props.navigation.goBack();
  };

  _onParamsChange = (isValid, params) => {
    this.setState({ isValid, params });
  };

  _checkout = async () => {
    const { showLoading, clearCart } = this.props;
    const { isValid, params } = this.state;
    if (!isValid) return;
    // const { livemode, created, card, tokenId } = token;
    // const { funding, cardId, country, isApplePayCard, expMonth, brand, last4, expYear } = card;
    const { checkoutParams } = this.props.navigation.state.params;
    showLoading(true);
    try {
      const token = await this._createToken(params);
      const { tokenId } = token;
      const result = await api.checkout({ ...checkoutParams, card_token_id: tokenId });
      const { id } = result;
      clearCart();
      this._resetRouteAndGoToPaymentSuccess(id);
    } catch (error) {
      const errorTitle = 'Checking out with credit card';
      if (error.code === ERROR_CODE.PRICE_CHANGE) {
        Alert.alert(
          errorTitle,
          error.message,
          [
            {
              text: 'OK',
              onPress: this._refreshPriceChanges
            }
          ],
          { cancelable: false }
        );
      } else {
        errorManager.createError(errorTitle, error);
      }
    } finally {
      showLoading(false);
    }
  };

  _refreshPriceChanges = () => {
    const { navigation } = this.props;
    navigation.state.params.onRefreshPriceChanges();
    navigation.goBack();
  };

  _createToken = async (params) => {
    try {
      return await stripe.createTokenWithCard(params);
    } catch (error) {
      const { message = '' } = error;
      if (message.toLowerCase().indexOf('ioexception') > -1) {
        throw new Error('Network request failed, please check your connection!');
      }
      throw error;
    }
  };

  _resetRouteAndGoToPaymentSuccess = (id) => {
    navigationUtils.reset({
      dispatch: this.props.navigation.dispatch,
      index: 1,
      routes: [
        { name: Routes.Cart },
        { name: Routes.PaymentSuccess, params: { id } }
      ]
    });
  };

  _renderCardImage = (imageSource) => {
    return (
      <View style={styles.cardImageContainer}>
        <Image style={styles.cardImage} source={imageSource}/>
      </View>
    );
  };

  render() {
    const { isValid } = this.state;
    return (
      <Background>
        <Header onTouchBackButton={this._goBack} title={I18n.t('payment').toUpperCase()}/>
        <KeyboardAwareScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <Text style={styles.paymentStripe}>Enter your credit card information</Text>
            <View style={styles.threeCardContainer}>
              {this._renderCardImage(Images.visa)}
              {this._renderCardImage(Images.master)}
              {this._renderCardImage(Images.discover)}
            </View>
            <View style={styles.paymentCardContainer}>
              <PaymentCardTextField style={styles.field} onParamsChange={this._onParamsChange}/>
            </View>
            <View style={styles.applyButtonContainer}>
              <SolidButton
                text={I18n.t('apply')}
                onPress={this._checkout}
                fontSize={18}
                bold
                textColor={Colors.WHITE}
                disabled={!isValid}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Background>
    );
  }

  callApiPayment() {
    this.props.navigation.navigate(Routes.PaymentSuccess);
  }
}

const mapDispatchToProps = {
  clearCart: actions.clearCart
};

const enhancer = compose(
  connect(null, mapDispatchToProps),
  WithLoading
);

export default enhancer(Payment);
