import { Platform } from 'react-native';
import _ from 'lodash';
import dataManager from '../assets/constants/dataManager';
import env from '../env';
import { store } from '../store';
import { NetworkError, ParseJsonError, ServerError } from '../common/CustomErrors';

export const STATUS_CODE = {
  OK: 200,
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
  NO_CONTENT: 204
};

const fetchData = async (url, params, customHeaders) => {
  const token = _.get(store.getState(), 'auth.token');
  const headers = {
    Host: env.hostHeader,
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: token && `Bearer ${token}`,
    token,
    'device-id': dataManager.deviceId,
    'device-type': Platform.OS,
    ...customHeaders
  };
  let response;
  try {
    response = await fetch(url, { ...params, headers });
  } catch (error) {
    throw new NetworkError(error.message);
  }
  if (response.status === STATUS_CODE.NO_CONTENT) return {}; // for DELETE method case
  const text = await response.text();
  /* eslint-disable no-console */
  console.log('--START--');
  console.log({ url, headers, params });
  console.log({ response, text });
  let json = {};
  try {
    json = JSON.parse(text);
    console.log({ json });
  } catch (error) {
    throw new ParseJsonError();
  }
  if (!response.ok) {
    if (json && json.message) {
      throw new ServerError(json.message);
    }
    throw new ServerError('Unknown error');
  }
  const { status_code: statusCode, result, error_code: errorCode } = json;
  if (statusCode !== STATUS_CODE.OK) {
    const error = new Error(result);
    error.code = errorCode;
    throw error;
  }
  return result;
};

const get = async (endpoint, params = {}, headers = {}) => {
  let queryString = Object.keys(params)
    .map(key => `${key}=${params[key]}`)
    .join('&');
  if (queryString.length > 0) {
    queryString = `?${queryString}`;
  }
  const url = `${env.baseUrl}${endpoint}${queryString}`;
  const fetchParams = {
    method: 'GET'
  };
  return fetchData(url, fetchParams, headers);
};

const post = async (endpoint, params = {}, headers = {}, shouldStringify = true) => {
  const url = `${env.baseUrl}${endpoint}`;
  const fetchParams = {
    method: 'POST',
    body: shouldStringify ? JSON.stringify(params) : params
  };
  return fetchData(url, fetchParams, headers);
};

const patch = async (endpoint, params = {}, headers = {}) => {
  const url = `${env.baseUrl}${endpoint}`;
  const fetchParams = {
    method: 'PATCH',
    body: JSON.stringify(params)
  };
  return fetchData(url, fetchParams, headers);
};

const remove = async (endpoint, headers = {}) => {
  const url = `${env.baseUrl}${endpoint}`;
  const fetchParams = {
    method: 'DELETE'
  };
  return fetchData(url, fetchParams, headers);
};

export { get, post, patch, remove };
