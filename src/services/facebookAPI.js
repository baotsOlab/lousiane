import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from 'react-native-fbsdk';

/**
 *
 * @returns {Promise<{{ id, name, accessToken }}>}
 */
const getUserInfo = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'user_friends', 'email', 'user_birthday']);
      if (result.isCancelled) {
        reject(new Error('User cancelled!'));
      } else {
        const fbAccessToken = await
        AccessToken.getCurrentAccessToken();
        const infoRequest = new GraphRequest(
          '/me?fields=name,picture,email,gender,link,birthday',
          null,
          (error, graphResult) => {
            if (error) {
              reject(error);
            } else {
              resolve({
                id: graphResult.id,
                name: graphResult.name,
                accessToken: fbAccessToken.accessToken
              });
            }
          }
        );
        // Start the graph request.
        new GraphRequestManager().addRequest(infoRequest).start();
      }
    } catch (error) {
      reject(error);
    }
  });
};

export default { getUserInfo };