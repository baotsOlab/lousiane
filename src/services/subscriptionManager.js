export const SUBSCRIPTION_TYPE = {
  NOTIFICATION: 'NOTIFICATION',
  NOTIFICATION_OPENED: 'NOTIFICATION_OPENED',
  DISHES_PRICE_CHANGES: 'DISHES_PRICE_CHANGES'
};

class SubscriptionManager {
  subscription = {};
  _getSubscribers = (key) => {
    return this.subscription[key] || [];
  };

  subscribe = (key, subscriber) => {
    const subscribers = this._getSubscribers(key);
    if (subscribers.indexOf(subscriber) === -1) {
      subscribers.push(subscriber);
    }
    this.subscription[key] = subscribers;
  };

  unsubscribe = (key, subscriber) => {
    const subscribers = this._getSubscribers(key);
    const index = subscribers.indexOf(subscriber);
    if (index > -1) {
      delete subscribers.splice(index, 1);
    }
    this.subscription[key] = subscribers;
  };

  notify(key, data) {
    const subscribers = this._getSubscribers(key);
    if (subscribers) {
      subscribers.forEach(subscribe => subscribe(data));
    }
  }
}
export default new SubscriptionManager();
