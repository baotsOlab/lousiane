/**
 * Handlers errors and subscriptions for errors
 */
class ErrorManager {
  constructor() {
    this.subscribers = [];
  }

  /**
   * Report that an unexpected error has occured
   * @param {String} message  - message to display during error
   * @param {Object} error - details of the error
   */
  createError = (title, error) => {
    this._notifySubscribers(title, error);
  };

  /**
   * Subscribe to errors occuring
   * @param {Function} subscriber - (message, error) => ...
   */
  subscribe = subscriber => {
    this.subscribers.push(subscriber);
  };
  /**
   * Remove original subscriber
   * @param {Function} subscriber - function that was used to subscribe for changes
   */
  unsubscribe = subscriber => {
    const index = this.subscribers.indexOf(subscriber);

    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  };

  _notifySubscribers(title, error) {
    this.subscribers.forEach(subscriber => subscriber(title, error));
  }
}
export { ErrorManager };
export default new ErrorManager();
