import { post, get } from './networking';
import env from '../env';

/**
 *
 * @param params {{ email, password }}
 * @returns {Promise<*>}
 */
const login = async (params) => post('/login', params);

const logout = async () => get('/auth/logout');

/**
 *
 * @param params {{ name, email, password, phone }}
 * @returns {Promise<*>}
 */
const signUp = async (params) => post('/signup', params);

const sendCode = async (params) => post('/sendcode', params);
const getCategories = async () => get('/categories');
const getZones = async () => get('/zones');

const getProfile = async () => get('/myprofile');
const editProfile = async (params) => get('/editprofile', params);
const editAvatar = async (data) => post('/editavatar', data, { 'Content-Type': 'multipart/form-data' }, false);

const getOrders = async (params) => get('/myorders', params);
const getOrderDetail = async (id) => get(`/orderdetail/${id}`);

/**
 *
 * @param params {{ id, lat, lng, page, take }}
 * @returns {Promise<*>}
 */
const getRestaurantsNearYou = async (params) => post('/restaurants', params);
const like = async (id) => get(`/like/${id}`);

const getRestaurantInfo = async (id) => get(`/restaurantinfo/${id}`);
const getDishes = async (id) => get(`/dishes/${id}`);
const getDeliveryFee = async (params) => post('/deliveryfee', params);
const checkout = async (params) => post('/checkout', params);

/**
 * @param params {{ phone, app_target }}
 * @returns {Promise<*>}
 */
const sendSmsCode = async (params) => get('/auth/sms_verify', params);
/**
 * @param params {{ verify_token, phone, device_id, device_type }}
 */
const verifySmsCode = async (params) => get('/auth/verify', params);

const getPhoneNumber = async () => get('/getcallphone');

/**
 * @param params {{ order_id }}
 */
const cancelOrder = async (params) => get('/cancel', params);

/**
 * @param params {{ fcm_token }}
 */
const updateDeviceToken = async (params) => get('/auth/update_fcm_token', params);

/**
 * @param params {{ id_restaurant, page, take }}
 */
const getSuggestedRestaurants = async (params) => post('/near_restaurants', params);

/**
 * @param params {{ search, take, page }}
 */
const searchRestaurants = async (params) => post('/search', params);

const fetchGoogleAPI = async (url) => {
  const response = await fetch(url, { headers: { Host: env.hostHeader } });
  return response.json();
};

/**
 * @param params {{ restaurants }}
 */
const getDeliveryTime = async (params) => post('/order-time-future', params);

/**
 * @param params {{ dishes }}
 */
const getDishesPrice = async (params) => post('/get-dishes-price', params);

const api = {
  login,
  signUp,
  sendCode,
  logout,
  getCategories,
  getZones,
  getProfile,
  editAvatar,
  editProfile,
  getOrders,
  getOrderDetail,
  getRestaurantsNearYou,
  like,
  getRestaurantInfo,
  getDishes,
  getDeliveryFee,
  checkout,
  sendSmsCode,
  verifySmsCode,
  getPhoneNumber,
  cancelOrder,
  updateDeviceToken,
  getSuggestedRestaurants,
  searchRestaurants,
  fetchGoogleAPI,
  getDeliveryTime,
  getDishesPrice
};

export default api;
