import { StyleSheet } from 'react-native';
import { scaleFont, scaleHorizontal, scaleSize, scaleVertical } from './size';

const PREFIXES = {
  FONT: 'f_',
  VERTICAL: 'v_',
  HORIZONTAL: 'h_',
  SIZE: 's_',
};
/**
 * f_: scale font
 * v_: scale vertical
 * h_: scale horizontal
 * s_: scale size
 * @type {string[]}
 */
const prefixes = [
  PREFIXES.FONT,
  PREFIXES.VERTICAL,
  PREFIXES.HORIZONTAL,
  PREFIXES.SIZE
];

const getFuncByPrefix = (prefix) => {
  switch (prefix) {
    case PREFIXES.FONT:
      return scaleFont;
    case PREFIXES.VERTICAL:
      return scaleVertical;
    case PREFIXES.HORIZONTAL:
      return scaleHorizontal;
    case PREFIXES.SIZE:
    default:
      return scaleSize;
  }
};

const create = (obj) => {
  Object.keys(obj).forEach(key => {
    const style = obj[key];
    Object.keys(style).forEach(styleKey => {
      prefixes.forEach(prefix => {
        if (styleKey.startsWith(prefix)) {
          // TODO: apply re-select to cache calculation
          const scale = getFuncByPrefix(prefix);
          obj[key][styleKey.slice(2)] = scale(obj[key][styleKey]);
          delete obj[key][styleKey];
        }
      });
    });
  });
  return StyleSheet.create(obj);
};

export default { create };

