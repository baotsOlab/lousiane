import { NavigationActions, StackActions } from 'react-navigation';

/**
 * @param params {{dispatch, routes, index}}
 */
const reset = (params) => {
  const { dispatch, routes = [], index = 0 } = params;
  const resetAction = StackActions.reset({
    index,
    actions: routes.map((route) => {
      const { name: routeName, params: routeParams = {} } = route;
      return NavigationActions.navigate({ routeName, params: routeParams });
    }),
    key: null
  });
  dispatch(resetAction);
};

export default {
  reset,
};
