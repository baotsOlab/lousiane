import _ from 'lodash';
import { Alert } from 'react-native';
import Images from '../assets/Images';
import { GOOGLE_STATUS, ORDER_PHASES, RESTAURANT_STATUS } from '../assets/constants/constant';
import { existRestaurant } from '../store/reducers/cart';
import env from '../env';
import api from '../services/api';
import errorManager from '../services/errorManager';
import Routes from '../navigation/routes';

const getOrderStatusIcon = status => {
  switch (status) {
    case ORDER_PHASES.CONFIRMATION:
      return Images.statusConfirmation;
    case ORDER_PHASES.ASSIGNED:
      return Images.statusAssign;
    case ORDER_PHASES.PICKUP:
      return Images.loading;
    case ORDER_PHASES.DELIVERED:
      return Images.tickIcon;
    default:
      return Images.cancelIcon;
  }
};

const isValidToAddToCart = async ({
  restaurants,
  restaurantInfo,
  showLoading = () => {},
  navigation
}) => {
  const restaurant = restaurantInfo.data;
  if (restaurant.status !== RESTAURANT_STATUS.OPEN) {
    alert(`Dear customers, our services are temporarily unavailable.

We would like to sincerely apologise for the inconvenience caused.

We thank you for your kind understanding and hope to serve you better soon.`);
    return false;
  }

  const isExist = existRestaurant(restaurant.id, restaurants);
  if (restaurants.length >= 3 && !isExist) {
    alert('Our app does not support adding products from more than three groceries or restaurants yet. Sorry for your inconvenience.');
    return false;
  }

  // validate the distance from first restaurants to others. must equal or less than 500 meters
  if (restaurants.length > 0 && !isExist) {
    const mainRestaurant = restaurants[0];
    let url = env.googleAPI.maps.GET_DISTANCE_MATRIX_GOOGLE_MAP_URL;
    url = url.replace('{from}', `${mainRestaurant.lat},${mainRestaurant.lng}`);
    url = url.replace('{to}', `${restaurant.lat},${restaurant.lng}`);
    showLoading(true);
    const goToNearBy = () => {
      if (restaurants.length === 0) return;
      const firstRestaurant = restaurants[0];
      navigation.navigate(Routes.NearYou, {
        mainRestaurantId: firstRestaurant.id,
        restaurantName: firstRestaurant.name
      });
    };
    const alertAndGoToNearBy = () => {
      Alert.alert(
        'Oops...',
        'Sorry. This grocery store is outside of our current pickup zone. Please select another nearby store.',
        [
          { text: 'Groceries near by', onPress: goToNearBy },
          { text: 'Cancel', onPress: () => {}, style: 'cancel' },
        ],
      );
    };
    try {
      const json = await api.fetchGoogleAPI(url);
      const firstElement = _.get(json, 'rows[0].elements[0]');
      if (_.isNil(firstElement) || firstElement.status === GOOGLE_STATUS.ZERO_RESULTS) {
        alertAndGoToNearBy();
        return false;
      } else if (firstElement.status === GOOGLE_STATUS.OK) {
        const distance = _.get(firstElement, 'distance.value');
        if (distance > 500) {
          alertAndGoToNearBy();
          return false;
        }
      }
    } catch (error) {
      errorManager.createError('Get distance', error);
      return false;
    } finally {
      showLoading(false);
    }
  }

  return true;
};

export default { getOrderStatusIcon, isValidToAddToCart };
