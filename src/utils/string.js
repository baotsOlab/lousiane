import moment from 'moment';
import _ from 'lodash';
import momentWithTimezone from 'moment-timezone';

export function convertToTimeUTCString(date) {
  const dateUtc = moment.utc(date);
  return dateUtc.format('DD MMMM, YYYY');
}

export function convertToHourString(hour, tz = 'Australia/Sydney') {
  return momentWithTimezone.tz(hour, 'HH:mm:ss', tz).local().format('hh:mma');
}

export const getString = (object, key, defaultValue = '') => {
  return _.chain(object).get(key).defaultTo(defaultValue).value();
};

export const getNumber = (object, key) => {
  return _.chain(object).get(key).defaultTo(0).value();
};

export const isEmail = (string) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(string);
};

export const getNumberOnly = (string = '') => string.replace(/[^\d]+/g, '');
