import firebase from 'react-native-firebase';
import api from '../services/api';
import subscriptionManager, { SUBSCRIPTION_TYPE } from '../services/subscriptionManager';

const listenNotifications = () => {
  const notifications = firebase.notifications();
  notifications.onNotification(notification => {
    subscriptionManager.notify(SUBSCRIPTION_TYPE.NOTIFICATION, notification.data);
  });
  notifications.onNotificationOpened(({ notification }) => {
    subscriptionManager.notify(SUBSCRIPTION_TYPE.NOTIFICATION_OPENED, notification.data);
  });
};

const messaging = firebase.messaging();
/* START: DEVICE TOKEN */
const updateToken = () => {
  listenNotifications();
  messaging.getToken().then(onUpdateToken);
  messaging.onTokenRefresh(onUpdateToken);
};

const onUpdateToken = async (token) => {
  if (token) {
    api.updateDeviceToken({ fcm_token: token });
  }
};
/* END: DEVICE TOKEN */

const checkAndRequestPermission = async () => {
  let enabled = await messaging.hasPermission();
  if (!enabled) {
    try {
      await messaging.requestPermission();
      enabled = true;
    } catch (error) {
      enabled = false;
    }
  }
  if (!enabled) {
    enabled = await messaging.hasPermission();
  }
  return enabled;
};

export default {
  updateToken,
  checkAndRequestPermission
};
