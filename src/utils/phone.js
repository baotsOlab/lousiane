import { Linking } from 'react-native';

import platformHelper from './platform';
import errorManager from '../services/errorManager';

const call = async (phoneNumber, prompt = false) => {
  let url = `tel:${phoneNumber}`;
  if (prompt && platformHelper.isIOS) {
    url = `telprompt:${phoneNumber}`;
  }

  try {
    const canOpen = await Linking.canOpenURL(url);
    if (canOpen) {
      await Linking.openURL(url);
    }
  } catch (error) {
    errorManager.createError('Phone call', error);
  }
};

export default { call };
