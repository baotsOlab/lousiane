import { PermissionsAndroid } from 'react-native';
import _ from 'lodash';
import FusedLocation from 'react-native-fused-location';
import platformHelper from './platform';
import { LocationError } from '../common/CustomErrors';

const positionOptions = {
  enableHighAccuracy: true,
  timeout: 20000,
  maximumAge: 1000
};

const DISTANCE_FILTER = 10;

class Location {
  watchID = 0;

  async requestLocation(callback = () => {}, errorCallback = () => {}) {
    if (platformHelper.isAndroid) {
      const granted = await this.requestLocationPermission();
      if (!granted) {
        errorCallback(new LocationError('Permission denied!'));
        return;
      }
      this._getLocationForAndroid(callback, errorCallback);
      return;
    }

    this._getLocation(callback, errorCallback);
  }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'App needs to access your location',
          message: 'App needs access to your location get the right data for you.'
        },
      );
      if (_.isBoolean(granted)) {
        return granted;
      }
      return granted === PermissionsAndroid.RESULTS.GRANTED;
    } catch (error) {
      return false;
    }
  }

  _getLocationForAndroid = async (callback, errorCallback) => {
    FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);
    try {
      const { latitude, longitude } = await FusedLocation.getFusedLocation();
      callback(latitude, longitude);
    } catch (error) {
      errorCallback(error);
    }
  };

  _getLocation = (callback, errorCallback) => {
    navigator.geolocation.getCurrentPosition(
      position => { callback(position.coords.latitude, position.coords.longitude); },
      (e) => { errorCallback(e); },
      positionOptions,
    );
  };


  watchPosition = async (callback = () => {}, errorCallback = () => {}) => {
    if (platformHelper.isAndroid) {
      const granted = await this.requestLocationPermission();
      if (!granted) {
        errorCallback(new LocationError('Permission denied!'));
        return;
      }
      this._watchPositionAndroid(callback, errorCallback);
      return;
    }

    this._watchPositionIOS(callback, errorCallback);
  };

  _watchPositionIOS = (callback, errorCallback) => {
    this.watchID = navigator.geolocation.watchPosition(
      position => { callback(position.coords.latitude, position.coords.longitude); },
      (e) => { errorCallback(e); },
      {
        ...positionOptions,
        distanceFilter: DISTANCE_FILTER,
        useSignificantChanges: true
      },
    );
  };

  _watchPositionAndroid = (callback, errorCallback) => {
    FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);
    FusedLocation.setLocationInterval(20000);
    FusedLocation.setFastestLocationInterval(15000);
    FusedLocation.setSmallestDisplacement(DISTANCE_FILTER);
    FusedLocation.startLocationUpdates();

    this.subscription = FusedLocation.on('fusedLocation', location => {
      const { latitude, longitude } = location;
      callback(latitude, longitude);
    });

    this.errSubscription = FusedLocation.on('fusedLocationError', error => {
      errorCallback(error);
    });
  };

  clear() {
    if (platformHelper.isIOS) {
      navigator.geolocation.clearWatch(this.watchID);
    } else if (platformHelper.isAndroid) {
      FusedLocation.off(this.subscription);
      FusedLocation.off(this.errSubscription);
      FusedLocation.stopLocationUpdates();
    }
  }
}

export default new Location();
