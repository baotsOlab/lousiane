import _ from 'lodash';

/* eslint-disable no-bitwise */
/* eslint-disable no-mixed-operators */
const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.random() * 16 | 0;
    const v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};

const getImageObjectForFormData = (image) => {
  return {
    uri: image.path,
    type: image.mime,
    name: _.get(image, 'filename', 'filename.jpg').toLowerCase()
  };
};

const numberWithCommas = (number = 0) => {
  const inputNumber = _.isNumber(number) ? number : 0;
  const x = parseFloat(inputNumber).toFixed(2);
  const parts = x.toString().split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return parts.join('.');
};

export default { uuidv4, getImageObjectForFormData, numberWithCommas };
