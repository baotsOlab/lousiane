/*
Calculate size in difference screen size based on standard size.
Use iPhone 6 specification as a default standard size.
Only support for Native Potrait mode.

Author: Denza
https://github.com/denza-dev

Inspired By:
https://www.npmjs.com/package/react-native-responsive-dimensions
https://github.com/nirsky/react-native-size-matters
 */

import { Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

// Use iPhone 6 size as a standard
const base = { width: 375, height: 667 };

const screenSize = {
  base: Math.sqrt((base.height * base.height) + (base.width * base.width)),
  current: Math.sqrt((height * height) + (width * width))
};

// Calculate the new size into responsive size using screen size percentage.
export const scaleSize = (size) => {
  return (size / screenSize.base) * screenSize.current;
};

// Calculate the new size into responsive size using screen width percentage.
export const scaleHorizontal = (w) => {
  return (w / base.width) * width;
};

// Calculate the new size into responsive size using screen height percentage.
export const scaleVertical = (h) => {
  return (h / base.height) * height;
};

// Calculate the new font size into responsive font size using screen width percentage.
export const scaleFont = (size) => scaleHorizontal(size);

export default {
  scaleSize,
  scaleHorizontal,
  scaleVertical,
  scaleFont
};
